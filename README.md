EtherCAT Configuration Module
=============================

The EtherCAT Configuration Module is a tool that configures EtherCAT networks.  Currently it supports only the
[IgH EtherCAT master](http://www.etherlab.org/en/ethercat/index.php).  This module additionally requires [RTAI](http://www.rtai.org)
for real-time behavior.

The EtherCAT Configuratoin Module is divided in two parts.  A Linux kernel module that performs the configuration itself and a
user-space program that provides it with configuration data.  The user-space program scans the network and groups PDO-entries in
domains based on an input XML configuration.  Once the configuration is done, the API exported by the kernel module can be used
to search for domains of interest and start working with the network.

This module is also used in the EtherCAT driver included in Skinware
