/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ETHERCAT_CONFIGURE_H
#define ETHERCAT_CONFIGURE_H

#define				ECC_PROCFS_FILE_NAME			"ecconfig"

#ifdef __KERNEL__
#include <linux/stringify.h>
#include <ecrt.h>

#define				ECC_VERSION_MAJOR			0
#define				ECC_VERSION_MINOR			2
#define				ECC_VERSION_REVISION			0
#define				ECC_VERSION_BUILD			107
#define				ECC_VERSION				__stringify(ECC_VERSION_MAJOR)"."\
										__stringify(ECC_VERSION_MINOR)"."\
										__stringify(ECC_VERSION_REVISION)"."\
										__stringify(ECC_VERSION_BUILD)

#define				ECC_NAME_MAX_LENGTH			64
#define				ECC_NAME_MAX_LENGTH_STR			__stringify(ECC_NAME_MAX_LENGTH)
typedef	char ecc_name[ECC_NAME_MAX_LENGTH + 1];

typedef struct ecc_domain_entry
{
	uint16_t		slave;			/* index to slave_names of ecc_master */
	uint16_t		entry;			/* index to entry_names of ecc_master */
} ecc_domain_entry;

typedef struct ecc_domain
{
	ec_domain_t		*domain;
	ecc_domain_entry	*entries;		/*
							 * entries in the domain, in the form of
							 * pairs of (slave, entry), which are names of
							 * slave groups and pdo entry groups respectively
							 */
	uint32_t		entries_count;		/* size of entries array */
	uint8_t			*data;			/*
							 * Arriving data from network and data sent to it
							 * Access this array using EC_READ_* and EC_WRITE_*
							 * indexed with *entries[e].offset
							 */
} ecc_domain;

typedef struct ecc_position_in_domain
{
	uint32_t		domain;			/* domain the entry belongs to */
	unsigned int		offset;			/* offset in data of domain */
	unsigned int		bit_position;		/* bit position at offset */
} ecc_position_in_domain;

typedef struct ecc_pdo_entry
{
	uint16_t		name;			/*
							 * index in entry_names of ecc_master
							 * 0xffff if not named
							 */
	ecc_position_in_domain	*domains;		/* domains this entry belongs to */
	uint32_t		domains_count;		/* size of domains array */
} ecc_pdo_entry;

typedef struct ecc_pdo
{
	ecc_pdo_entry		*entries;		/* entries of this pdo */
	uint32_t		entries_count;		/* size of entries array */
} ecc_pdo;

typedef struct ecc_sync
{
	ecc_pdo			*pdos;			/* pdos of this sync */
	uint32_t		pdos_count;		/* size of pdos array */
} ecc_sync;

typedef struct ecc_slave
{
	uint16_t		name;			/*
							 * index in slave_names of ecc_master
							 * 0xffff if not named
							 */
	ecc_sync		*syncs;			/* syncs of this slave */
	uint32_t		syncs_count;		/* size of syncs array */
} ecc_slave;

typedef struct ecc_master
{
	bool			ready;			/* if false, then other variables are undefined */
	ec_master_t		*master;

	ecc_slave		*slaves;		/* slaves, their syncs, pdos and pdo entries */
	uint32_t		slaves_count;		/* size of slaves array */

	ecc_name		*slave_names;		/* slave group names, sorted */
	uint32_t		slave_names_count;	/* size of slave_names array */
	ecc_name		*entry_names;		/* entry names, sorted */
	uint32_t		entry_names_count;	/* size of entry_names array */

	ecc_domain		*domains;		/* domains handled by this master */
	uint32_t		domains_count;		/* size of domains array */

	void			*internal;		/* for internal use */
} ecc_master;

extern ecc_master		*ecc_masters;		/* masters configured with this module */
extern unsigned int		ecc_masters_count;	/* size of ecc_masters array */

void ecc_lock_master(ecc_master *);
void ecc_unlock_master(ecc_master *);

#endif // __KERNEL__

#endif
