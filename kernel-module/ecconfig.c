/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/vmalloc.h>
#include <rtai.h>
#include <rtai_sem.h>
#include <kio.h>
#include "ecconfig.h"
#include "ecc_config.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

#ifndef NDEBUG
# define 			END_OF_FILE_PATH			__FILE__ + (sizeof(__FILE__) < 25?0:(sizeof(__FILE__) - 15))
# define			ECC_LOG(x, ...)				printk(KERN_INFO "ecconfig: ...%s(%u): " x"\n",\
											END_OF_FILE_PATH, __LINE__, ##__VA_ARGS__)
# define			ECC_RT_LOG(x, ...)			rt_printk(KERN_INFO "ecconfig: ...%s(%u): " x"\n",\
											END_OF_FILE_PATH, __LINE__, ##__VA_ARGS__)
# define			ECC_DBG(x, ...)				ECC_LOG(x, ##__VA_ARGS__)
# define			ECC_RT_DBG(x, ...)			ECC_RT_LOG(x, ##__VA_ARGS__)
#else
# define			ECC_LOG(x, ...)				printk(KERN_INFO "ecconfig: " x"\n", ##__VA_ARGS__)
# define			ECC_RT_LOG(x, ...)			rt_printk(KERN_INFO "ecconfig: " x"\n", ##__VA_ARGS__)
# define			ECC_DBG(x, ...)				((void)0)
# define			ECC_RT_DBG(x, ...)			((void)0)
#endif
#define				ECC_LOG_M(x, ...)			ECC_LOG("master %u: " x, _current_master, ##__VA_ARGS__)
#define				ECC_LOG_MD(x, ...)			ECC_LOG("master %u: domain %u: " x, _current_master,			\
										_current_domain, ##__VA_ARGS__)
#define				ECC_LOG_MS(x, ...)			ECC_LOG("master %u: slave %u: " x, _current_master,			\
										_current_slave, ##__VA_ARGS__)
#define				ECC_LOG_MSY(x, ...)			ECC_LOG("master %u: slave %u: sync %u: " x, _current_master,		\
										_current_slave, _current_sync, ##__VA_ARGS__)
#define				ECC_LOG_MSYP(x, ...)			ECC_LOG("master %u: slave %u: sync %u: pdo %u: " x, _current_master,	\
										_current_slave, _current_sync, _current_pdo, ##__VA_ARGS__)
#define				ECC_LOG_MSYPE(x, ...)			ECC_LOG("master %u: slave %u: sync %u: pdo %u: entry %u: " x,		\
										_current_master, _current_slave, _current_sync, _current_pdo,	\
										_current_pdo_entry, ##__VA_ARGS__)
#define				ECC_DBG_M(x, ...)			ECC_DBG("master %u: " x, _current_master, ##__VA_ARGS__)
#define				ECC_DBG_MD(x, ...)			ECC_DBG("master %u: domain %u: " x, _current_master,			\
										_current_domain, ##__VA_ARGS__)
#define				ECC_DBG_MS(x, ...)			ECC_DBG("master %u: slave %u: " x, _current_master,			\
										_current_slave, ##__VA_ARGS__)
#define				ECC_DBG_MSY(x, ...)			ECC_DBG("master %u: slave %u: sync %u: " x, _current_master,		\
										_current_slave, _current_sync, ##__VA_ARGS__)
#define				ECC_DBG_MSYP(x, ...)			ECC_DBG("master %u: slave %u: sync %u: pdo %u: " x, _current_master,	\
										_current_slave, _current_sync, _current_pdo, ##__VA_ARGS__)
#define				ECC_DBG_MSYPE(x, ...)			ECC_DBG("master %u: slave %u: sync %u: pdo %u: entry %u: " x,		\
										_current_master, _current_slave, _current_sync, _current_pdo,	\
										_current_pdo_entry, ##__VA_ARGS__)

#define				ECC_COMMAND_MASTERS			'M'
#define				ECC_COMMAND_SLAVE_NAMES			'S'
#define				ECC_COMMAND_ENTRY_NAMES			'E'
#define				ECC_COMMAND_DOMAINS			'D'
#define				ECC_COMMAND_ADD_DOMAIN_ENTRIES		'd'
#define				ECC_COMMAND_SLAVES			'N'
#define				ECC_COMMAND_ADD_SLAVE			's'
#define				ECC_COMMAND_ADD_SYNC			'y'
#define				ECC_COMMAND_ADD_PDO			'p'
#define				ECC_COMMAND_ADD_PDO_ENTRY		'e'
#define				ECC_COMMAND_FINALIZE_SLAVE		'f'
#define				ECC_COMMAND_REGISTER_DOMAINS		'r'
#define				ECC_COMMAND_ACTIVATE_MASTER		'a'

#define				ECC_NAME_FORMAT_REG			"[^\"]"

ecc_master			*ecc_masters				= NULL;
unsigned int			ecc_masters_count			= 0;
EXPORT_SYMBOL_GPL(ecc_masters);
EXPORT_SYMBOL_GPL(ecc_masters_count);

typedef struct master_extra
{
	/* private data */
	SEM			mutex;
	/* memory */
	ecc_slave		*all_slaves;
	ecc_sync		*all_syncs;
	ecc_pdo			*all_pdos;
	ecc_pdo_entry		*all_pdo_entries;
	ecc_position_in_domain	*all_position_in_domains;
	ecc_domain		*all_domains;
	ecc_domain_entry	*all_domain_entries;
	uint32_t		all_slaves_count;
	uint32_t		all_syncs_count;
	uint32_t		all_pdos_count;
	uint32_t		all_pdo_entries_count;
	uint32_t		all_position_in_domains_count;
	uint32_t		all_domains_count;
	uint32_t		all_domain_entries_count;
} master_extra;

static master_extra		*_masters_extra				= NULL;

/* temporary variables for configuration phase */
static uint32_t			_current_master				= 0;
static bool			_ignore_current_master			= false;
static uint32_t			_current_slave				= 0;
static uint32_t			_current_sync				= 0;
static uint32_t			_current_pdo				= 0;
static uint32_t			_current_pdo_entry			= 0;
static uint32_t			_current_domain				= 0;
static uint32_t			_current_domain_entry			= 0;
static uint32_t			_next_unused_sync			= 0;
static uint32_t			_next_unused_pdo			= 0;
static uint32_t			_next_unused_pdo_entry			= 0;
static uint32_t			_next_unused_position_in_domain		= 0;
static uint32_t			_next_unused_domain_entry		= 0;

/* temporary memory for configuration phase */
typedef struct domain_config
{
	ec_pdo_entry_reg_t	*pdo_entries;
	uint32_t		pdo_entries_count;
	uint32_t		pdo_entries_mem_size;
	uint32_t		next_unused_pdo_entry;
} domain_config;
static ec_slave_info_t		_current_slave_info;
static ec_sync_info_t		*_sync_infos				= NULL;
static ec_pdo_info_t		*_pdo_infos				= NULL;
static ec_pdo_entry_info_t	*_pdo_entry_infos			= NULL;
static struct domain_config	*_domain_infos				= NULL;
static uint32_t			_sync_infos_mem_size			= 0;
static uint32_t			_pdo_infos_mem_size			= 0;
static uint32_t			_pdo_entry_infos_mem_size		= 0;
static uint32_t			_domain_infos_mem_size			= 0;
static uint32_t			_sync_infos_count			= 0;
static uint32_t			_pdo_infos_count			= 0;
static uint32_t			_pdo_entry_infos_count			= 0;
static uint32_t			_domain_infos_count			= 0;
static uint32_t			_next_unused_sync_info			= 0;
static uint32_t			_next_unused_pdo_info			= 0;
static uint32_t			_next_unused_pdo_entry_info		= 0;

/* input handling */
static KFILE			*_cfgin					= NULL;

/* initial thread */
static struct task_struct	*_init_thread_task;
static bool			_init_thread_must_exit			= false;
static bool			_init_thread_exited			= true;

/* helper functions and macros */
static inline void _init_ecc_domain_entry(ecc_domain_entry *e)
{
	*e = (ecc_domain_entry){0};
}

static inline void _init_ecc_domain(ecc_domain *d)
{
	*d = (ecc_domain){0};
}

static inline void _init_ecc_position_in_domain(ecc_position_in_domain *p)
{
	*p = (ecc_position_in_domain){0};
}

static inline void _init_ecc_pdo_entry(ecc_pdo_entry *e)
{
	*e = (ecc_pdo_entry){
		.domains = NULL,
		.domains_count = 0
	};
}

static inline void _init_ecc_pdo(ecc_pdo *p)
{
	*p = (ecc_pdo){
		.entries = NULL,
		.entries_count = 0
	};
}

static inline void _init_ecc_sync(ecc_sync *s)
{
	*s = (ecc_sync){
		.pdos = NULL,
		.pdos_count = 0
	};
}

static inline void _init_ecc_slave(ecc_slave *s)
{
	*s = (ecc_slave){
		.syncs = NULL,
		.syncs_count = 0
	};
}

static inline void _init_ecc_master(ecc_master *m)
{
	*m = (ecc_master){0};
}

static inline void _init_master_extra(master_extra *m)
{
	*m = (master_extra){
		.all_slaves = NULL,
		.all_syncs = NULL,
		.all_pdos = NULL,
		.all_pdo_entries = NULL,
		.all_position_in_domains = NULL,
		.all_domains = NULL,
		.all_domain_entries = NULL
	};
}

static inline void _init_ecc_name(ecc_name *n)
{
	(*n)[0] = '\0';
}

static inline void _init_domain_config(domain_config *dc)
{
	*dc = (domain_config){0};
}
#define				CLEAN_FREE(x)				do { vfree(x); x = NULL; } while (0)

#define MEM_ALLOC(_mem, _count)					\
do {								\
	if (_count > 0)						\
	{							\
		_mem = vmalloc((_count) * sizeof(*(_mem)));	\
		if (_mem == NULL)				\
			goto exit_no_mem;			\
	}							\
	else							\
		_mem = NULL;					\
} while (0)

#define MEM_ALLOC_AND_INIT(_mem, _type, _count)			\
do {								\
	unsigned int _i;					\
								\
	MEM_ALLOC(_mem, _count);				\
								\
	for (_i = 0; _i < (_count); ++_i)			\
		_init_##_type(&(_mem)[_i]);			\
} while (0)

/* MEM_ENLARGE destroys previous memory, so it's not like realloc */
#define MEM_ENLARGE(_mem, _size, _count)			\
do {								\
	if ((_size) < (_count))					\
	{							\
		CLEAN_FREE(_mem);				\
		MEM_ALLOC(_mem, (_count));			\
		(_size) = (_count);				\
	}							\
} while (0)

void ecc_lock_master(ecc_master *m)
{
	if (m != NULL && m->internal != NULL)
		rt_sem_wait(&((master_extra *)m->internal)->mutex);
}
EXPORT_SYMBOL(ecc_lock_master);

void ecc_unlock_master(ecc_master *m)
{
	if (m != NULL && m->internal != NULL)
		rt_sem_signal(&((master_extra *)m->internal)->mutex);
}
EXPORT_SYMBOL(ecc_unlock_master);

/*
 * These callbacks are only called when concurrent master access is enabled.
 * That is after ec_master_eoe_start is called
 */
static void _request_lock_callback(void *cb_data)
{
	ecc_lock_master(cb_data);
}

static void _release_lock_callback(void *cb_data)
{
	ecc_unlock_master(cb_data);
}

static int _wait_command(char expect)
{
	while (!_init_thread_must_exit)
	{
		char command;
		unsigned int i, n;

		if (kscanf(_cfgin, " %c", &command) != 1)
			goto exit_bad_io;
		if (command == expect)
			break;

		/* if not the expected command, ignore its input */
		ECC_LOG("unexpected command '%c' at this stage (ignored)", command);
		switch (command)
		{
		case ECC_COMMAND_MASTERS:
			if (kscanf(_cfgin, "%u", &n) != 1)
				goto exit_bad_io;
			for (i = 0; i < n; ++i)
				kscanf(_cfgin, "%*u");
			break;
		case ECC_COMMAND_SLAVE_NAMES:
		case ECC_COMMAND_ENTRY_NAMES:
			if (kscanf(_cfgin, "%u", &n) != 1)
				goto exit_bad_io;
			for (i = 0; i < n; ++i)
				kscanf(_cfgin, " \"%*"ECC_NAME_FORMAT_REG"\"");
			break;
		case ECC_COMMAND_DOMAINS:
			if (kscanf(_cfgin, "%u %*u", &n) != 1)
				goto exit_bad_io;
			for (i = 0; i < n; ++i)
				kscanf(_cfgin, "%*u");
			break;
		case ECC_COMMAND_ADD_DOMAIN_ENTRIES:
			if (kscanf(_cfgin, "%u", &n) != 1)
				goto exit_bad_io;
			for (i = 0; i < n; ++i)
				kscanf(_cfgin, "%*u %*u");
			break;
		case ECC_COMMAND_SLAVES:
			kscanf(_cfgin, "%*u %*u %*u %*u");
			break;
		case ECC_COMMAND_ADD_SLAVE:
			kscanf(_cfgin, "%*u %*u %*u %*x %*x %*u %*u %*u");
			break;
		case ECC_COMMAND_ADD_SYNC:
			kscanf(_cfgin, "%*x %*u %*u %*u");
			break;
		case ECC_COMMAND_ADD_PDO:
			kscanf(_cfgin, "%*x %*u");
			break;
		case ECC_COMMAND_ADD_PDO_ENTRY:
			if (kscanf(_cfgin, "%*u %*x %*x %*u %u", &n) != 1)
				goto exit_bad_io;
			for (i = 0; i < n; ++i)
				kscanf(_cfgin, "%*u");
			break;
		case ECC_COMMAND_FINALIZE_SLAVE:
		case ECC_COMMAND_REGISTER_DOMAINS:
		case ECC_COMMAND_ACTIVATE_MASTER:
		default:
			break;
		}
	}
	return 0;
exit_bad_io:
	ECC_LOG("unexpected end of file while reading commands or I/O error");
	return -1;
}

/*
 * Each of the _configure_* functions work like this:
 * If any of these functions returned a negative number, the error is unrecoverable.
 * If there is a problem with the master, 0 will be returned, but _ignore_current_master will be set.
 * If 0 is returned, then all relevant input is consumed regardless of whether _ignore_current_master is set.
 * If _ignore_current_master is set, then the structures may not be completely built.
 */

/*
 * Get number of masters, and the id of each
 *
 * Format: n id1 id2 id3 ... idn
 * Where:
 * 	n is the number of masters
 * 	idk is the id of the kth master
 */
static int _configure_masters(void)
{
	unsigned int i = 0;
	unsigned int n;

	if (_wait_command(ECC_COMMAND_MASTERS))
		goto exit_fail;

	if (kscanf(_cfgin, "%u", &n) != 1)
		goto exit_bad_io;

	/* input sanity check */
	if (n == 0)
		goto exit_invalid_input_master_0;

	ECC_DBG("configuring %u masters", n);

	ecc_masters_count = n;

	/* allocate and initialize memory */
	MEM_ALLOC_AND_INIT(ecc_masters, ecc_master, ecc_masters_count);
	MEM_ALLOC_AND_INIT(_masters_extra, master_extra, ecc_masters_count);

	/* read the ids and active the masters */
	for (i = 0; i < ecc_masters_count; ++i)
	{
		unsigned int id = 0;

		if (kscanf(_cfgin, "%u", &id) != 1)
			goto exit_bad_io;

		ecc_masters[i].master = ecrt_request_master(id);
		if (ecc_masters[i].master == NULL)
			ECC_LOG("requesting master %u failed", id);
		else
		{
			rt_typed_sem_init(&_masters_extra[i].mutex, 1, BIN_SEM);
			ecc_masters[i].internal = &_masters_extra[i];
			ecrt_master_callbacks(ecc_masters[i].master, _request_lock_callback, _release_lock_callback, &ecc_masters[i]);
		}
	}

	return 0;
exit_bad_io:
	ECC_LOG("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_master_0:
	ECC_LOG("0 as number of masters is not allowed");
	goto exit_fail;
exit_no_mem:
	ECC_LOG("out of memory");
	goto exit_fail;
exit_fail:
	return -1;
}

/* common part of getting slave and entry names */
static int _configure_names(ecc_name **names, uint32_t *count)
{
	unsigned int i;
	unsigned int read;

	/* allocate and initialize memory */
	MEM_ALLOC_AND_INIT(*names, ecc_name, *count);

	/* read in the names */
	read = 0;
	for (i = 0; i < *count; ++i)
	{
		/* read and truncate the name */
		if (kscanf(_cfgin, " \"%"ECC_NAME_MAX_LENGTH_STR ECC_NAME_FORMAT_REG, (*names)[i]) != 1)
			goto exit_bad_io;
		++read;

		/* discard rest of name, if any */
		kungetc('x', _cfgin);
		kscanf(_cfgin, "%*"ECC_NAME_FORMAT_REG"\"");

		/* make sure the names arrive sorted */
		if (i > 0 && strcmp((*names)[i], (*names)[i - 1]) < 0)
			goto exit_invalid_input_slave_unsorted;
	}

	return 0;
exit_bad_io:
	ECC_LOG_M("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_slave_unsorted:
	ECC_LOG_M("slave/entry names must arrive sorted");
	goto exit_ignore;
exit_no_mem:
	ECC_LOG_M("out of memory");
	goto exit_fail;
exit_fail:
	return -1;
exit_ignore:
	/* consume any input left */
	for (; read < *count; ++read)
		kscanf(_cfgin, " \"%*"ECC_NAME_FORMAT_REG"\"");
	_ignore_current_master = true;
	return 0;
}

/*
 * Get number of slave groups, and their names
 *
 * Format: n "name1" "name2" "name3" ... "namen"
 * Where:
 * 	n is the number of slave groups
 * 	namek is the name of the kth group
 */
static int _configure_slave_names(void)
{
	unsigned int i = 0;
	unsigned int n;
	ecc_master *m;

	if (_wait_command(ECC_COMMAND_SLAVE_NAMES))
		goto exit_fail;

	if (kscanf(_cfgin, "%u", &n) != 1)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];

	/* input sanity check */
	if (n == 0)
		goto exit_invalid_input_slave_0;

	ECC_DBG_M("configuring %u slave names", n);

	m->slave_names_count = n;

	if (_configure_names(&m->slave_names, &m->slave_names_count))
		goto exit_fail;

	return 0;
exit_bad_io:
	ECC_LOG_M("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_slave_0:
	ECC_LOG_M("0 as number of slave names is not allowed");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	/* consume any input left */
	for (i = 0; i < n; ++i)
		kscanf(_cfgin, " \"%*"ECC_NAME_FORMAT_REG"\"");
	_ignore_current_master = true;
	return 0;
}

/*
 * Get number of entry groups, and their names
 *
 * Format: n "name1" "name2" "name3" ... "namen"
 * Where:
 * 	n is the number of entry groups
 * 	namek is the name of the kth group
 */
static int _configure_entry_names(void)
{
	unsigned int i = 0;
	unsigned int n;
	ecc_master *m;

	if (_wait_command(ECC_COMMAND_ENTRY_NAMES))
		goto exit_fail;

	if (kscanf(_cfgin, "%u", &n) != 1)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];

	/* input sanity check */
	if (n == 0)
		goto exit_invalid_input_entry_0;

	ECC_DBG_M("configuring %u entry names", n);

	m->entry_names_count = n;

	if (_configure_names(&m->entry_names, &m->entry_names_count))
		goto exit_fail;

	return 0;
exit_bad_io:
	ECC_LOG_M("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_entry_0:
	ECC_LOG_M("0 as number of entry names is not allowed");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	/* consume any input left */
	for (i = 0; i < n; ++i)
		kscanf(_cfgin, " \"%*"ECC_NAME_FORMAT_REG"\"");
	_ignore_current_master = true;
	return 0;
}

/*
 * Get number of domains, and the number of their entries
 *
 * Format: ndomain nentry npdoentry1 npdoentry2 ... npdoentryndomain
 * Where:
 * 	ndomain is the number of domains
 * 	nentry is the total number of domain entries in all domains
 * 	npdoentryk is the number of pdo entries in domain k
 */
static int _configure_domains(void)
{
	unsigned int ndomain, nentry;
	unsigned int sum;
	unsigned int i, read = 0;
	ecc_master *m;
	master_extra *me;

	if (_wait_command(ECC_COMMAND_DOMAINS))
		goto exit_fail;

	if (kscanf(_cfgin, "%u %u", &ndomain, &nentry) != 2)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];

	/* input sanity check */
	if (ndomain == 0 || nentry == 0)
		goto exit_invalid_input_sizes_0;

	ECC_DBG_M("configuring %u domains containing a total of %u entries", ndomain, nentry);

	m->domains_count = ndomain;
	me->all_domains_count = ndomain;
	me->all_domain_entries_count = nentry;

	/* allocate and initialize memory */
	MEM_ALLOC_AND_INIT(me->all_domains, ecc_domain, ndomain);
	MEM_ALLOC_AND_INIT(me->all_domain_entries, ecc_domain_entry, nentry);
	m->domains = me->all_domains;

	_next_unused_domain_entry = 0;

	/* allocate configuration memory */
	if (_domain_infos_mem_size < ndomain)
	{
		domain_config *enlarged;
		unsigned int i;

		MEM_ALLOC_AND_INIT(enlarged, domain_config, ndomain);
		for (i = 0; i < _domain_infos_mem_size; ++i)
			enlarged[i] = _domain_infos[i];

		CLEAN_FREE(_domain_infos);
		_domain_infos = enlarged;
		_domain_infos_mem_size = ndomain;
	}
	_domain_infos_count = ndomain;

	read = 0;
	sum = 0;
	for (i = 0; i < ndomain; ++i)
	{
		unsigned int npdoentry;
		ecc_domain *d = &m->domains[i];
		domain_config *dc = &_domain_infos[i];

		/* read number of pdo entries of this domain */
		if (kscanf(_cfgin, "%u", &npdoentry) != 1)
			goto exit_bad_io;
		++read;

		/* input sanity check */
		if (npdoentry == 0)
			goto exit_invalid_input_sizes_0;

		/* note: ec_pdo_entry_reg_t array must be null-terminated */
		MEM_ENLARGE(dc->pdo_entries, dc->pdo_entries_mem_size, npdoentry + 1);
		dc->pdo_entries_count = npdoentry;
		dc->next_unused_pdo_entry = 0;

		sum += npdoentry;

		/* tell master about the domain too */
		d->domain = ecrt_master_create_domain(m->master);
		if (d->domain == NULL)
			goto exit_no_domain;
	}

	/* allocate and initialize memory */
	MEM_ALLOC_AND_INIT(me->all_position_in_domains, ecc_position_in_domain, sum);
	me->all_position_in_domains_count = sum;

	_next_unused_position_in_domain = 0;

	return 0;
exit_bad_io:
	ECC_LOG_M("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_sizes_0:
	ECC_LOG_M("0 as number of %s is not allowed", ndomain == 0?"domains":
			nentry == 0?"domain entries":"pdo entries in domain");
	goto exit_ignore;
exit_no_domain:
	ECC_LOG_M("master failed to create domain");
	goto exit_ignore;
exit_no_mem:
	ECC_LOG_M("out of memory");
	goto exit_fail;
exit_fail:
	return -1;
exit_ignore:
	/* consume any input left */
	for (; read < ndomain; ++read)
		kscanf(_cfgin, "%*u");
	_ignore_current_master = true;
	return 0;
}

/*
 * Get domain entries
 *
 * Format: n slave1 entry1 slave2 entry2 ... slaven entryn
 * Where:
 * 	n is the number of entries in domain
 * 	slavek is the index to slave names for the kth domain entry
 * 	entryk is the index to entry names for the kth domain entry
 */
static int _configure_add_domain_entries(void)
{
	unsigned int i = 0;
	unsigned int n;
	unsigned int read = 0;
	ecc_master *m;
	master_extra *me;
	ecc_domain *d;

	if (_wait_command(ECC_COMMAND_ADD_DOMAIN_ENTRIES))
		goto exit_fail;

	if (kscanf(_cfgin, "%u", &n) != 1)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];
	d = &m->domains[_current_domain];

	/* input sanity check */
	if (_next_unused_domain_entry + n > me->all_domain_entries_count)
		goto exit_too_many;

	ECC_DBG_MD("configuring %u domain entries", n);

	d->entries = &me->all_domain_entries[_next_unused_domain_entry];
	d->entries_count = n;
	_next_unused_domain_entry += n;

	/* read the entries */
	read = 0;
	for (i = 0; i < d->entries_count; ++i)
	{
		unsigned int slave;
		unsigned int entry;
		ecc_domain_entry *de;

		if (kscanf(_cfgin, "%u %u", &slave, &entry) != 2)
			goto exit_bad_io;
		++read;

		if (slave > m->slave_names_count || entry > m->entry_names_count)
			goto exit_invalid_input_entry_out_of_range;

		de = &d->entries[i];
		de->slave = slave;
		de->entry = entry;
	}

	return 0;
exit_bad_io:
	ECC_LOG_MD("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_entry_out_of_range:
	ECC_LOG_MD("given slave-entry combination is out of previously defined slave and entry ranges");
	goto exit_ignore;
exit_too_many:
	ECC_LOG_MD("sum of number of domain entries is larger than what was previously given");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	/* consume any input left */
	for (; read < n; ++read)
		kscanf(_cfgin, "%*u %*u");
	_ignore_current_master = true;
	return 0;
}

/*
 * Get slave sizes
 *
 * Format: nslave nsync npdo npdoentry
 * Where:
 * 	nslave is the number of slaves
 * 	nsync is the total number of syncs in all slaves
 * 	npdo is the total number of pdos in all syncs in all slaves
 * 	npdoentry is the total number of pdo entries in all pdos in all syncs in all slaves
 */
static int _configure_slaves(void)
{
	unsigned int nslave, nsync, npdo, npdoentry;
	ecc_master *m;
	master_extra *me;

	if (_wait_command(ECC_COMMAND_SLAVES))
		goto exit_fail;

	if (kscanf(_cfgin, "%u %u %u %u", &nslave, &nsync, &npdo, &npdoentry) != 4)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];

	/* input sanity check */
	if (nslave == 0 || nsync == 0 || npdo == 0 || npdoentry == 0)
		goto exit_invalid_input_sizes_0;

	ECC_DBG_M("configuring %u slaves with a total of %u syncs, %u pdos and %u pdo entries",
			nslave, nsync, npdo, npdoentry);

	m->slaves_count = nslave;
	me->all_slaves_count = nslave;
	me->all_syncs_count = nsync;
	me->all_pdos_count = npdo;
	me->all_pdo_entries_count = npdoentry;

	/* allocate and initialize memory */
	MEM_ALLOC_AND_INIT(me->all_slaves, ecc_slave, nslave);
	MEM_ALLOC_AND_INIT(me->all_syncs, ecc_sync, nsync);
	MEM_ALLOC_AND_INIT(me->all_pdos, ecc_pdo, npdo);
	MEM_ALLOC_AND_INIT(me->all_pdo_entries, ecc_pdo_entry, npdoentry);
	m->slaves = me->all_slaves;

	_next_unused_sync = 0;
	_next_unused_pdo = 0;
	_next_unused_pdo_entry = 0;

	return 0;
exit_bad_io:
	ECC_LOG_M("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_sizes_0:
	ECC_LOG_M("0 as number of %s is not allowed", nslave == 0?"slaves":nsync == 0?"syncs":
			npdo == 0?"pdos":"pdo entries");
	goto exit_ignore;
exit_no_mem:
	ECC_LOG_M("out of memory");
	goto exit_fail;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

/*
 * Get slaves
 *
 * Format: name alias position vendorid productcode nsync npdo npdoentry
 * Where:
 * 	name is the index to slave names for this master
 * 	alias is the slave alias
 * 	position is the slave position
 * 	vendorid is the slave vendor id
 * 	productcode is the slave product code
 * 	nsync is the number of syncs of the slave
 * 	npdo is the number of pdos of all syncs of the slave
 * 	npdoentry is the number of pdo entries of all pdos of all syncs of the slave
 */
static int _configure_add_slave(void)
{
	unsigned int name, alias, position, vendorid, productcode;
	unsigned int nsync, npdo, npdoentry;
	ecc_master *m;
	master_extra *me;
	ecc_slave *s;

	if (_wait_command(ECC_COMMAND_ADD_SLAVE))
		goto exit_fail;

	if (kscanf(_cfgin, "%u %u %u %x %x %u %u %u", &name, &alias, &position,
			&vendorid, &productcode, &nsync, &npdo, &npdoentry) != 8)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];
	s = &m->slaves[_current_slave];

	/* input sanity check */
	if (_next_unused_sync + nsync > me->all_syncs_count
		|| _next_unused_pdo + npdo > me->all_pdos_count
		|| _next_unused_pdo_entry + npdoentry > me->all_pdo_entries_count)
		goto exit_too_many;
	if (name >= m->slave_names_count)
		goto exit_invalid_input_name_out_of_range;

	ECC_DBG_MS("configuring slave with name %u (\"%s\"), alias %u, position %u, vendor id %0#10x, product code %0#10x",
			name, m->slave_names[name], alias, position, vendorid, productcode);
	ECC_DBG_MS("    with a total of %u syncs, %u pdos and %u pdo entries", nsync, npdo, npdoentry);

	*s = (struct ecc_slave){
		.name = name,
		.syncs = &me->all_syncs[_next_unused_sync],
		.syncs_count = nsync
	};
	_next_unused_sync += nsync;

	/* allocate configuration memory */
	MEM_ENLARGE(_sync_infos, _sync_infos_mem_size, nsync);
	MEM_ENLARGE(_pdo_infos, _pdo_infos_mem_size, npdo);
	MEM_ENLARGE(_pdo_entry_infos, _pdo_entry_infos_mem_size, npdoentry);
	_sync_infos_count = nsync;
	_pdo_infos_count = npdo;
	_pdo_entry_infos_count = npdoentry;
	_next_unused_sync_info = 0;
	_next_unused_pdo_info = 0;
	_next_unused_pdo_entry_info = 0;

	/* store configuration data */
	_current_slave_info = (ec_slave_info_t){
		.alias = alias,
		.position = position,
		.vendor_id = vendorid,
		.product_code = productcode
	};

	return 0;
exit_bad_io:
	ECC_LOG_MS("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_name_out_of_range:
	ECC_LOG_MS("given slave name index is out of previously defined slave range");
	goto exit_ignore;
exit_too_many:
	ECC_LOG_MS("sum of number of %s is larger than what was previously given",
		_next_unused_sync + nsync > me->all_syncs_count?"syncs":
		_next_unused_pdo + npdo > me->all_pdos_count?"pdos":"pdo entries");
	goto exit_ignore;
exit_no_mem:
	ECC_LOG_MS("out of memory");
	goto exit_fail;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

/*
 * Get syncs
 *
 * Format: index io watchdog npdo
 * Where:
 * 	index is the sync index
 * 	io is 0 or 1 if the sync is for output or input respectively
 * 	watchdog is 0, 1 or other if sync's watchdog is disabled, enabled or follows
 * 		default respectively
 * 	npdo is the number of pdos of the sync
 */
static int _configure_add_sync(void)
{
	unsigned int index, io, watchdog, npdo;
	master_extra *me;
	ecc_sync *s;

	if (_wait_command(ECC_COMMAND_ADD_SYNC))
		goto exit_fail;

	if (kscanf(_cfgin, "%x %u %u %u", &index, &io, &watchdog, &npdo) != 4)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	me = &_masters_extra[_current_master];
	s = &ecc_masters[_current_master].slaves[_current_slave].syncs[_current_sync];

	/* input sanity check */
	if (_next_unused_pdo + npdo > me->all_pdos_count
		|| _next_unused_sync_info >= _sync_infos_count)
		goto exit_too_many;
	if (io != 0 && io != 1)
		goto exit_invalid_input_io_invalid;

	ECC_DBG_MSY("configuring sync with index %0#10x, direction %s, %s watchdog and %u pdos",
			index, io?"input":"output", watchdog == 0?"no":watchdog == 1?"has":"default", npdo);

	*s = (struct ecc_sync){
		.pdos = &me->all_pdos[_next_unused_pdo],
		.pdos_count = npdo
	};
	_next_unused_pdo += npdo;

	/* store configuration data */
	_sync_infos[_next_unused_sync_info++] = (ec_sync_info_t){
		.index = index,
		.n_pdos = npdo,
		.pdos = &_pdo_infos[_next_unused_pdo_info],
		.dir = io?EC_DIR_INPUT:EC_DIR_OUTPUT,
		.watchdog_mode = (watchdog == 0)?EC_WD_DISABLE:
				(watchdog == 1)?EC_WD_ENABLE:EC_WD_DEFAULT,
	};

	return 0;
exit_bad_io:
	ECC_LOG_MSY("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_io_invalid:
	ECC_LOG_MSY("I/O direction of sync must be either 0 or 1");
	goto exit_ignore;
exit_too_many:
	ECC_LOG_MSY("sum of number of %s is larger than what was previously given",
		_next_unused_pdo + npdo > me->all_pdos_count?"pdos":"syncs");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

/*
 * Get pdos
 *
 * Format: index nentry
 * Where:
 * 	index is the pdo index
 * 	nentry is the number of pdos of the sync
 */
static int _configure_add_pdo(void)
{
	unsigned int index, nentry;
	master_extra *me;
	ecc_pdo *p;

	if (_wait_command(ECC_COMMAND_ADD_PDO))
		goto exit_fail;

	if (kscanf(_cfgin, "%x %u", &index, &nentry) != 2)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	me = &_masters_extra[_current_master];
	p = &ecc_masters[_current_master].slaves[_current_slave].
		syncs[_current_sync].pdos[_current_pdo];

	/* input sanity check */
	if (_next_unused_pdo_entry + nentry > me->all_pdo_entries_count
		|| _next_unused_pdo_info >= _pdo_infos_count)
		goto exit_too_many;

	ECC_DBG_MSYP("configuring pdo with index %0#6x and %u pdo entries", index, nentry);

	*p = (struct ecc_pdo){
		.entries = &me->all_pdo_entries[_next_unused_pdo_entry],
		.entries_count = nentry
	};
	_next_unused_pdo_entry += nentry;

	/* store configuration data */
	_pdo_infos[_next_unused_pdo_info++] = (ec_pdo_info_t){
		.index = index,
		.n_entries = nentry,
		.entries = &_pdo_entry_infos[_next_unused_pdo_entry_info]
	};

	return 0;
exit_bad_io:
	ECC_LOG_MSYP("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_too_many:
	ECC_LOG_MSYP("sum of number of %s is larger than what was previously given",
		_next_unused_pdo_entry + nentry > me->all_pdo_entries_count?"pdo entries":"pdos");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

/*
 * Get pdo entries
 *
 * Format: name index subindex bitlength ndomain domain1 domain2 ... domainndomain
 * Where:
 * 	name is the index to entry names for this master
 * 	index is the pdo entry index
 * 	subindex is the pdo entry subindex
 * 	bitlength is the pdo entry bit length
 * 	ndomain is the number of domains this entry is part of
 * 	domaink is the id of the kth domain this entry is part of
 */
static int _configure_add_pdo_entry(void)
{
	unsigned int name, index, subindex, bitlength, ndomain;
	unsigned int i, read;
	ecc_master *m;
	master_extra *me;
	ecc_pdo_entry *e;

	if (_wait_command(ECC_COMMAND_ADD_PDO_ENTRY))
		goto exit_fail;

	if (kscanf(_cfgin, "%u %x %x %u %u", &name, &index, &subindex, &bitlength, &ndomain) != 5)
		goto exit_bad_io;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];
	e = &m->slaves[_current_slave].syncs[_current_sync].
		pdos[_current_pdo].entries[_current_pdo_entry];

	/* input sanity check */
	if (_next_unused_position_in_domain + ndomain > me->all_position_in_domains_count
		|| _next_unused_pdo_entry_info >= _pdo_entry_infos_count)
		goto exit_too_many;
	if (name >= m->entry_names_count)
		goto exit_invalid_input_name_out_of_range;

	ECC_DBG_MSYPE("configuring pdo entry with index %0#6x, subindex %0#6x, bit length %u and registered in %u domains",
			index, subindex, bitlength, ndomain);

	*e = (struct ecc_pdo_entry){
		.domains = &me->all_position_in_domains[_next_unused_position_in_domain],
		.domains_count = ndomain
	};
	_next_unused_position_in_domain += ndomain;

	/* store configuration data */
	_pdo_entry_infos[_next_unused_pdo_entry_info++] = (ec_pdo_entry_info_t){
		.index = index,
		.subindex = subindex,
		.bit_length = bitlength
	};

	/* read domains to which this entry belongs to */
	for (i = 0; i < ndomain; ++i)
	{
		unsigned int domain;
		domain_config *dc;

		if (kscanf(_cfgin, "%u", &domain) != 1)
			goto exit_bad_io;
		++read;

		if (domain >= m->domains_count)
			goto exit_invalid_input_domain_out_of_range;

		e->domains[i] = (ecc_position_in_domain){
			.domain = domain
		};

		/* store configuration data */
		dc = &_domain_infos[domain];
		dc->pdo_entries[dc->next_unused_pdo_entry++] = (ec_pdo_entry_reg_t){
			.alias = _current_slave_info.alias,
			.position = _current_slave_info.position,
			.vendor_id = _current_slave_info.vendor_id,
			.product_code = _current_slave_info.product_code,
			.index = index,
			.subindex = subindex,
			.offset = &e->domains[i].offset,
			.bit_position = &e->domains[i].bit_position
		};
	}

	return 0;
exit_bad_io:
	ECC_LOG_MSYPE("unexpected end of file while reading configuration data or I/O error");
	goto exit_fail;
exit_invalid_input_name_out_of_range:
	ECC_LOG_MSYPE("given entry name index is out of previously defined entry range");
	goto exit_ignore;
exit_invalid_input_domain_out_of_range:
	ECC_LOG_MSYPE("given domain id out of previously defined domain range");
	goto exit_ignore;
exit_too_many:
	ECC_LOG_MSYPE("sum of number of %s is larger than what was previously given",
		_next_unused_position_in_domain + ndomain > me->all_position_in_domains_count?
		"pdo entries in domains":"pdo entries");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	/* consume any input left */
	for (; read < ndomain; ++read)
		kscanf(_cfgin, "%*u");
	_ignore_current_master = true;
	return 0;
}

/*
 * Finalize slave configuration
 *
 * No input
 */
static int _configure_finalize_slave(void)
{
	ecc_master *m;
	ec_slave_config_t *slave_config;

	if (_wait_command(ECC_COMMAND_FINALIZE_SLAVE))
		goto exit_fail;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];

	/* sanity check */
	if (_sync_infos_count != _next_unused_sync_info
		|| _pdo_infos_count != _next_unused_pdo_info
		|| _pdo_entry_infos_count != _next_unused_pdo_entry_info)
		goto exit_too_few;

	ECC_DBG_MS("finalizing slave configuration");

	/* request configuration */
	slave_config = ecrt_master_slave_config(m->master, _current_slave_info.alias,
				_current_slave_info.position, _current_slave_info.vendor_id,
				_current_slave_info.product_code);
	if (slave_config == NULL)
		goto exit_no_config;

	/* set configuration */
	if (ecrt_slave_config_pdos(slave_config, _sync_infos_count, _sync_infos))
		goto exit_bad_config;

	return 0;
exit_too_few:
	ECC_LOG_MS("mismatch between sum of number of %s and what was previously given",
		_sync_infos_count != _next_unused_sync_info?"syncs":
		_pdo_infos_count != _next_unused_pdo_info?"pdos":"pdo entries");
	goto exit_ignore;
exit_no_config:
	ECC_LOG_MS("failed to request configuration");
	goto exit_ignore;
exit_bad_config:
	ECC_LOG_MS("failed to configure slave");
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

/*
 * Register domains
 *
 * No input
 */
static int _configure_register_domains(void)
{
	unsigned int i;
	ecc_master *m;
	master_extra *me;

	if (_wait_command(ECC_COMMAND_REGISTER_DOMAINS))
		goto exit_fail;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];

	/* sanity check */
	for (i = 0; i < _domain_infos_count; ++i)
	{
		domain_config *dc = &_domain_infos[i];

		if (dc->pdo_entries_count != dc->next_unused_pdo_entry)
			goto exit_too_few;
	}

	ECC_DBG_M("registering domains");

	for (i = 0; i < _domain_infos_count; ++i)
	{
		ecc_domain *d = &m->domains[i];
		domain_config *dc = &_domain_infos[i];

		/* terminate the list of entries with one having index 0 */
		dc->pdo_entries[dc->pdo_entries_count].index = 0;

		/* register the domain */
		if (ecrt_domain_reg_pdo_entry_list(d->domain, dc->pdo_entries))
			goto exit_bad_reg;
	}

	return 0;
exit_too_few:
	ECC_LOG_M("mismatch between sum of number of pdo entries in domains and what was previously given");
	goto exit_ignore;
exit_bad_reg:
	ECC_LOG_M("failed to register domain %u", i);
	goto exit_ignore;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

/*
 * Activate master
 *
 * No input
 */
static int _configure_activate_master(void)
{
	unsigned int i;
	ecc_master *m;
	master_extra *me;

	if (_wait_command(ECC_COMMAND_ACTIVATE_MASTER))
		goto exit_fail;

	if (_ignore_current_master)
		goto exit_ignore;

	m = &ecc_masters[_current_master];
	me = &_masters_extra[_current_master];

	ECC_DBG_MS("activating master");

	/* activate the master */
	if (ecrt_master_activate(m->master))
		goto exit_bad_activate;

	for (i = 0; i < m->domains_count; ++i)
	{
		ecc_domain *d = &m->domains[i];

		/* get the process data that the master allocated */
		d->data = ecrt_domain_data(d->domain);
		if (d->data == NULL)
			goto exit_no_mem;
	}

	/* make the master ready to use */
	m->ready = true;

	return 0;
exit_bad_activate:
	ECC_LOG_M("failed to activate master");
	goto exit_ignore;
exit_no_mem:
	ECC_LOG("out of memory");
	goto exit_fail;
exit_fail:
	return -1;
exit_ignore:
	_ignore_current_master = true;
	return 0;
}

void _configuration_reset(void)
{
	if (_cfgin)
		kclearerr(_cfgin);
	_current_master		= 0;
	_ignore_current_master	= false;
	_current_slave		= 0;
	_current_sync		= 0;
	_current_pdo		= 0;
	_current_pdo_entry	= 0;
	_current_domain		= 0;
	_current_domain_entry	= 0;
}

void _configuration_cleanup(void)
{
	if (_domain_infos)
	{
		unsigned int i;

		for (i = 0; i < _domain_infos_mem_size; ++i)
			CLEAN_FREE(_domain_infos[i].pdo_entries);
	}
	CLEAN_FREE(_sync_infos);
	CLEAN_FREE(_pdo_infos);
	CLEAN_FREE(_pdo_entry_infos);
	CLEAN_FREE(_domain_infos);
	_sync_infos_mem_size = 0;
	_pdo_infos_mem_size = 0;
	_pdo_entry_infos_mem_size = 0;
	_domain_infos_mem_size = 0;
	_sync_infos_count = 0;
	_pdo_infos_count = 0;
	_pdo_entry_infos_count = 0;
	_domain_infos_count = 0;
}

static void _cleanup(void)
{
	unsigned int m;

	for (m = 0; m < ecc_masters_count; ++m)
	{
		/* domain `data` is freed by ecrt_master_deactivate */
		CLEAN_FREE(_masters_extra[m].all_slaves);
		CLEAN_FREE(_masters_extra[m].all_syncs);
		CLEAN_FREE(_masters_extra[m].all_pdos);
		CLEAN_FREE(_masters_extra[m].all_pdo_entries);
		CLEAN_FREE(_masters_extra[m].all_position_in_domains);
		CLEAN_FREE(_masters_extra[m].all_domains);
		CLEAN_FREE(_masters_extra[m].all_domain_entries);

		CLEAN_FREE(ecc_masters[m].slave_names);
		CLEAN_FREE(ecc_masters[m].entry_names);

		if (ecc_masters[m].master != NULL)
			rt_sem_delete(&_masters_extra[m].mutex);
	}
	CLEAN_FREE(ecc_masters);
	CLEAN_FREE(_masters_extra);
	ecc_masters_count = 0;
}

static int _configure_ethercat_network(void)
{
	int ret = 0;

	_configuration_reset();

	/* get the master counts */
	if (_configure_masters())
		goto exit_fail;

	for (_current_master = 0; _current_master < ecc_masters_count; ++_current_master)
	{
		ecc_master *master = &ecc_masters[_current_master];

		/* get slave and entry names */
		if (_configure_slave_names() || _configure_entry_names())
			goto exit_fail;

		/* get domain sizes */
		if (_configure_domains())
			goto exit_fail;

		/* get domain entries */
		for (_current_domain = 0; _current_domain < master->domains_count; ++_current_domain)
			if (_configure_add_domain_entries())
				goto exit_fail;

		/* get slave sizes, their pdos and their entries */
		if (_configure_slaves())
			goto exit_fail;

		/* get slaves */
		for (_current_slave = 0; _current_slave < master->slaves_count; ++_current_slave)
		{
			ecc_slave *slave = &master->slaves[_current_slave];

			if (_configure_add_slave())
				goto exit_fail;

			/* get slave syncs */
			for (_current_sync = 0; _current_sync < slave->syncs_count; ++_current_sync)
			{
				ecc_sync *sync = &slave->syncs[_current_sync];

				if (_configure_add_sync())
					goto exit_fail;

				/* get sync pdos */
				for (_current_pdo = 0; _current_pdo < sync->pdos_count; ++_current_pdo)
				{
					ecc_pdo *pdo = &sync->pdos[_current_pdo];

					if (_configure_add_pdo())
						goto exit_fail;

					/* get pdo entries */
					for (_current_pdo_entry = 0; _current_pdo_entry < pdo->entries_count; ++_current_pdo_entry)
						if (_configure_add_pdo_entry())
							goto exit_fail;
				}
			}

			/* finalize slave */
			if (_configure_finalize_slave())
				goto exit_fail;
		}

		/* register domains */
		if (_configure_register_domains())
			goto exit_fail;

		/* activate the master */
		if (_configure_activate_master())
			goto exit_fail;
	}
	if (_init_thread_must_exit)
		ECC_DBG("configuration halted because the module is exiting");

	goto exit_normal;
exit_fail:
	ret = -1;
exit_normal:
	_configuration_cleanup();
	return ret;
}

static int _ec_config_init_thread(void *arg)
{
	int res = 0;

	_cfgin = kopen(ECC_PROCFS_FILE_NAME, "pr");
	if (_cfgin == NULL)
		goto exit_no_mem;

	while (!_init_thread_must_exit && _configure_ethercat_network())
	{
		_cleanup();
		ECC_LOG("retry configuration (error above)");
	}

	goto exit_normal;
exit_no_mem:
	ECC_LOG("out of memory");
	goto exit_fail;
exit_fail:
	res = -1;
exit_normal:
	if (_cfgin)
		kclose(_cfgin);
	_cfgin = NULL;
	_init_thread_exited = true;
	return res;
}

static int __init ecconfig_init(void)
{
	ECC_LOG("ecconfig intializing...");

	_init_thread_exited = false;
	if ((_init_thread_task = kthread_run(_ec_config_init_thread, NULL, "ecconfig_init_thread")) == ERR_PTR(-ENOMEM))
	{
		_init_thread_task = NULL;
		_init_thread_exited = true;
		ECC_LOG("error creating the initial thread");
		return -ENOMEM;
	}

	ECC_LOG("ecconfig intializing...done");

	return 0;
}

static void __exit ecconfig_exit(void)
{
	unsigned int m;

	ECC_LOG("ecconfig exiting...");

	/* close the input stream */
	if (_cfgin)
		kclose(_cfgin);
	_cfgin = NULL;

	/* wait for init thread to stop */
	_init_thread_must_exit = true;
	while (!_init_thread_exited)
	{
		set_current_state(TASK_INTERRUPTIBLE);
		msleep(10);
	}

	/* release the masters */
	for (m = 0; m < ecc_masters_count; ++m)
		if (ecc_masters[m].master)
		{
			ecrt_master_deactivate(ecc_masters[m].master);
			ecrt_release_master(ecc_masters[m].master);
		}

	/* free memory */
	_cleanup();

	ECC_LOG("ecconfig exiting...done");
}

module_init(ecconfig_init);
module_exit(ecconfig_exit);
