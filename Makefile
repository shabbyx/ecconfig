# Targets:
# all: makes the kernel-module, user-program (feeder), the test and the documentation
# config: convert Makefile.config configuration to config headers
# module: makes only the kernel-module
# feeder: makes only the feeder
# test: makes only the test
# doc: makes only the documentation
# install: installs the software
# uninstall: uninstalls the software

.PHONY: all
all: module feeder test doc
.PHONY: config
config:
	@$(MAKE) --no-print-directory -f Makefile.headers config_headers
.PHONY: module feeder test doc
module:
	@$(MAKE) --no-print-directory -C kernel-module
feeder:
	@$(MAKE) --no-print-directory -C user-program
test:
	@$(MAKE) --no-print-directory -C test
doc:
	@$(MAKE) --no-print-directory -C documentation
.PHONY: install uninstall
install uninstall:
	@$(MAKE) --no-print-directory -f Makefile.install $@
.PHONY: clean
clean:
	-@$(MAKE) --no-print-directory -C kernel-module clean
	-@$(MAKE) --no-print-directory -C user-program clean
	-@$(MAKE) --no-print-directory -C test clean
	-@$(MAKE) --no-print-directory -C documentation clean
