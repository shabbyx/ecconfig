/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_INPUT_H
#define CONFIG_INPUT_H

#include <stdint.h>
#include <stdbool.h>

/*
 * Structure of the configuration file:
 *
 * The configuration file is an XML file, containing the tag <ecconfig>.  Within
 * this tag, domains are specified by <domain> tags.  Inside the domains, a series
 * of slaves are introduced.  Each slave is identified by the tag <slave> and the
 * property `name` which corresponds to the group of slaves as stored in the database.
 *
 * Each slave has <input> and <output> tags, inside which is a string.  This string
 * is used to match PDO entries of that slave and assign those entries to the domain
 * in which the slave was mentioned.
 *
 * Note: the write of the config file consists of first backing up the previous file,
 * writing the configuration and then deleting the backup.  If anything goes wrong, you
 * can always retrieve the last config file from the backup.
 */

typedef struct ecc_config_slave
{
	char *name;
	char **inputs;				/* sorted */
	uint32_t inputs_count;
	char **outputs;				/* sorted */
	uint32_t outputs_count;

	uint32_t inputs_mem_size;
	uint32_t outputs_mem_size;
} ecc_config_slave;

typedef struct ecc_config_domain
{
	uint32_t id;				/* index in domains of ecc_config */
	struct ecc_config_slave *slaves;	/* sorted by name */
	uint32_t slaves_count;

	uint32_t slaves_mem_size;
} ecc_config_domain;

typedef struct ecc_config
{
	struct ecc_config_domain *domains;
	uint32_t domains_count;

	uint32_t domains_mem_size;
} ecc_config;

void ecc_init_config(ecc_config *config);
void ecc_free_config(ecc_config *config);

/*
 * configuration access
 *
 * read: read the configuration
 * write: backup the configuration, write the new one and remove the backup
 *        This function only rewrites the <ecconfig> tag and keeps the rest
 *        of the file intact.
 * saveas: change configuration filename and call write
 */
int ecc_read_config(ecc_config *conf);
int ecc_write_config(const ecc_config *conf, bool force);
int ecc_saveas_config(const ecc_config *conf, const char *filename);

/*
 * operations on the configuration
 *
 * lookup_slave: binary search for slave name
 * lookup_domain: linear search in domains and binary search for slave name,
 * 		but return the domain
 * lookup_domain_next: get the next domain with the same name
 * modify: add or modify a slave in the config
 * remove: remove an entry from the config
 * remove_slave: remove a whole slave from the config
 * configure: send configuration data to the kernel
 */
ecc_config_slave *ecc_lookup_config_slave(const ecc_config *conf, uint32_t domain,
		const char *name);
ecc_config_domain *ecc_lookup_config_domain(const ecc_config *conf, const char *name);
ecc_config_domain *ecc_lookup_config_domain_next(const ecc_config *conf,
		const ecc_config_domain *cur, const char *name);
int ecc_add_entry_to_config(ecc_config *conf, uint32_t domain, const char *slave,
		const char *entry_name, bool is_input);
int ecc_remove_from_config(ecc_config *conf, uint32_t domain, const char *slave,
		const char *entry_name, bool is_input);
int ecc_remove_slave_from_config(ecc_config *conf, uint32_t domain, const char *slave);

/*
 * Debug function: Logs the configuration using LOG macro
 */
void ecc_log_config(ecc_config *conf);

#endif
