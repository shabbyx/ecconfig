/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#ifndef NDEBUG
# include <time.h>
#endif
#include "configure.h"
#include "network.h"
#include "database.h"
#include "config_input.h"
#include "log.h"
#include "last_error.h"

static char _unknown_config_name[] = "UNKNOWN";
static char *_unknown_config_io = _unknown_config_name;
static ecc_config_slave _unknown_config_slave = {
	.name = _unknown_config_name,
	.inputs = &_unknown_config_io,
	.inputs_count = 1,
	.outputs = &_unknown_config_io,
	.outputs_count = 1
};
static ecc_config_domain _unknown_config_domain = {
	.id = (uint32_t)-1,
	.slaves = &_unknown_config_slave,
	.slaves_count = 1
};

typedef struct ecc_conf_domain_entry
{
	const char *slave_name;		/* temp, while slaves are being inserted, can't keep index */
	const char *entry_name;		/* temp, while entries are being inserted, can't keep index */

	uint32_t slave;			/* index to slave_names of ecc_conf_master */
	uint32_t entry;			/* index to entry_names of ecc_conf_master */
} ecc_conf_domain_entry;

typedef struct ecc_conf_domain
{
	ecc_config_domain *domain;	/* reference to domain in ecc_config */

	ecc_conf_domain_entry *entries;	/* sorted on slave then entry */
	uint32_t entries_count;
	uint32_t entries_mem_size;

	uint32_t pdo_entries_count;	/* number of pdo entries in this domain */
} ecc_conf_domain;

typedef struct ecc_conf_pdo_entry
{
	uint32_t name;			/* index to entry_names in ecc_conf_master */
	ecc_pdo_entry *pdo_entry;

	uint32_t *domains;
	uint32_t domains_count;
	uint32_t domains_mem_size;
} ecc_conf_pdo_entry;

typedef struct ecc_conf_pdo
{
	ecc_pdo *pdo;

	ecc_conf_pdo_entry *entries;
	uint32_t entries_count;
} ecc_conf_pdo;

typedef struct ecc_conf_sync
{
	ecc_sync *sync;

	ecc_conf_pdo *pdos;
	uint32_t pdos_count;
} ecc_conf_sync;

typedef struct ecc_conf_slave
{
	uint32_t name;			/* index to slave_names in ecc_conf_master */
	ecc_slave *slave;

	ecc_conf_sync *syncs;
	uint32_t syncs_count;
} ecc_conf_slave;

typedef struct ecc_conf_master
{
	ecc_master *master;		/* reference to master in ecc_network */

	ecc_conf_domain *domains;
	uint32_t domains_count;
	uint32_t domains_mem_size;

	const char **slave_names;	/* sorted, pointers to names in ecc_database */
	const char **entry_names;	/*
					 * sorted, pointers to inputs/outputs of slaves
					 * of domains of ecc_config
					 */
	uint32_t slave_names_count;
	uint32_t entry_names_count;
	uint32_t slave_names_mem_size;
	uint32_t entry_names_mem_size;
	bool has_unknown_slave_name;
	bool has_unknown_entry_name;

	ecc_conf_slave *slaves;		/* sorted on position */
	uint32_t slaves_count;
} ecc_conf_master;

typedef struct ecc_conf
{
	ecc_conf_master *masters;	/* sorted on index of ecc_master (done when loading options) */
	uint32_t masters_count;
} ecc_conf;

static void _free_domain(ecc_conf_domain *d)
{
	free(d->entries);

	*d = (ecc_conf_domain){0};
}

static void _free_pdo_entry(ecc_conf_pdo_entry *e)
{
	free(e->domains);

	*e = (ecc_conf_pdo_entry){0};
}

static void _free_pdo(ecc_conf_pdo *p)
{
	unsigned int i;

	if (p->entries)
		for (i = 0; i < p->entries_count; ++i)
			_free_pdo_entry(&p->entries[i]);
	free(p->entries);

	*p = (ecc_conf_pdo){0};
}

static void _free_sync(ecc_conf_sync *s)
{
	unsigned int i;

	if (s->pdos)
		for (i = 0; i < s->pdos_count; ++i)
			_free_pdo(&s->pdos[i]);
	free(s->pdos);

	*s = (ecc_conf_sync){0};
}

static void _free_slave(ecc_conf_slave *s)
{
	unsigned int i;

	if (s->syncs)
		for (i = 0; i < s->syncs_count; ++i)
			_free_sync(&s->syncs[i]);
	free(s->syncs);

	*s = (ecc_conf_slave){0};
}

static void _free_master(ecc_conf_master *m)
{
	unsigned int i;

	if (m->domains)
		for (i = 0; i < m->domains_count; ++i)
			_free_domain(&m->domains[i]);
	if (m->slaves)
		for (i = 0; i < m->slaves_count; ++i)
			_free_slave(&m->slaves[i]);
	free(m->domains);
	free(m->slaves);
	free(m->slave_names);
	free(m->entry_names);

	*m = (ecc_conf_master){0};
}

static void _free_conf(ecc_conf *c)
{
	unsigned int i;

	if (c->masters)
		for (i = 0; i < c->masters_count; ++i)
			_free_master(&c->masters[i]);
	free(c->masters);

	*c = (ecc_conf){0};
}

static int _find_matching_name(const char *name, char **names, unsigned int count)
{
	unsigned int i;

	for (i = 0; i < count; ++i)
		if (strstr(name, names[i]) != NULL)
			return i;

	return -1;
}

static const char *_find_entry_name(ecc_config *conf, const char *slave_name,
		const char *entry_name, bool is_output)
{
	ecc_config_domain *d;
	ecc_config_slave *s;
	uint32_t d_index = 0;
	int ret;

	d = ecc_lookup_config_domain(conf, slave_name);
	while (d != NULL)
	{
		d_index = d - conf->domains;
		s = ecc_lookup_config_slave(conf, d_index, slave_name);
		if (s == NULL)
			goto exit_internal;

		if (is_output)
		{
			ret = _find_matching_name(entry_name, s->outputs, s->outputs_count);
			if (ret >= 0)
				return s->outputs[ret];
		}
		else
		{
			ret = _find_matching_name(entry_name, s->inputs, s->inputs_count);
			if (ret >= 0)
				return s->inputs[ret];
		}

		d = ecc_lookup_config_domain_next(conf, d, slave_name);
	}
	return NULL;
exit_internal:
	LOG("internal error: a domain was found (index %u) that contains slave name \"%s\", "
		"but searching the domain itself for the same name yielded no results",
		d_index, slave_name);
	ecc_error = ECC_ERROR_INTERNAL;
	return NULL;
}

static int _add_name(const char ***names, uint32_t *count, uint32_t *mem_size, const char *name)
{
	unsigned int i, j;

	for (i = 0; i < *count; ++i)
	{
		int ret = strcmp(name, (*names)[i]);
		if (ret == 0)
			return 0;
		if (ret < 0)
			break;
	}

	if (*count >= *mem_size)
	{
		const char **enlarged;
		uint32_t new_size;

		new_size = *mem_size * 2 + 1;
		enlarged = realloc(*names, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		*names = enlarged;
		*mem_size = new_size;
	}

	for (j = *count; j > i; --j)
		(*names)[j] = (*names)[j - 1];

	(*names)[i] = name;
	++*count;

	return 0;
}

static inline int _add_slave_name(ecc_conf_master *m, const char *slave_name)
{
	return _add_name(&m->slave_names, &m->slave_names_count, &m->slave_names_mem_size, slave_name);
}

static inline int _add_entry_name(ecc_conf_master *m, const char *entry_name)
{
	return _add_name(&m->entry_names, &m->entry_names_count, &m->entry_names_mem_size, entry_name);
}

static int _add_domain_to_pdo_entry(ecc_conf_domain *d, ecc_conf_pdo_entry *e, uint32_t d_id)
{
	/* add domain id to pdo entry */
	if (e->domains_count >= e->domains_mem_size)
	{
		uint32_t *enlarged;
		uint32_t new_size;

		new_size = e->domains_mem_size * 2 + 1;
		enlarged = realloc(e->domains, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		e->domains = enlarged;
		e->domains_mem_size = new_size;
	}

	e->domains[e->domains_count] = d_id;
	++e->domains_count;

	/* increment the number of pdo entries in domain */
	++d->pdo_entries_count;

	return 0;
}

/* this function does not check whether d has already been added */
static int _add_domain(ecc_conf_master *m, ecc_config_domain *d)
{
	if (m->domains_count >= m->domains_mem_size)
	{
		ecc_conf_domain *enlarged;
		uint32_t new_size;

		new_size = m->domains_mem_size * 2 + 1;
		enlarged = realloc(m->domains, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		m->domains = enlarged;
		m->domains_mem_size = new_size;
	}

	m->domains[m->domains_count] = (ecc_conf_domain){
		.domain = d
	};
	++m->domains_count;

	return 0;
}

static int _domainentrycmp(const void *e1, const void *e2)
{
	const ecc_conf_domain_entry *first = e1;
	const ecc_conf_domain_entry *second = e2;

	if (first->slave < second->slave)
		return -1;
	if (first->slave > second->slave)
		return 1;
	if (first->entry < second->entry)
		return -1;
	if (first->entry > second->entry)
		return 1;
	return 0;
}

static int _add_domain_entry_helper(ecc_conf_domain *d, const char *s, const char *e)
{
	unsigned int i;

	/*
	 * check if it already exists.
	 *
	 * note that since `s` and `e` are pointers to strings found elsewhere,
	 * there is no need for strcmp
	 */
	for (i = 0; i < d->entries_count; ++i)
		if (d->entries[i].slave_name == s && d->entries[i].entry_name == e)
			return 1;

	if (d->entries_count >= d->entries_mem_size)
	{
		ecc_conf_domain_entry *enlarged;
		uint32_t new_size;

		new_size = d->entries_mem_size * 2 + 1;
		enlarged = realloc(d->entries, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		d->entries = enlarged;
		d->entries_mem_size = new_size;
	}

	d->entries[d->entries_count] = (ecc_conf_domain_entry){
		.slave_name = s,
		.entry_name = e
	};
	++d->entries_count;

	return 0;
}

static int _add_domain_entry(ecc_conf_domain *d, const char *s, const char *e)
{
	int ret;

	ret = _add_domain_entry_helper(d, s, e);

	return ret == -1?-1:0;
}

static int _add_domain_entry_by_id(ecc_conf_domain *d, uint32_t s_index, uint32_t e_index,
		const char *s, const char *e)
{
	int ret;

	ret = _add_domain_entry_helper(d, s, e);

	/* if failed, or already exists, return */
	if (ret)
		return ret == -1?-1:0;

	d->entries[d->entries_count - 1].slave = s_index;
	d->entries[d->entries_count - 1].entry = e_index;

	return 0;
}

static int _namecmp(const void *e1, const void *e2)
{
	return strcmp(*(const char * const *)e1, *(const char * const *)e2);
}

/*
 * this function adds domain matching slave-entry names to be configured.
 * at the same time, it adds those domains to the PDO entry
 */
static int _add_domains(ecc_config *conf, ecc_conf_master *conf_master, ecc_conf_pdo_entry *e,
		const char *slave_name, const char *domain_entry_name, bool is_output)
{
	unsigned int i;
	ecc_config_domain *d;
	ecc_config_slave *s;
	uint32_t d_index = 0;

	d = ecc_lookup_config_domain(conf, slave_name);
	while (d != NULL)
	{
		d_index = d - conf->domains;
		s = ecc_lookup_config_slave(conf, d_index, slave_name);
		if (s == NULL)
			goto exit_internal_no_slave;

		/* check if this domain has the entry of interest */
		if ((is_output && bsearch(&domain_entry_name, s->outputs, s->outputs_count, sizeof(*s->outputs), _namecmp) != NULL)
			|| (!is_output && bsearch(&domain_entry_name, s->inputs, s->inputs_count, sizeof(*s->inputs), _namecmp) != NULL))
		{
			ecc_conf_domain *conf_domain;
			uint32_t conf_domain_id;

			/* check if this domain has already been added */
			for (i = 0; i < conf_master->domains_count; ++i)
				if (conf_master->domains[i].domain == d)
					break;

			/* if not, add it */
			if (i == conf_master->domains_count)
			{
				if (_add_domain(conf_master, d))
					goto exit_no_mem;

				conf_domain_id = conf_master->domains_count - 1;
			}
			else
				conf_domain_id = i;

			conf_domain = &conf_master->domains[conf_domain_id];

			/* add this domain to the given PDO entry */
			if (_add_domain_to_pdo_entry(conf_domain, e, conf_domain_id))
				goto exit_no_mem;

			/*
			 * add slave-entry pair to the domain
			 *
			 * since we are in the middle of adding slave and entry names,
			 * their indices can't be used
			 */
			if (_add_domain_entry(conf_domain, slave_name, domain_entry_name))
				goto exit_no_mem;
		}

		d = ecc_lookup_config_domain_next(conf, d, slave_name);
	}
	return 0;
exit_internal_no_slave:
	LOG("internal error: a domain was found (index %u) that contains slave name \"%s\", "
		"but searching the domain itself for the same name yielded no results",
		d_index, slave_name);
	ecc_error = ECC_ERROR_INTERNAL;
	goto exit_skip;
exit_skip:
	return 0;
exit_no_mem:
	return -1;
}

/*
 * Mirror the network with configuration structures,
 */
static int _build_slave_configuration(ecc_conf *c, ecc_network *network)
{
	unsigned int i_master, i_slave;
	unsigned int i_sync, i_pdo, i_pdo_entry;
	ecc_master *master;
	ecc_slave *slave;
	ecc_sync *sync;
	ecc_pdo *pdo;
	ecc_pdo_entry *pdo_entry;
	ecc_conf_master *m;
	ecc_conf_slave *s;
	ecc_conf_sync *y;
	ecc_conf_pdo *p;
	ecc_conf_pdo_entry *e;

#ifndef NDEBUG
	uint64_t t_diff;
	struct timespec t_beg, t_now;
	clock_gettime(CLOCK_MONOTONIC, &t_beg);
#endif

	c->masters = malloc(network->masters_count * sizeof(*c->masters));
	if (c->masters == NULL)
		goto exit_no_mem;
	c->masters_count = network->masters_count;

	for (i_master = 0; i_master < network->masters_count; ++i_master)
	{
		master = &network->masters[i_master];
		m = &c->masters[i_master];

		*m = (ecc_conf_master){0};

		m->master = master;
		m->slaves = malloc(master->slaves_count * sizeof(*m->slaves));
		if (m->slaves == NULL)
			goto exit_no_mem;
		m->slaves_count = master->slaves_count;

		for (i_slave = 0; i_slave < master->slaves_count; ++i_slave)
		{
			slave = &master->slaves[i_slave];
			s = &m->slaves[i_slave];

			s->slave = slave;
			s->syncs = malloc(slave->syncs_count * sizeof(*s->syncs));
			if (s->syncs == NULL)
				goto exit_no_mem;
			s->syncs_count = slave->syncs_count;

			for (i_sync = 0; i_sync < slave->syncs_count; ++i_sync)
			{
				sync = &slave->syncs[i_sync];
				y = &s->syncs[i_sync];

				y->sync = sync;
				y->pdos = malloc(sync->pdos_count * sizeof(*y->pdos));
				if (y->pdos == NULL)
					goto exit_no_mem;
				y->pdos_count = sync->pdos_count;

				for (i_pdo = 0; i_pdo < sync->pdos_count; ++i_pdo)
				{
					pdo = &sync->pdos[i_pdo];
					p = &y->pdos[i_pdo];

					p->pdo = pdo;
					p->entries = malloc(pdo->entries_count * sizeof(*p->entries));
					if (p->entries == NULL)
						goto exit_no_mem;
					p->entries_count = pdo->entries_count;

					for (i_pdo_entry = 0; i_pdo_entry < pdo->entries_count; ++i_pdo_entry)
					{
						pdo_entry = &pdo->entries[i_pdo_entry];
						e = &p->entries[i_pdo_entry];

						*e = (ecc_conf_pdo_entry){
							.pdo_entry = pdo_entry
						};
					}
				}
			}
		}
	}

#ifndef NDEBUG
	clock_gettime(CLOCK_MONOTONIC, &t_now);
	t_diff = t_now.tv_sec * 1000000000llu + t_now.tv_nsec
		- (t_beg.tv_sec * 1000000000llu + t_beg.tv_nsec);
	LOG("build slave config took %llu.%llums", t_diff / 1000000, t_diff % 1000000);
#endif

	return 0;
exit_no_mem:
	LOG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	return -1;
}

/*
 * this function should be called only after all slave/entry names have been added,
 * and only if there is a slave/entry without any database/configuration match
 */
static int _append_unknown_name(const char ***names, uint32_t *count, uint32_t *mem_size)
{
	if (*count >= *mem_size)
	{
		const char **enlarged;
		uint32_t new_size;

		new_size = *mem_size * 2 + 1;
		enlarged = realloc(*names, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		*names = enlarged;
		*mem_size = new_size;
	}

	(*names)[*count] = "UNKNOWN";
	/* *count is not incremented to keep this name hidden from bsearch */

	return 0;
}

static int _append_unknown_slave_name(ecc_conf_master *m)
{
	if (m->has_unknown_slave_name)
		return 0;
	m->has_unknown_slave_name = true;
	return _append_unknown_name(&m->slave_names, &m->slave_names_count, &m->slave_names_mem_size);
}

static int _append_unknown_entry_name(ecc_conf_master *m)
{
	if (m->has_unknown_entry_name)
		return 0;
	m->has_unknown_entry_name = true;
	return _append_unknown_name(&m->entry_names, &m->entry_names_count, &m->entry_names_mem_size);
}

static unsigned int _sort_unknown_name_common(const char **names, uint32_t count)
{
	unsigned int i, j;
	const char *unknown_name;

	/* find sorted position of "UNKNOWN" (which is the last name) */
	for (i = 0; i < count - 1; ++i)
		if (strcmp(names[count - 1], names[i]) < 0)
			break;
	/* put it in place and shift the others */
	unknown_name = names[count - 1];
	for (j = count - 1; j > i; --j)
		names[j] = names[j - 1];
	names[i] = unknown_name;

	/* return position of unknown name */
	return i;
}

static void _sort_slave_unknown_name(ecc_conf_master *m)
{
	unsigned int i_domain, i_entry;
	unsigned int i_slave;
	unsigned int unknown_name_original_pos;
	unsigned int unknown_name_pos;
	ecc_conf_domain *d;
	ecc_conf_domain_entry *de;
	ecc_conf_slave *s;

	if (!m->has_unknown_slave_name)
		return;

	/* put unknown name in its sorted location */
	unknown_name_original_pos = m->slave_names_count - 1;
	unknown_name_pos = _sort_unknown_name_common(m->slave_names, m->slave_names_count);

	/* fix slave name indices */
	for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
	{
		s = &m->slaves[i_slave];

		/* if s->name is unknown, it should contian the new index */
		if (s->name == unknown_name_original_pos)
			s->name = unknown_name_pos;
		/* if s->name is >= new unknown index, it is shifted during sort */
		else if (s->name >= unknown_name_pos)
			++s->name;
	}

	/* fix domain entry slave name indices */
	for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
	{
		d = &m->domains[i_domain];

		for (i_entry = 0; i_entry < d->entries_count; ++i_entry)
		{
			de = &d->entries[i_entry];

			/* same logic as above */
			if (de->slave == unknown_name_original_pos)
				de->slave = unknown_name_pos;
			else if (de->slave >= unknown_name_pos)
				++de->slave;
			/*
			 * NOTE: slave_name and entry_name are not fixed because they
			 * were temporary anyway and they are not used after this point
			 * */
		}
	}
}

static void _sort_entry_unknown_name(ecc_conf_master *m)
{
	unsigned int i_domain, i_entry;
	unsigned int i_slave, i_sync, i_pdo, i_pdo_entry;
	unsigned int unknown_name_original_pos;
	unsigned int unknown_name_pos;
	ecc_conf_domain *d;
	ecc_conf_domain_entry *de;
	ecc_conf_slave *s;
	ecc_conf_sync *y;
	ecc_conf_pdo *p;
	ecc_conf_pdo_entry *e;

	if (!m->has_unknown_entry_name)
		return;

	/* put unknown name in its sorted location */
	unknown_name_original_pos = m->entry_names_count - 1;
	unknown_name_pos = _sort_unknown_name_common(m->entry_names, m->entry_names_count);

	/* fix pdo entry name indices */
	for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
	{
		s = &m->slaves[i_slave];

		for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
		{
			y = &s->syncs[i_sync];

			for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
			{
				p = &y->pdos[i_pdo];

				for (i_pdo_entry = 0; i_pdo_entry < p->entries_count; ++i_pdo_entry)
				{
					e = &p->entries[i_pdo_entry];

					/* if e->name is unknown, it should contian the new index */
					if (e->name == unknown_name_original_pos)
						e->name = unknown_name_pos;
					/* if e->name is >= new unknown index, it is shifted during sort */
					else if (e->name >= unknown_name_pos)
						++e->name;
				}
			}
		}
	}

	/* fix domain entry entry name indices */
	for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
	{
		d = &m->domains[i_domain];

		for (i_entry = 0; i_entry < d->entries_count; ++i_entry)
		{
			de = &d->entries[i_entry];

			/* same logic as above */
			if (de->entry == unknown_name_original_pos)
				de->entry = unknown_name_pos;
			else if (de->entry >= unknown_name_pos)
				++de->entry;
			/*
			 * NOTE: slave_name and entry_name are not fixed because they
			 * were temporary anyway and they are not used after this point
			 * */
		}
	}
}

/*
 * Match pdo entry names from the network with that specified in the configuration
 * file.  Store only those slave and entry groups that are actually present in
 * the network.
 *
 * In the meantime, add domains that have a matching pdo entry to the configuration
 * and store helpful data in pdo entries to ease their matching at a later stage.
 */
static int _build_domain_configuration(ecc_conf *c, ecc_config *conf, ecc_database *db)
{
	unsigned int i_master, i_domain, i_domain_entry;
	unsigned int i_slave, i_sync, i_pdo, i_pdo_entry;
	ecc_conf_master *m;
	ecc_conf_domain *d;
	ecc_conf_domain_entry *de;
	ecc_conf_slave *s;
	ecc_conf_sync *y;
	ecc_conf_pdo *p;
	ecc_conf_pdo_entry *e;
	ecc_conf_domain *unknown_domain;
	uint32_t unknown_domain_id;

#ifndef NDEBUG
	uint64_t t_diff;
	uint64_t t_diff_add_domains = 0;
	uint64_t t_diff_name_ids = 0;
	uint64_t t_diff_add_unknown = 0;
	uint64_t t_diff_unknown_sort = 0;
	struct timespec t_beg, t_part, t_now;
	clock_gettime(CLOCK_MONOTONIC, &t_beg);
#endif

	for (i_master = 0; i_master < c->masters_count; ++i_master)
	{
		bool needs_unknown_domain = false;

#ifndef NDEBUG
		clock_gettime(CLOCK_MONOTONIC, &t_part);
#endif

		m = &c->masters[i_master];

		for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
		{
			const char *slave_name;

			s = &m->slaves[i_slave];

			if (s->slave->db_entry == NULL)
			{
				LOG("warning: slave with alias: %u and position: %u "
					"is not mentioned in configuration file",
					s->slave->alias, s->slave->position);
				LOG("         its entries will be added to a special domain");
				needs_unknown_domain = true;
				continue;
			}
			slave_name = db->names[s->slave->db_entry->name];
			/*
			 * keep all slave names, because they are useful for configuration
			 * even if they are not mentioned in configuration file
			 */
			if (_add_slave_name(m, slave_name))
				goto exit_no_mem;

			for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
			{
				y = &s->syncs[i_sync];

				for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
				{
					p = &y->pdos[i_pdo];

					for (i_pdo_entry = 0; i_pdo_entry < p->entries_count; ++i_pdo_entry)
					{
						const char *domain_entry_name = NULL;

						e = &p->entries[i_pdo_entry];

						/* ignore padding entries */
						if (e->pdo_entry->index == 0)
							continue;

						domain_entry_name = _find_entry_name(conf, slave_name,
								e->pdo_entry->name, y->sync->is_output);
						if (domain_entry_name != NULL)
						{
							/* add the name of this entry */
							if (_add_entry_name(m, domain_entry_name))
								goto exit_no_mem;

							/*
							 * the domains containing this slave-entry
							 * combination are also of interest
							 */
							if (_add_domains(conf, m, e, slave_name,
									domain_entry_name, y->sync->is_output))
								goto exit_no_mem;
						}
						else
						{
							LOG("warning: entry of slave with alias: %u and "
								"position %u with name \"%s\" is not "
								"mentioned in the configuration file",
								s->slave->alias, s->slave->position, e->pdo_entry->name);
							LOG("         it will be added to a special domain");
							needs_unknown_domain = true;
						}
					}
				}
			}
		}

#ifndef NDEBUG
		clock_gettime(CLOCK_MONOTONIC, &t_now);
		t_diff_add_domains += t_now.tv_sec * 1000000000llu + t_now.tv_nsec
			- (t_part.tv_sec * 1000000000llu + t_part.tv_nsec);
		clock_gettime(CLOCK_MONOTONIC, &t_part);
#endif

		/* convert all slave_name and entry_names added in domains to their indices */
		for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
		{
			d = &m->domains[i_domain];

			for (i_domain_entry = 0; i_domain_entry < d->entries_count; ++i_domain_entry)
			{
				const char **n;

				de = &d->entries[i_domain_entry];

				n = bsearch(&de->slave_name, m->slave_names, m->slave_names_count,
				       sizeof(*m->slave_names), _namecmp);
				if (n == NULL)
					goto exit_internal_no_slave_name;
				de->slave = n - m->slave_names;

				n = bsearch(&de->entry_name, m->entry_names, m->entry_names_count,
				       sizeof(*m->entry_names), _namecmp);
				if (n == NULL)
					goto exit_internal_no_domain_entry_name;
				de->entry = n - m->entry_names;

			}

			qsort(d->entries, d->entries_count, sizeof(*d->entries), _domainentrycmp);
		}

#ifndef NDEBUG
		clock_gettime(CLOCK_MONOTONIC, &t_now);
		t_diff_name_ids += t_now.tv_sec * 1000000000llu + t_now.tv_nsec
			- (t_part.tv_sec * 1000000000llu + t_part.tv_nsec);
		clock_gettime(CLOCK_MONOTONIC, &t_part);
#endif

		/* rescan, and this time add the unmentioned slaves/pdo entries to a new domain */
		if (!needs_unknown_domain)
			continue;

		_unknown_config_domain.id = m->domains_count;
		if (_add_domain(m, &_unknown_config_domain))
			goto exit_no_mem;
		unknown_domain_id = m->domains_count - 1;
		unknown_domain = &m->domains[unknown_domain_id];

		for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
		{
			uint32_t slave_name_id;
			const char *slave_name;

			s = &m->slaves[i_slave];

			if (s->slave->db_entry == NULL)
			{
				if (_append_unknown_slave_name(m))
					goto exit_no_mem;
				slave_name_id = m->slave_names_count;
			}
			else
			{
				const char **sn;

				sn = bsearch(&db->names[s->slave->db_entry->name], m->slave_names,
						m->slave_names_count, sizeof(*m->slave_names), _namecmp);
				if (sn == NULL)
					goto exit_internal_no_db_slave_name;
				slave_name_id = sn - m->slave_names;
			}
			slave_name = m->slave_names[slave_name_id];
			s->name = slave_name_id;

			for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
			{
				y = &s->syncs[i_sync];

				for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
				{
					p = &y->pdos[i_pdo];

					for (i_pdo_entry = 0; i_pdo_entry < p->entries_count; ++i_pdo_entry)
					{
						uint32_t entry_name_id;
						const char *entry_name;

						e = &p->entries[i_pdo_entry];

						/* ignore padding entries */
						if (e->pdo_entry->index == 0)
							continue;

						/* if entry is already in some domain, then skip it */
						if (s->slave->db_entry != NULL && e->domains != NULL)
						{
							if (m->domains[e->domains[0]].entries == NULL)
								goto exit_internal_domain_with_no_entry;
							e->name = m->domains[e->domains[0]].entries[0].entry;
							continue;
						}

						/* add slave-entry to domain */
						if (_append_unknown_entry_name(m))
							goto exit_no_mem;
						entry_name_id = m->entry_names_count;
						entry_name = m->entry_names[entry_name_id];

						if (_add_domain_entry_by_id(unknown_domain, slave_name_id, entry_name_id,
									slave_name, entry_name))
							goto exit_no_mem;

						if (_add_domain_to_pdo_entry(unknown_domain, e, unknown_domain_id))
							goto exit_no_mem;

						e->name = entry_name_id;
					}
				}
			}
		}

#ifndef NDEBUG
		clock_gettime(CLOCK_MONOTONIC, &t_now);
		t_diff_add_unknown += t_now.tv_sec * 1000000000llu + t_now.tv_nsec
			- (t_part.tv_sec * 1000000000llu + t_part.tv_nsec);
		clock_gettime(CLOCK_MONOTONIC, &t_part);
#endif

		/*
		 * unknown names were hidden until now, for bsearch to ignore them.
		 * however, to include them in the configuration, they are now revealed
		 */
		if (m->has_unknown_slave_name)
			++m->slave_names_count;
		if (m->has_unknown_entry_name)
			++m->entry_names_count;

		/*
		 * finally, move the "UNKNOWN" name to its sorted position,
		 * fixing the indices of slaves or entries
		 */
		_sort_slave_unknown_name(m);
		_sort_entry_unknown_name(m);

#ifndef NDEBUG
		clock_gettime(CLOCK_MONOTONIC, &t_now);
		t_diff_unknown_sort += t_now.tv_sec * 1000000000llu + t_now.tv_nsec
			- (t_part.tv_sec * 1000000000llu + t_part.tv_nsec);
#endif

	}

#ifndef NDEBUG
	clock_gettime(CLOCK_MONOTONIC, &t_now);
	t_diff = t_now.tv_sec * 1000000000llu + t_now.tv_nsec
		- (t_beg.tv_sec * 1000000000llu + t_beg.tv_nsec);
	LOG("build domain config took %llu.%llums", t_diff / 1000000, t_diff % 1000000);
	LOG("  in first pass: %llu.%llums", t_diff_add_domains / 1000000, t_diff_add_domains % 1000000);
	LOG("  in second pass: %llu.%llums", t_diff_add_unknown / 1000000, t_diff_add_unknown % 1000000);
	LOG("  to find name ids: %llu.%llums", t_diff_name_ids / 1000000, t_diff_name_ids % 1000000);
	LOG("  to sort \"UNKNOWN\" names: %llu.%llums", t_diff_unknown_sort / 1000000, t_diff_unknown_sort % 1000000);
#endif

	return 0;
exit_internal_no_db_slave_name:
	LOG("internal error: the slave name: \"%s\" that must have been added to the "
			"configuration could not be found", db->names[s->slave->db_entry->name]);
	ecc_error = ECC_ERROR_INTERNAL;
	goto exit_fail;
exit_internal_no_slave_name:
	LOG("internal error: the slave name: \"%s\" that must have been added to the "
			"configuration could not be found", de->slave_name);
	ecc_error = ECC_ERROR_INTERNAL;
	goto exit_fail;
exit_internal_no_domain_entry_name:
	LOG("internal error: the domain entry name: \"%s\" that must have been added to the "
			"configuration could not be found", de->entry_name);
	ecc_error = ECC_ERROR_INTERNAL;
	goto exit_fail;
exit_internal_domain_with_no_entry:
	LOG("internal error: a domain has been registered without any domain entries");
	ecc_error = ECC_ERROR_INTERNAL;
	goto exit_fail;
exit_no_mem:
	LOG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_fail:
	return -1;
}

static int _slavecmp(const void *s1, const void *s2)
{
	const ecc_conf_slave *first = s1;
	const ecc_conf_slave *second = s2;

	if (first->slave->position < second->slave->position)
		return -1;
	if (first->slave->position > second->slave->position)
		return 1;
	return 0;
}

static void _sort_slaves(ecc_conf *c)
{
	unsigned int i_master;
	ecc_conf_master *m;

	for (i_master = 0; i_master < c->masters_count; ++i_master)
	{
		m = &c->masters[i_master];

		qsort(m->slaves, m->slaves_count, sizeof(*m->slaves), _slavecmp);
	}
}

#ifndef NDEBUG
static void _log_conf(ecc_conf *c)
{
	unsigned int i_master, i_domain, i_entry;
	unsigned int i_slave_name, i_entry_name;
	unsigned int i_slave, i_sync, i_pdo, i_pdo_entry;
	ecc_conf_master *m;
	ecc_conf_domain *d;
	ecc_conf_domain_entry *de;
	ecc_conf_slave *s;
	ecc_conf_sync *y;
	ecc_conf_pdo *p;
	ecc_conf_pdo_entry *e;

	LOG("Configuration to send:");
	if (c->masters_count == 0)
		LOG("Empty");

	for (i_master = 0; i_master < c->masters_count; ++i_master)
	{
		m = &c->masters[i_master];

		LOG("Master: index: %0#10x", m->master->index);
		LOG("        slave names: %u", m->slave_names_count);
		LOG("        entry names: %u", m->entry_names_count);
		LOG("        domains: %u", m->domains_count);
		LOG("        slaves: %u", m->slaves_count);

		for (i_slave_name = 0; i_slave_name < m->slave_names_count; ++i_slave_name)
		{
			const char *slave_name = m->slave_names[i_slave_name];

			LOG("  Slave name: \"%s\"", slave_name?slave_name:"<null>");
		}

		for (i_entry_name = 0; i_entry_name < m->entry_names_count; ++i_entry_name)
		{
			const char *entry_name = m->entry_names[i_entry_name];

			LOG("  Entry name: \"%s\"", entry_name?entry_name:"<null>");
		}

		for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
		{
			d = &m->domains[i_domain];

			LOG("  Domain: id: %u", d->domain->id);
			LOG("          entries: %u", d->entries_count);
			LOG("          pdo entries: %u", d->pdo_entries_count);

			for (i_entry = 0; i_entry < d->entries_count; ++i_entry)
			{
				de = &d->entries[i_entry];

				LOG("    Entry: slave: %u (\"%s\")", de->slave, de->slave >= m->slave_names_count?"<out of range>":
						m->slave_names[de->slave]?m->slave_names[de->slave]:"<null>");
				LOG("           entry: %u (\"%s\")", de->entry, de->entry >= m->entry_names_count?"<out of range>":
						m->entry_names[de->entry]?m->entry_names[de->entry]:"<null>");
			}
		}

		for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
		{
			s = &m->slaves[i_slave];

			LOG("  Slave: name: %u (\"%s\")", s->name, s->name >= m->slave_names_count?"<out of range>":
					m->slave_names[s->name]?m->slave_names[s->name]:"<null>");
			LOG("         alias: %u", s->slave->alias);
			LOG("         position: %u", s->slave->position);
			LOG("         vendor id: %0#10x", s->slave->vendor_id);
			LOG("         product code: %0#10x", s->slave->product_code);
			LOG("         serial number: %0#10x", s->slave->serial_number);
			LOG("         syncs: %u", s->syncs_count);

			for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
			{
				y = &s->syncs[i_sync];

				LOG("    Sync: index: %0#10x", y->sync->index);
				LOG("          pdos: %u", y->pdos_count);

				for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
				{
					p = &y->pdos[i_pdo];

					LOG("      PDO: index: %0#6x", p->pdo->index);
					LOG("           entries: %u", p->entries_count);

					for (i_pdo_entry = 0; i_pdo_entry < p->entries_count; ++i_pdo_entry)
					{
						e = &p->entries[i_pdo_entry];

						LOG("        PDO Entry: name: %u (\"%s\")", e->name,
								e->name >= m->entry_names_count?"<out of range>":
								m->entry_names[e->name]?m->entry_names[e->name]:"<null>");
						LOG("                   index: %0#6x", e->pdo_entry->index);
						LOG("                   sub index: %0#6x", e->pdo_entry->sub_index);
						LOG("                   domains: %u", e->domains_count);

						for (i_domain = 0; i_domain < e->domains_count; ++i_domain)
							LOG("          Domain: id: %u",
								m->domains[e->domains[i_domain]].domain->id);
					}
				}
			}
		}
	}
}
#endif

/*
 * The format of the commands to send are the following.  More information on
 * the kernel module where the command is received.
 *
 * M n master_id_1 ... master_id_n
 * for each master:
 *   S n slave_name_1 ... slave_name_n
 *   E n entry_name_1 ... entry_name_n
 *   D n_domain n_domain_entry n_pdo_entry_1 ... n_pdo_entry_n_domain
 *     (n_domain_entry is the total of all domain entries in all domains)
 *   for each domain:
 *     d n slave_name_id_1 entry_name_id_1 ... slave_name_id_n entry_name_id_n
 *   N n_slave n_sync n_pdo n_pdo_entry
 *     (n_sync, n_pdo and n_pdo_entry are the total number of syncs, pdos and
 *       pdo entries among all slaves)
 *   for each slave:
 *     s name_id alias position vendor_id product_code n_sync n_pdo n_pdo_entry
 *     for each sync:
 *       y index io watchdog n_pdo
 *       for each pdo:
 *         p index n_pdo_entry
 *         for each pdo entry:
 *           e name_id index subindex bit_length n domain_id_1 ... domain_id_n
 *     f
 *   r
 *   a
 */
int ecc_configure(FILE *out, ecc_config *conf, ecc_database *db, ecc_network *network)
{
	unsigned int i_master, i_domain, i_domain_entry;
	unsigned int i_slave_name, i_entry_name;
	unsigned int i_slave, i_sync, i_pdo, i_pdo_entry;
	unsigned int domain_entry_total;
	unsigned int sync_total;
	unsigned int pdo_total;
	unsigned int pdo_entry_total;
	ecc_conf c = {0};
	ecc_conf_master *m;
	ecc_conf_domain *d;
	ecc_conf_domain_entry *de;
	ecc_conf_slave *s;
	ecc_conf_sync *y;
	ecc_conf_pdo *p;
	ecc_conf_pdo_entry *e;
	int ret = 0;

	/*
	 * create a structure similar to that of the network, but used
	 * for configuration.
	 */
	if (_build_slave_configuration(&c, network))
		goto exit_fail;

	/*
	 * for each master, find slave and entry names.
	 * at the same time, add the domains
	 */
	if (_build_domain_configuration(&c, conf, db))
		goto exit_fail;

	/* sort the slaves based on their alias/position */
	_sort_slaves(&c);

#ifndef NDEBUG
	_log_conf(&c);
#endif

	/* send the M command */
	fprintf(out, "M %u", c.masters_count);
	for (i_master = 0; i_master < c.masters_count; ++i_master)
		fprintf(out, " %u", c.masters[i_master].master->index);
	fprintf(out, "\n");

	for (i_master = 0; i_master < c.masters_count; ++i_master)
	{
		m = &c.masters[i_master];

		/* send the S command */
		fprintf(out, " S %u", m->slave_names_count);
		for (i_slave_name = 0; i_slave_name < m->slave_names_count; ++i_slave_name)
			fprintf(out, " \"%s\"", m->slave_names[i_slave_name]);
		fprintf(out, "\n");

		/* send the E command */
		fprintf(out, " E %u", m->entry_names_count);
		for (i_entry_name = 0; i_entry_name < m->entry_names_count; ++i_entry_name)
			fprintf(out, " \"%s\"", m->entry_names[i_entry_name]);
		fprintf(out, "\n");

		/* send the D command */
		domain_entry_total = 0;
		for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
			domain_entry_total += m->domains[i_domain].entries_count;
		fprintf(out, " D %u %u", m->domains_count, domain_entry_total);
		for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
			fprintf(out, " %u", m->domains[i_domain].pdo_entries_count);
		fprintf(out, "\n");

		for (i_domain = 0; i_domain < m->domains_count; ++i_domain)
		{
			d = &m->domains[i_domain];

			/* send the d command */
			fprintf(out, "  d %u", d->entries_count);
			for (i_domain_entry = 0; i_domain_entry < d->entries_count; ++i_domain_entry)
			{
				de = &d->entries[i_domain_entry];
				fprintf(out, " %u %u", de->slave, de->entry);
			}
			fprintf(out, "\n");
		}

		/* send the N command */
		sync_total = 0;
		pdo_total = 0;
		pdo_entry_total = 0;
		for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
		{
			s = &m->slaves[i_slave];
			sync_total += s->syncs_count;

			for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
			{
				y = &s->syncs[i_sync];
				pdo_total += y->pdos_count;

				for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
					pdo_entry_total += y->pdos[i_pdo].entries_count;
			}
		}
		fprintf(out, " N %u %u %u %u\n", m->slaves_count, sync_total,
			pdo_total, pdo_entry_total);

		for (i_slave = 0; i_slave < m->slaves_count; ++i_slave)
		{
			s = &m->slaves[i_slave];

			/* send the s command */
			pdo_total = 0;
			pdo_entry_total = 0;
			for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
			{
				y = &s->syncs[i_sync];
				pdo_total += y->pdos_count;

				for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
					pdo_entry_total += y->pdos[i_pdo].entries_count;
			}
			fprintf(out, "  s %u %u %u %#x %#x %u %u %u\n", s->name, s->slave->alias,
				s->slave->position, s->slave->vendor_id, s->slave->product_code,
				s->syncs_count, pdo_total, pdo_entry_total);

			for (i_sync = 0; i_sync < s->syncs_count; ++i_sync)
			{
				y = &s->syncs[i_sync];

				/* send the y command */
				fprintf(out, "   y %#x %u %u %u\n", y->sync->index, !y->sync->is_output,
					y->sync->has_watchdog, y->sync->pdos_count);

				for (i_pdo = 0; i_pdo < y->pdos_count; ++i_pdo)
				{
					p = &y->pdos[i_pdo];

					/* send the p command */
					fprintf(out, "    p %#x %u\n", p->pdo->index, p->pdo->entries_count);

					for (i_pdo_entry = 0; i_pdo_entry < p->entries_count; ++i_pdo_entry)
					{
						e = &p->entries[i_pdo_entry];

						/* send the e command */
						fprintf(out, "     e %u %#x %#x %u %u", e->name, e->pdo_entry->index,
							e->pdo_entry->sub_index, e->pdo_entry->bit_length,
							e->domains_count);
						for (i_domain = 0; i_domain < e->domains_count; ++i_domain)
							fprintf(out, " %u", e->domains[i_domain]);
						fprintf(out, "\n");
					}
				}
			}

			/* send the f command */
			fprintf(out, "  f\n");
		}

		/* send the r command */
		fprintf(out, " r\n");

		/* send the a command */
		fprintf(out, " a\n");
	}

	if (feof(out) || ferror(out))
		goto exit_bad_io;

	goto exit_cleanup;
exit_bad_io:
	LOG("error writing configuration commands");
	ecc_error = ECC_ERROR_WRITE;
	goto exit_fail;
exit_fail:
	ret = -1;
exit_cleanup:
	_free_conf(&c);
	return ret;
}
