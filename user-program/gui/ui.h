/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_UI_H
#define ECC_UI_H

#include <gtk/gtk.h>

/*
 * functions of type ecc_ui_static_str should return a static string
 *
 * functions of type ecc_ui_dynamic_str should `malloc` memory, build
 * the string and return it.  This string will be `free`d after the
 * callback
 *
 * TODO: callbacks for other operations
 */
struct ecc_ui_callbacks;

typedef const char *(*ecc_ui_static_str)(struct ecc_ui_callbacks *callbacks);
typedef char *(*ecc_ui_dynamic_str)(struct ecc_ui_callbacks *callbacks);

struct ecc_ui_callbacks
{
	ecc_ui_static_str database_file_name;
	ecc_ui_static_str config_file_name;

	ecc_ui_static_str guide;
	/* TODO: the rest */

	ecc_ui_dynamic_str get_logs;
	/* TODO: the rest */

	/* TODO: other callbacks */

	void *data;
};

int ecc_ui_create_window(struct ecc_ui_callbacks *callbacks);

#endif
