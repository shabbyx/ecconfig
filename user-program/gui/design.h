/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_DESIGN_H
#define ECC_DESIGN_H

/* note: at some time in the future, it may be better to move these stuff to a file */

#define ECC_UI_WHOLE_WINDOW_BORDER_WIDTH 3
#define ECC_UI_STATUS_BORDER_WIDTH 3
#define ECC_UI_MAIN_BORDER_WIDTH 3
#define ECC_UI_LEFT_PANEL_BORDER_WIDTH 3
#define ECC_UI_LEFT_PANEL_SECTION_BORDER_WIDTH 3
#define ECC_UI_RIGHT_PANEL_BORDER_WIDTH 3
#define ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH 3
#define ECC_UI_MULTI_FUNCTION_AREA_BORDER_WIDTH 5
#define ECC_UI_MULTI_FUNCTION_AREA_PADDING_WIDTH 7
#define ECC_UI_SUB_BUTTON_BORDER_WIDTH 3
#define ECC_UI_SUB_INFO_BORDER_WIDTH 3

#define ECC_UI_WHOLE_WINDOW_SPACING 5
#define ECC_UI_STATUS_SPACING 5
#define ECC_UI_MAIN_SPACING 5
#define ECC_UI_LEFT_PANEL_SPACING 5
#define ECC_UI_LEFT_PANEL_SECTION_SPACING 5
#define ECC_UI_RIGHT_PANEL_SPACING 5
#define ECC_UI_RIGHT_PANEL_SECTION_SPACING 5
#define ECC_UI_SUB_BUTTON_SPACING 5
#define ECC_UI_SUB_INFO_SPACING 7

#define ECC_UI_STATUS_BAR_BUTTON_WIDTH 60
#define ECC_UI_PANEL_BUTTON_HEIGHT 50
#define ECC_UI_PANEL_LARGE_BUTTON_HEIGHT 70

#endif
