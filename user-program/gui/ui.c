/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui.h"
#include "design.h"
#include "ui_signals.h"

#define CHECK(x) do { if ((x) == NULL) goto exit_no_mem; } while (0)
#define DESTROY(x) do { if ((x) != NULL) gtk_widget_destroy(x); x = NULL; } while (0)

static inline GtkWidget *_get_button_with_markup(const char *text)
{
	GtkWidget *button = NULL;
	GtkWidget *label = NULL;

	CHECK(button = gtk_button_new());
	CHECK(label = gtk_label_new(""));
	gtk_label_set_markup(GTK_LABEL(label), text);
	gtk_widget_show(label);
	gtk_container_add(GTK_CONTAINER(button), label);

	return button;
exit_no_mem:
	DESTROY(button);
	return NULL;
}

static inline GtkWidget *_get_button_multiline(const char *text)
{
	GtkWidget *button = NULL;
	GtkWidget *label = NULL;

	CHECK(button = gtk_button_new());
	CHECK(label = gtk_label_new(""));
	gtk_label_set_text(GTK_LABEL(label), text);
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER);
	gtk_widget_show(label);
	gtk_container_add(GTK_CONTAINER(button), label);

	return button;
exit_no_mem:
	DESTROY(button);
	return NULL;
}

static inline GtkWidget *_get_clickable_event_box(const char *text)
{
	GtkWidget *event_box = NULL;
	GtkWidget *label = NULL;

	CHECK(event_box = gtk_event_box_new());
	CHECK(label = gtk_label_new(text?text:"unest"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0f, 0.5f);
	gtk_widget_show(label);
	gtk_container_add(GTK_CONTAINER(event_box), label);
	gtk_widget_set_events(event_box, GDK_BUTTON_PRESS_MASK);

	return event_box;
exit_no_mem:
	DESTROY(event_box);
	return NULL;
}

/* TODO: make tooltips for everything! */

/*******************************************
 *               Status Bar                *
 *******************************************/
static GtkWidget *_create_status_bar(struct ecc_ui_callbacks *callbacks,
		struct ecc_ui_widgets_to_remember *widgets_to_remember)
{
	GtkWidget *status_bar = NULL;
	GtkWidget *left_side = NULL;
	GtkWidget *right_side = NULL;
	GtkWidget *status = NULL;
	GtkWidget *last_message = NULL;
	GtkWidget *logs = NULL;
	GtkWidget *progress_bar = NULL;
	GtkWidget *cancel = NULL;
	GtkWidget *separator = NULL;

	/* status bar contains two sides */
	CHECK(status_bar = gtk_hbox_new(FALSE, ECC_UI_STATUS_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(status_bar), ECC_UI_STATUS_BORDER_WIDTH);

	/* the left side contains the status, last message, and show logs button */
	CHECK(left_side = gtk_hbox_new(FALSE, ECC_UI_STATUS_SPACING));
	gtk_widget_show(left_side);
	gtk_box_pack_start(GTK_BOX(status_bar), left_side, TRUE, TRUE, 0);

	/* the right side contains a separator, the progress bar and the cancel button */
	CHECK(right_side = gtk_hbox_new(FALSE, ECC_UI_STATUS_SPACING));
	gtk_widget_show(right_side);
	gtk_box_pack_end(GTK_BOX(status_bar), right_side, FALSE, FALSE, 0);

	/* the status message (left side) */
	CHECK(status = gtk_label_new("Ready"));
	gtk_widget_show(status);
	gtk_box_pack_start(GTK_BOX(left_side), status, FALSE, FALSE, 0);
	widgets_to_remember->status = status;

	/* the last log message (left side) */
	CHECK(last_message = gtk_label_new(":  Test"));
	gtk_label_set_ellipsize(GTK_LABEL(last_message), PANGO_ELLIPSIZE_END);
	gtk_misc_set_alignment(GTK_MISC(last_message), 0.0f, 0.5f);
	gtk_widget_show(last_message);
	gtk_box_pack_start(GTK_BOX(left_side), last_message, TRUE, TRUE, 0);
	widgets_to_remember->last_log = last_message;

	/* the log button (left side) */
	CHECK(logs = gtk_button_new_with_label("Logs"));
	gtk_widget_set_size_request(logs, ECC_UI_STATUS_BAR_BUTTON_WIDTH, -1);
	gtk_widget_show(logs);
	gtk_box_pack_start(GTK_BOX(left_side), logs, FALSE, FALSE, 0);
	widgets_to_remember->logs = logs;

	/* the cancel button */
	CHECK(cancel = gtk_button_new_with_label("Cancel"));
	gtk_box_pack_end(GTK_BOX(right_side), cancel, FALSE, FALSE, 0);
	widgets_to_remember->cancel_button = cancel;

	/* the progress bar (right side) */
	CHECK(progress_bar = gtk_progress_bar_new());
	gtk_box_pack_end(GTK_BOX(right_side), progress_bar, TRUE, TRUE, 0);
	widgets_to_remember->progress_bar = progress_bar;

	/* the separator (right side) */
	CHECK(separator = gtk_vseparator_new());
	gtk_box_pack_start(GTK_BOX(right_side), separator, FALSE, FALSE, 0);
	widgets_to_remember->separator = separator;

	return status_bar;
exit_no_mem:
	DESTROY(status_bar);
	return NULL;
}

/*******************************************
 *               Left Panel                *
 *******************************************/
static GtkWidget *_create_left_panel(struct ecc_ui_callbacks *callbacks,
		struct ecc_ui_widgets_to_remember *widgets_to_remember)
{
	GtkWidget *left_panel = NULL;
	GtkWidget *network = NULL;
	GtkWidget *database = NULL;
	GtkWidget *configuration = NULL;
	GtkWidget *config = NULL;
	GtkWidget *realias = NULL;
	GtkWidget *button = NULL;
	GtkWidget *label = NULL;
	GtkWidget *event_box = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *table = NULL;

	/* the left panel is a vertical list of buttons and texts */
	CHECK(left_panel = gtk_vbox_new(FALSE, ECC_UI_LEFT_PANEL_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(left_panel), ECC_UI_LEFT_PANEL_BORDER_WIDTH);


	/***********
	 * network *
	 ***********/
	CHECK(network = gtk_vbox_new(FALSE, ECC_UI_LEFT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(network), ECC_UI_LEFT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(network);
	gtk_box_pack_start(GTK_BOX(left_panel), network, FALSE, FALSE, 0);

	CHECK(button = gtk_button_new_with_label("Network"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(network), button, FALSE, FALSE, 0);
	widgets_to_remember->network = button;

	/* network sub buttons: reset */
	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_SUB_BUTTON_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_SUB_BUTTON_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(network), hbox, FALSE, FALSE, 0);

	CHECK(button = _get_button_with_markup("<small>Reset</small>"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->network_reset = button;

	/* phantom label to make hbox even */
	CHECK(label = gtk_label_new(""));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

	/* network infos: masters, slaves */
	CHECK(table = gtk_table_new(2, 2, FALSE));
	gtk_table_set_row_spacings(GTK_TABLE(table), 1);	/* for the clickable labels to be distinguished */
	gtk_container_set_border_width(GTK_CONTAINER(table), ECC_UI_SUB_INFO_BORDER_WIDTH);
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(network), table, FALSE, FALSE, 0);

	/* first column: info lables, right justified */
	CHECK(label = gtk_label_new("masters: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("slaves: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the information */
	CHECK(event_box = _get_clickable_event_box("unknown"));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->masters_count = event_box;

	CHECK(event_box = _get_clickable_event_box("unknown"));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->slaves_count = event_box;


	/************
	 * database *
	 ************/
	CHECK(database = gtk_vbox_new(FALSE, ECC_UI_LEFT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(database), ECC_UI_LEFT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(database);
	gtk_box_pack_start(GTK_BOX(left_panel), database, FALSE, FALSE, 0);

	CHECK(button = gtk_button_new_with_label("Namebase"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(database), button, FALSE, FALSE, 0);
	widgets_to_remember->database = button;

	/* database sub buttons: reset, save */
	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_SUB_BUTTON_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_SUB_BUTTON_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(database), hbox, FALSE, FALSE, 0);

	CHECK(button = _get_button_with_markup("<small>Clear</small>"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->database_clear = button;

	CHECK(button = _get_button_with_markup("<small>Save</small>"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->database_save = button;

	/* database infos: names, matched_slaves, file */
	CHECK(table = gtk_table_new(3, 2, FALSE));
	gtk_table_set_row_spacings(GTK_TABLE(table), 1);	/* for the clickable labels to be distinguished */
	gtk_container_set_border_width(GTK_CONTAINER(table), ECC_UI_SUB_INFO_BORDER_WIDTH);
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(database), table, FALSE, FALSE, 0);

	/* first column: info lables, right justified */
	CHECK(label = gtk_label_new("names: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("matched: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("file: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the information */
	CHECK(event_box = _get_clickable_event_box("0"));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->names_count = event_box;

	CHECK(event_box = _get_clickable_event_box("0"));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->matched_count = event_box;

	CHECK(event_box = _get_clickable_event_box(callbacks->database_file_name(callbacks)));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 2, 3, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->db_file_name = event_box;


	/****************
	 * config input *
	 ****************/
	CHECK(configuration = gtk_vbox_new(FALSE, ECC_UI_LEFT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(configuration), ECC_UI_LEFT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(configuration);
	gtk_box_pack_start(GTK_BOX(left_panel), configuration, FALSE, FALSE, 0);

	CHECK(button = gtk_button_new_with_label("Configuration"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(configuration), button, FALSE, FALSE, 0);
	widgets_to_remember->config = button;

	/* configuration sub buttons: reset, save */
	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_SUB_BUTTON_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_SUB_BUTTON_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(configuration), hbox, FALSE, FALSE, 0);

	CHECK(button = _get_button_with_markup("<small>Clear</small>"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->config_clear = button;

	CHECK(button = _get_button_with_markup("<small>Save</small>"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->database_save = button;

	/* configuration infos: domains, file */
	CHECK(table = gtk_table_new(2, 2, FALSE));
	gtk_table_set_row_spacings(GTK_TABLE(table), 1);	/* for the clickable labels to be distinguished */
	gtk_container_set_border_width(GTK_CONTAINER(table), ECC_UI_SUB_INFO_BORDER_WIDTH);
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(configuration), table, FALSE, FALSE, 0);

	/* first column: info lables, right justified */
	CHECK(label = gtk_label_new("domains: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("file: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the information */
	CHECK(event_box = _get_clickable_event_box("0"));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->domains_count = event_box;

	CHECK(event_box = _get_clickable_event_box(callbacks->config_file_name(callbacks)));
	gtk_widget_show(event_box);
	gtk_table_attach(GTK_TABLE(table), event_box, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->config_file_name = event_box;


	/********************
	 * configure button *
	 ********************/
	CHECK(config = gtk_vbox_new(FALSE, ECC_UI_LEFT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(config), ECC_UI_LEFT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(config);
	gtk_box_pack_end(GTK_BOX(left_panel), config, FALSE, FALSE, 0);

	CHECK(button = _get_button_multiline("Configure\nThe Network"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_LARGE_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_widget_set_sensitive(button, FALSE);
	gtk_box_pack_start(GTK_BOX(config), button, FALSE, FALSE, 0);
	widgets_to_remember->configure = button;


	/********************
	 * Realias button *
	 ********************/
	CHECK(realias = gtk_vbox_new(FALSE, ECC_UI_LEFT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(realias), ECC_UI_LEFT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(realias);
	gtk_box_pack_end(GTK_BOX(left_panel), realias, FALSE, FALSE, 0);

	CHECK(button = _get_button_multiline("Realias\nthe Slaves"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_LARGE_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_widget_set_sensitive(button, FALSE);
	gtk_box_pack_start(GTK_BOX(realias), button, FALSE, FALSE, 0);
	widgets_to_remember->realias = button;

	return left_panel;
exit_no_mem:
	DESTROY(left_panel);
	return NULL;
}

/*******************************************
 *               Right Panel               *
 *******************************************/
/*static void _show_guide(GtkWidget *w, gpointer data)
{
	struct ecc_ui_callbacks *callbacks = data;
	GtkTextBuffer *buffer = NULL;

	CHECK(buffer = gtk_text_buffer_new(NULL));
	gtk_text_buffer_set_text(buffer, callbacks->guide(callbacks), -1);
	gtk_text_view_set_buffer(GTK_TEXT_VIEW(w), buffer);

exit_no_mem:
	return;
}*/

static GtkWidget *_create_multi_function_area(struct ecc_ui_callbacks *callbacks,
		struct ecc_ui_widgets_to_remember *widgets_to_remember)
{
	GtkWidget *scrolled_window = NULL;
	GtkWidget *vbox = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *form = NULL;
	GtkWidget *label = NULL;
	GtkWidget *button = NULL;
	GtkWidget *text_entry = NULL;
	GtkWidget *combo = NULL;
	GtkWidget *table = NULL;

	/*
	 * the multi-function area consists of many possible widgets:
	 *
	 * - Guide (if all other constraints failed)
	 *   * tell next step to take
	 *   * tell how to cancel during operations
	 *   * tell to double click to undo/redo something
	 * - Show information (if clicked on some info, below Network, Namebase and Configuration buttons)
	 * - Show slave data, with options (if clicked on slave)
	 *   * show slave data, such as syncs, pdos, entries etc
	 *   * also, show "Add to DB" and "Add to Config" buttons
	 * - Show form (if there is form to fill)
	 *   * options form
	 *   * add to db form
	 *   * add to config form
	 */
	CHECK(scrolled_window = gtk_scrolled_window_new(NULL, NULL));
	gtk_widget_set_size_request(scrolled_window, -1, 50);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width(GTK_CONTAINER(scrolled_window), ECC_UI_MULTI_FUNCTION_AREA_BORDER_WIDTH);

	/* all possible windows will be created in a vbox, but only one of them will be visible at all times */
	CHECK(vbox = gtk_vbox_new(FALSE, 0));
	gtk_widget_show(vbox);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), vbox);
	gtk_container_set_border_width(GTK_CONTAINER(vbox), ECC_UI_MULTI_FUNCTION_AREA_PADDING_WIDTH);

	/*********
	 * Guide *
	 *********/
	CHECK(label = gtk_label_new("This is a test\n\n\n\n\n\n\n\n\n\nsomething\n\n\n\n\n\n\n\n\nmore\n\n\n\n\n\n\n\nto\n\n\n\n\n\n\n\n\nsay"));
	/* Note: others will not have gtk_widget_show */
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	widgets_to_remember->guide = label;

	/*******************************
	 * Info from left panel labels *
	 *******************************/
	CHECK(label = gtk_label_new(""));
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	widgets_to_remember->masters_info = label;

	CHECK(label = gtk_label_new(""));
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	widgets_to_remember->slaves_info = label;

	CHECK(label = gtk_label_new(""));
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	widgets_to_remember->names_info = label;

	CHECK(label = gtk_label_new(""));
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	widgets_to_remember->matched_info = label;

	CHECK(label = gtk_label_new(""));
	gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, TRUE, 0);
	widgets_to_remember->domains_info = label;

	/**************
	 * Slave form *
	 **************/
	CHECK(form = gtk_vbox_new(FALSE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_box_pack_start(GTK_BOX(vbox), form, TRUE, TRUE, 0);
	widgets_to_remember->slave_form = form;

	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(form), hbox, FALSE, FALSE, 0);

	CHECK(button = _get_button_multiline("Add to\nNamebase"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->slave_add_to_db = button;

	CHECK(button = _get_button_multiline("Add to\nConfinguration"));
	gtk_widget_show(button);
	gtk_box_pack_end(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->slave_add_to_config = button;

	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(form), hbox, FALSE, FALSE, 0);

	CHECK(button = _get_button_multiline("Remove from\nNamebase"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->slave_rm_from_db = button;

	CHECK(button = _get_button_multiline("Remove from\nConfinguration"));
	gtk_widget_show(button);
	gtk_box_pack_end(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->slave_rm_from_config = button;

	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(form), hbox, FALSE, FALSE, 0);

	CHECK(button = gtk_button_new_with_label("Realias"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->slave_realias = button;

	CHECK(button = _get_button_multiline("Unalias"));
	gtk_widget_show(button);
	gtk_box_pack_end(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->slave_unalias = button;

	CHECK(label = gtk_label_new(""));
	gtk_widget_show(label);
	gtk_box_pack_end(GTK_BOX(form), label, TRUE, TRUE, 0);
	widgets_to_remember->slave_info = label;

	/****************
	 * Options form *	TODO: I don't know the options yet
	 ****************/
	CHECK(table = gtk_table_new(2, 2, FALSE));
	gtk_box_pack_start(GTK_BOX(vbox), table, TRUE, TRUE, 0);
	widgets_to_remember->options_form = table;

	/* first column: labels, right justified */
	CHECK(label = gtk_label_new("option 1: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("other option: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the form */
	CHECK(text_entry = gtk_entry_new());
	gtk_widget_show(text_entry);
	gtk_table_attach(GTK_TABLE(table), text_entry, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->option_todo = text_entry;

	CHECK(text_entry = gtk_entry_new());
	gtk_widget_show(text_entry);
	gtk_table_attach(GTK_TABLE(table), text_entry, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
	widgets_to_remember->option_todo2 = text_entry;

	/******************
	 * Add to DB form *
	 ******************/
	CHECK(form = gtk_vbox_new(FALSE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_box_pack_start(GTK_BOX(vbox), form, TRUE, TRUE, 0);
	widgets_to_remember->add_to_db_form = form;

	CHECK(label = gtk_label_new(""));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(form), label, FALSE, FALSE, 0);
	widgets_to_remember->add_to_db_text = label;

	CHECK(table = gtk_table_new(1, 2, FALSE));
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(form), table, TRUE, TRUE, 0);

	/* first column: labels, right justified */
	CHECK(label = gtk_label_new("Name: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the form */
	CHECK(combo = gtk_combo_box_text_new_with_entry());
	gtk_widget_show(combo);
	gtk_table_attach(GTK_TABLE(table), combo, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND, 0, 0);
	widgets_to_remember->add_to_db_name = combo;

	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(form), hbox, FALSE, FALSE, 0);

	CHECK(button = gtk_button_new_with_label("Back"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->add_to_db_back = button;

	CHECK(button = gtk_button_new_with_label("Add"));
	gtk_widget_show(button);
	gtk_box_pack_end(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->add_to_db_add = button;

	/**********************
	 * Add to config form *
	 **********************/
	CHECK(form = gtk_vbox_new(FALSE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_box_pack_start(GTK_BOX(vbox), form, TRUE, TRUE, 0);
	widgets_to_remember->add_to_config_form = form;

	CHECK(label = gtk_label_new(""));
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(form), label, FALSE, FALSE, 0);
	widgets_to_remember->add_to_config_text = label;

	CHECK(table = gtk_table_new(3, 2, FALSE));
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(form), table, TRUE, TRUE, 0);

	/* first column: labels, right justified */
	CHECK(label = gtk_label_new("Domain: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("Entry: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("Direction: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the form */
	CHECK(combo = gtk_combo_box_text_new_with_entry());
	gtk_widget_show(combo);
	gtk_table_attach(GTK_TABLE(table), combo, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND, 0, 0);
	widgets_to_remember->add_to_config_domain = combo;

	CHECK(combo = gtk_combo_box_text_new_with_entry());
	gtk_widget_show(combo);
	gtk_table_attach(GTK_TABLE(table), combo, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND, 0, 0);
	widgets_to_remember->add_to_config_entry = combo;

	CHECK(combo = gtk_combo_box_text_new_with_entry());
	gtk_widget_show(combo);
	gtk_table_attach(GTK_TABLE(table), combo, 1, 2, 2, 3, GTK_EXPAND | GTK_FILL, GTK_EXPAND, 0, 0);
	widgets_to_remember->add_to_config_io = combo;

	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), "Input");
	gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), "Output");

	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(form), hbox, FALSE, FALSE, 0);

	CHECK(button = gtk_button_new_with_label("Back"));
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->add_to_config_back = button;

	CHECK(button = gtk_button_new_with_label("Add"));
	gtk_widget_show(button);
	gtk_box_pack_end(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->add_to_config_add = button;

	return scrolled_window;
exit_no_mem:
	DESTROY(scrolled_window);
	return NULL;
}

static GtkWidget *_create_right_panel(struct ecc_ui_callbacks *callbacks,
		struct ecc_ui_widgets_to_remember *widgets_to_remember)
{
	GtkWidget *right_panel = NULL;
	GtkWidget *separator = NULL;
	GtkWidget *pane = NULL;
	GtkWidget *scrolled_window = NULL;
	GtkWidget *button = NULL;
	GtkWidget *label = NULL;
	GtkWidget *hbox = NULL;
	GtkWidget *vbox = NULL;
	GtkWidget *table = NULL;
	GtkWidget *tree_view = NULL;
	GtkListStore *list_store = NULL;
	GtkCellRenderer *cell = NULL;
	GtkTreeViewColumn *column = NULL;
	GtkTreeIter iter;

	/* the right panel is a vertical list of buttons and texts */
	CHECK(right_panel = gtk_vbox_new(FALSE, ECC_UI_RIGHT_PANEL_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(right_panel), ECC_UI_RIGHT_PANEL_BORDER_WIDTH);

	/*******************
	 * general buttons *
	 *******************/
	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(right_panel), hbox, FALSE, FALSE, 0);

	/* general buttons consist of about, help and exit */
	CHECK(button = gtk_button_new_with_label("About"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->about = button;

	CHECK(button = gtk_button_new_with_label("Help"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->help = button;

	CHECK(button = gtk_button_new_with_label("Exit"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->exit = button;


	/***********
	 * Options *
	 ***********/
	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_box_pack_start(GTK_BOX(right_panel), hbox, FALSE, FALSE, 0);

	/* Options have a button as well as a table with the most important options */
	CHECK(button = gtk_button_new_with_label("Options"));
	gtk_widget_set_size_request(button, -1, ECC_UI_PANEL_BUTTON_HEIGHT);
	gtk_widget_show(button);
	gtk_box_pack_start(GTK_BOX(hbox), button, TRUE, TRUE, 0);
	widgets_to_remember->options = button;

	CHECK(table = gtk_table_new(2, 2, FALSE));
	gtk_container_set_border_width(GTK_CONTAINER(table), ECC_UI_SUB_INFO_BORDER_WIDTH);
	gtk_widget_show(table);
	gtk_box_pack_start(GTK_BOX(hbox), table, FALSE, FALSE, 0);

	/* first column: info lables, right justified */
	CHECK(label = gtk_label_new("test: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("other test: "));
	gtk_misc_set_alignment(GTK_MISC(label), 1.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2, GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* second column: the information */
	CHECK(label = gtk_label_new("something"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 1, 2, 0, 1, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	CHECK(label = gtk_label_new("meh"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0f, 0.5f);
	gtk_widget_show(label);
	gtk_table_attach(GTK_TABLE(table), label, 1, 2, 1, 2, GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

	/* TODO: remember these options */

	/* the separator */
	CHECK(separator = gtk_hseparator_new());
	gtk_widget_show(separator);
	gtk_box_pack_start(GTK_BOX(right_panel), separator, FALSE, FALSE, 0);

	/* from here, the panel is divided in two; undo/redo section and info/forms section */
	CHECK(pane = gtk_vpaned_new());
	gtk_container_set_border_width(GTK_CONTAINER(pane), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(pane);
	gtk_box_pack_end(GTK_BOX(right_panel), pane, TRUE, TRUE, 0);


	/*******************
	 * Undo/Redo Lists *
	 *******************/
	CHECK(hbox = gtk_hbox_new(TRUE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(hbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(hbox);
	gtk_paned_pack1(GTK_PANED(pane), hbox, FALSE, FALSE);

	/* undo list */
	CHECK(vbox = gtk_vbox_new(FALSE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(vbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(vbox);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);

	CHECK(label = gtk_label_new("Undo List"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0f, 0.5f);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

	CHECK(scrolled_window = gtk_scrolled_window_new(NULL, NULL));
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show(scrolled_window);
	gtk_box_pack_end(GTK_BOX(vbox), scrolled_window, TRUE, TRUE, 0);

	CHECK(tree_view = gtk_tree_view_new());
	gtk_widget_show(tree_view);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), tree_view);
	widgets_to_remember->undo_list = tree_view;

	CHECK(list_store = gtk_list_store_new(1, G_TYPE_STRING));
	gtk_tree_view_set_model(GTK_TREE_VIEW(tree_view), GTK_TREE_MODEL(list_store));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(tree_view), FALSE);

	CHECK(cell = gtk_cell_renderer_text_new());
	CHECK(column = gtk_tree_view_column_new_with_attributes("Redo list", cell, "text", 0, NULL));
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), GTK_TREE_VIEW_COLUMN(column));

	gtk_list_store_prepend(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, "undo list store 1", -1);
	gtk_list_store_prepend(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, "undo list store 0", -1);

	/* redo list */
	CHECK(vbox = gtk_vbox_new(FALSE, ECC_UI_RIGHT_PANEL_SECTION_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(vbox), ECC_UI_RIGHT_PANEL_SECTION_BORDER_WIDTH);
	gtk_widget_show(vbox);
	gtk_box_pack_end(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);

	CHECK(label = gtk_label_new("Redo List"));
	gtk_misc_set_alignment(GTK_MISC(label), 0.0f, 0.5f);
	gtk_widget_show(label);
	gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 0);

	CHECK(scrolled_window = gtk_scrolled_window_new(NULL, NULL));
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolled_window), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show(scrolled_window);
	gtk_box_pack_end(GTK_BOX(vbox), scrolled_window, TRUE, TRUE, 0);

	CHECK(tree_view = gtk_tree_view_new());
	gtk_widget_show(tree_view);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrolled_window), tree_view);
	widgets_to_remember->redo_list = tree_view;

	CHECK(list_store = gtk_list_store_new(1, G_TYPE_STRING));
	gtk_tree_view_set_model(GTK_TREE_VIEW(tree_view), GTK_TREE_MODEL(list_store));
	gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(tree_view), FALSE);

	CHECK(cell = gtk_cell_renderer_text_new());
	CHECK(column = gtk_tree_view_column_new_with_attributes("Redo list", cell, "text", 0, NULL));
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree_view), GTK_TREE_VIEW_COLUMN(column));

	gtk_list_store_prepend(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, "test list store 1", -1);
	gtk_list_store_prepend(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, "test list store 0", -1);
	gtk_list_store_append(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, "test list store 2", -1);
	gtk_list_store_append(list_store, &iter);
	gtk_list_store_set(list_store, &iter, 0, "test list store 3", -1);

	/***********************
	 * Multi Function Area *
	 ***********************/
	CHECK(scrolled_window = _create_multi_function_area(callbacks, widgets_to_remember));
	gtk_widget_show(scrolled_window);
	gtk_paned_pack2(GTK_PANED(pane), scrolled_window, TRUE, FALSE);

	return right_panel;
exit_no_mem:
	DESTROY(right_panel);
	return NULL;
}

/*******************************************
 *              Whole Program              *
 *******************************************/
static GtkWidget *_create_window(struct ecc_ui_callbacks *callbacks)
{
	struct ecc_ui_widgets_to_remember widgets_to_remember = {0};
	GtkWidget *window = NULL;
	GtkWidget *whole_window = NULL;
	GtkWidget *status_bar = NULL;
	GtkWidget *separator = NULL;
	GtkWidget *main_area = NULL;
	GtkWidget *left_panel = NULL;
	GtkWidget *right_panel = NULL;
//	GtkWidget *middle_panel = NULL;
	GdkCursor *cursor;

	/* create the window */
	CHECK(window = gtk_window_new(GTK_WINDOW_TOPLEVEL));
	gtk_window_set_title(GTK_WINDOW(window), "ECConfig");
	gtk_window_set_default_size(GTK_WINDOW(window),
			gdk_screen_width() - 30,
			gdk_screen_height() - 40);
	gtk_window_maximize(GTK_WINDOW(window));
	/* TODO: add shortcuts for buttons. Is this by gtk_window_add_mnemonics */
	/* TODO: design icon and set it with gtk_window_set_icon */
	/* gtk_container_set_border_width(GTK_CONTAINER(window), 20); */
	widgets_to_remember.window = window;

	/* the window consists of the main area, a separator and a status bar */
	CHECK(whole_window = gtk_vbox_new(FALSE, ECC_UI_WHOLE_WINDOW_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(whole_window), ECC_UI_WHOLE_WINDOW_BORDER_WIDTH);
	gtk_widget_show(whole_window);
	gtk_container_add(GTK_CONTAINER(window), whole_window);

	/* the status bar */
	CHECK(status_bar = _create_status_bar(callbacks, &widgets_to_remember));
	gtk_widget_show(status_bar);
	gtk_box_pack_end(GTK_BOX(whole_window), status_bar, FALSE, FALSE, 0);

	/* the separator */
	CHECK(separator = gtk_hseparator_new());
	gtk_widget_show(separator);
	gtk_box_pack_end(GTK_BOX(whole_window), separator, FALSE, FALSE, 0);

	/* the main area, which in itself consists of left, right and middle panels */
	CHECK(main_area = gtk_hbox_new(FALSE, ECC_UI_MAIN_SPACING));
	gtk_container_set_border_width(GTK_CONTAINER(main_area), ECC_UI_MAIN_BORDER_WIDTH);
	gtk_widget_show(main_area);
	gtk_box_pack_start(GTK_BOX(whole_window), main_area, TRUE, TRUE, 0);

	/* the left panel */
	CHECK(left_panel = _create_left_panel(callbacks, &widgets_to_remember));
	gtk_widget_show(left_panel);
	gtk_box_pack_start(GTK_BOX(main_area), left_panel, FALSE, FALSE, 0);

	/* the right panel */
	CHECK(right_panel = _create_right_panel(callbacks, &widgets_to_remember));
	gtk_widget_show(right_panel);
	gtk_box_pack_end(GTK_BOX(main_area), right_panel, FALSE, FALSE, 0);

#if 0
	/* the middle panel */
	CHECK(middle_panel = _create_middle_panel(callbacks, &widgets_to_remember));
	gtk_widget_show(middle_panel);
	gtk_box_pack_start(GTK_BOX(main_area), middle_panel, TRUE, TRUE, 0);
#endif

	/* final polish */

	/* mouse change over clickable labels */
	CHECK(cursor = gdk_cursor_new(GDK_HAND1));
	gtk_widget_realize(widgets_to_remember.masters_count);
	gtk_widget_realize(widgets_to_remember.slaves_count);
	gtk_widget_realize(widgets_to_remember.names_count);
	gtk_widget_realize(widgets_to_remember.matched_count);
	gtk_widget_realize(widgets_to_remember.db_file_name);
	gtk_widget_realize(widgets_to_remember.domains_count);
	gtk_widget_realize(widgets_to_remember.config_file_name);
	gdk_window_set_cursor(widgets_to_remember.masters_count->window, cursor);
	gdk_window_set_cursor(widgets_to_remember.slaves_count->window, cursor);
	gdk_window_set_cursor(widgets_to_remember.names_count->window, cursor);
	gdk_window_set_cursor(widgets_to_remember.matched_count->window, cursor);
	gdk_window_set_cursor(widgets_to_remember.db_file_name->window, cursor);
	gdk_window_set_cursor(widgets_to_remember.domains_count->window, cursor);
	gdk_window_set_cursor(widgets_to_remember.config_file_name->window, cursor);

	/* connect signals */
	ecc_ui_connect_signals(callbacks, &widgets_to_remember);

	return window;
exit_no_mem:
	return NULL;
}

int ecc_ui_create_window(struct ecc_ui_callbacks *callbacks)
{
	GtkWidget *window;

	CHECK(window = _create_window(callbacks));
	gtk_widget_show(window);

	return 0;
exit_no_mem:
	return -1;
}
