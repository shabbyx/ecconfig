/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtk/gtk.h>
#include "ui.h"

static const char *_database_file_name(struct ecc_ui_callbacks *callbacks)
{
	/* TODO */
	return NULL;
}

static const char *_config_file_name(struct ecc_ui_callbacks *callbacks)
{
	/* TODO */
	return NULL;
}

static const char *_guide(struct ecc_ui_callbacks *callbacks)
{
	/* TODO */
	return "Test\nYo!\nShabi was here";
}

int main(int argc, char **argv)
{
/*	GtkWidget *button;
	GtkWidget *box;

	gtk_init(&argc, &argv);

	box = gtk_hbox_new(FALSE, 10);
	gtk_container_add(GTK_CONTAINER(window), box);

	button = gtk_button_new_with_label("Button");
	g_signal_connect(button, "clicked", G_CALLBACK(test_button), (gpointer)"button 1");
	gtk_box_pack_start(GTK_BOX(box), button, TRUE, FALSE, 17);
	gtk_widget_show(button);

	button = gtk_button_new_with_label("Button 2");
	g_signal_connect(button, "clicked", G_CALLBACK(test_button), (gpointer)"button 2");
	gtk_box_pack_start(GTK_BOX(box), button, TRUE, TRUE, 5);
	gtk_widget_show(button);

	gtk_widget_show(box);
	gtk_widget_show(window);*/

	struct ecc_ui_callbacks callbacks = {
		.database_file_name = _database_file_name,
		.config_file_name = _config_file_name,
		.guide = _guide,
	};

	gtk_init(&argc, &argv);

	if (ecc_ui_create_window(&callbacks))
		printf("out of memory\n");

	gtk_main();

	return 0;
}
