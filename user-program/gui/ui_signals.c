/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui.h"
#include "ui_signals.h"

#define RESPONSE_YES 0
#define RESPONSE_NO 1
#define RESPONSE_CANCEL 2
#define RESPONSE_SAVE 0
#define RESPONSE_DONT_SAVE 1

#define CHECK(x) do { if ((x) == NULL) goto exit_no_mem; } while (0)
#define DESTROY(x) do { if ((x) != NULL) gtk_widget_destroy(x); x = NULL; } while (0)

/**************
 * Status bar *
 **************/
static void _show_logs(GtkWidget *w, gpointer data)
{
	//struct ecc_ui_callbacks *callbacks = data;

	/* TODO: call callbacks->get_data() and show the string */
}


/*****************
 * Window delete *
 *****************/
static void _window_destroy(GtkWidget *w, gpointer data)
{
	/* TODO: cleanup */
	gtk_main_quit();
}

static gboolean _window_delete(GtkWidget *w, gpointer data)
{
	GtkWidget *dialog = NULL;
	gint response;

	/* create an "are-you-sure" dialog */
	CHECK(dialog = gtk_message_dialog_new(GTK_WINDOW(w),
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_NONE,
			"Are you sure you want to quit?"));
	gtk_dialog_add_buttons(GTK_DIALOG(dialog),
			"Cancel", RESPONSE_CANCEL,
			"Yes", RESPONSE_YES,
			NULL);
	gtk_window_set_title(GTK_WINDOW(dialog), "Confirm close");

	response = gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);

	/* decide what to do based on response */
	switch (response)
	{
	case RESPONSE_YES:
		_window_destroy(w, data);
		break;
	default:
		break;
	}

	return TRUE;
exit_no_mem:
	DESTROY(dialog);
	return FALSE;
}


void ecc_ui_connect_signals(struct ecc_ui_callbacks *callbacks,
		struct ecc_ui_widgets_to_remember *widgets_to_remember)
{
	/* program exit */
	g_signal_connect(widgets_to_remember->window, "delete-event", G_CALLBACK(_window_delete), callbacks);
	g_signal_connect(widgets_to_remember->window, "destroy", G_CALLBACK(_window_destroy), callbacks);
	g_signal_connect_swapped(widgets_to_remember->exit, "clicked", G_CALLBACK(_window_delete),
			widgets_to_remember->window);

	/* status bar */
	g_signal_connect(widgets_to_remember->logs, "clicked", G_CALLBACK(_show_logs), callbacks);

	/* TODO: for tree views, find a signal that unselects the selected element if focus is set to another widget */
}
