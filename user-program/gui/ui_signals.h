/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_UI_SIGNALS_H
#define ECC_UI_SIGNALS_H

#include <gtk/gtk.h>

struct ecc_ui_callbacks;

struct ecc_ui_widgets_to_remember
{
	/* whole window */
	GtkWidget *window;

	/* status bar */
	GtkWidget *status;
	GtkWidget *last_log;
	GtkWidget *progress_bar;
	GtkWidget *cancel_button;
	GtkWidget *separator;
	GtkWidget *logs;

	/* left panel */
	GtkWidget *network;
	GtkWidget *network_reset;
	GtkWidget *database;
	GtkWidget *database_clear;
	GtkWidget *database_save;
	GtkWidget *config;
	GtkWidget *config_clear;
	GtkWidget *config_save;
	GtkWidget *configure;
	GtkWidget *realias;
	GtkWidget *masters_count;
	GtkWidget *slaves_count;
	GtkWidget *names_count;
	GtkWidget *matched_count;
	GtkWidget *db_file_name;
	GtkWidget *domains_count;
	GtkWidget *config_file_name;

	/* right panel */
	GtkWidget *exit;
	GtkWidget *about;
	GtkWidget *help;
	GtkWidget *options;
	GtkWidget *undo_list;
	GtkWidget *redo_list;

	/* info/form area within the right panel */
	GtkWidget *guide;
	GtkWidget *masters_info;
	GtkWidget *slaves_info;
	GtkWidget *names_info;
	GtkWidget *matched_info;
	GtkWidget *domains_info;
	GtkWidget *slave_form;
	GtkWidget *slave_info;		/* part of slave_form */
	GtkWidget *slave_add_to_db;	/* part of slave_form */
	GtkWidget *slave_add_to_config;	/* part of slave_form */
	GtkWidget *slave_rm_from_db;	/* part of slave_form */
	GtkWidget *slave_rm_from_config;/* part of slave_form */
	GtkWidget *slave_realias;	/* part of slave_form */
	GtkWidget *slave_unalias;	/* part of slave_form */
	GtkWidget *options_form;
	GtkWidget *option_todo;		/* part of options */
	GtkWidget *option_todo2;	/* part of options */
	GtkWidget *add_to_db_form;
	GtkWidget *add_to_db_text;	/* part of add to db */
	GtkWidget *add_to_db_name;	/* part of add to db */
	GtkWidget *add_to_db_back;	/* part of add to db */
	GtkWidget *add_to_db_add;	/* part of add to db */
	GtkWidget *add_to_config_form;
	GtkWidget *add_to_config_text;	/* part of add to db */
	GtkWidget *add_to_config_domain;/* part of add to db */
	GtkWidget *add_to_config_entry;	/* part of add to db */
	GtkWidget *add_to_config_io;	/* part of add to db */
	GtkWidget *add_to_config_back;	/* part of add to db */
	GtkWidget *add_to_config_add;	/* part of add to db */
};

void ecc_ui_connect_signals(struct ecc_ui_callbacks *callbacks,
		struct ecc_ui_widgets_to_remember *widgets_to_remember);

#endif
