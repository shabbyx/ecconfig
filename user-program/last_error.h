/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_H
#define ERROR_H

#define ECC_ERROR_NO_MEM		1	/* not enough memory */
#define ECC_ERROR_NO_MASTER		2	/* could not open master */
#define ECC_ERROR_NO_FILE		3	/* could not open file */
#define ECC_ERROR_IOCTL			4	/* ioctl failed */
#define ECC_ERROR_MASTER_VERSION	5	/* bad master version */
#define ECC_ERROR_BAD_REQUEST		6	/* invalid request */
#define ECC_ERROR_PARSE_ERROR		7	/* error parsing file or command line */
#define ECC_ERROR_CORRUPT		8	/* corrupted file */
#define ECC_ERROR_NO_BACKUP		9	/* data safety not ensured */
#define ECC_ERROR_OUT_OF_ALIAS		10	/* maximum aliases reached */
#define ECC_ERROR_BAD_ARG		11	/* provided argument is invalid */
#define ECC_ERROR_EXISTS		12	/* entry already exists */
#define ECC_ERROR_WRITE			13	/* error writing to file */
#define ECC_ERROR_INTERNAL		14	/* internal error */

extern int ecc_error;

#endif
