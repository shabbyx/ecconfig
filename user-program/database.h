/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DATABASE_H
#define DATABASE_H

#include <stdint.h>
#include <stdbool.h>

/*
 * Structure of the database:
 *
 * Each group name is mapped to a number.  When scanning the network, the slaves that have
 * alias 0 are never configured.  The ones that have an alias that is not in the database
 * are branded as unknown.
 *
 * After the network is completely scanned, pointers to the matched slaves are kept,
 * while missing slaves contain a NULL pointer.
 *
 * When aliasing slaves, the internal representation of the network must be updated with
 * the new aliases.  Since it may be possible for a slave to now match a database entry,
 * the entries also need to be updated.  This includes adding new entries for new or
 * unknown slaves or changing the entry for reassigned slaves.
 *
 * Special direct manipulation functions also exist to add, modify or remove entries.
 *
 * Note: the write of the database consists of first backing up the previous file,
 * writing the database and then deleting the backup.  If anything goes wrong, you
 * can always retrieve the last database from the backup.
 *
 * The file format of the database is a series of chunks, each of the with the
 * following format:
 *
 * [name]
 * 	alias = num, num, num, num, ...
 *
 * The `name` must be unique in the database.  These define groups of slaves that
 * correspond to an area of interest in the robot.  In the configuration file later,
 * the `name` can be used to choose what slave configures to what.
 *
 * "*" and "UNKNOWN" are reserved names and cannot be used.  Additionally, "" and
 * any name containing '"' are invalid.
 *
 * Note: currently, searching entries for alias is linear.  If matching the database
 * and network was slow, improve this.
 */

#define ECC_MAX_ALIAS 65535

struct ecc_slave;
struct ecc_network;

typedef struct ecc_database_entry
{
	uint16_t alias;
	uint16_t name;		/* name string is database.names[name] */
	struct ecc_slave *match;
} ecc_database_entry;

typedef struct ecc_database
{
	char **names;		/* sorted */
	uint16_t names_count;
	ecc_database_entry *entries;
	uint32_t entries_count;

	uint32_t names_mem_size;
	uint32_t entries_mem_size;
	bool *aliases_used;	/* hash lookup of whether an alias is in use */
} ecc_database;

void ecc_init_database(ecc_database *db);
void ecc_free_database(ecc_database *db);

/*
 * database access
 *
 * read: read the database
 * write: backup the database, write the new one and remove the backup
 * saveas: change database filename and call write
 */
int ecc_read_database(ecc_database *db);
int ecc_write_database(ecc_database *db, bool force);
int ecc_saveas_database(ecc_database *db, const char *filename);

/*
 * operations on the database
 *
 * lookup_alias: linear search entries for alias
 * lookup_name: binary+linear search entries for name
 * lookup_name_next: get next entry with same name
 * match: match the database with the current network
 * modify: add or modify a slave in the database (updates alias field in network)
 * remove: remove a slave from the database (updates alias field in network)
 * 	if slave is not in the database, it will still unalias it
 */
struct ecc_database_entry *ecc_lookup_database_alias(const ecc_database *db,
		uint16_t alias);
struct ecc_database_entry *ecc_lookup_database_name(const ecc_database *db,
		const char *name);
struct ecc_database_entry *ecc_lookup_database_name_next(const ecc_database *db,
		const ecc_database_entry *cur);
void ecc_match_database(ecc_database *db, const struct ecc_network *network);
int ecc_add_entry_to_database(ecc_database *db, struct ecc_network *network,
		uint32_t master_index, uint32_t slave_position, const char *entry_name);
int ecc_remove_from_database(ecc_database *db, struct ecc_network *network,
		uint32_t master_index, uint32_t slave_position);

/*
 * Debug function: Logs the database using LOG macro
 */
void ecc_log_database(ecc_database *db);

#endif
