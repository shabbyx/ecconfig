/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "configure.h"
#include "network.h"
#include "database.h"
#include "log.h"
#include "last_error.h"
#include "ecc_config.h"

/*
 * Note: I could take the code from EtherCAT tool,
 * but it _may_ not be the same in different versions.
 *
 * Even though forking and executing for every slave
 * is not so efficient, it is more portable.
 */
static int _realias(uint32_t master, uint16_t position, uint16_t new_alias)
{
	pid_t pid;
	int status;
	char m[20];
	char p[10];
	char na[10];

	sprintf(m, "%u", master);
	sprintf(p, "%u", position);
	sprintf(na, "%u", new_alias);

	pid = fork();

	if (pid == -1)
	{
		LOG("failed to fork");
		return -1;
	}
	if (pid == 0)
	{
		/*
		 * the command to execute is:
		 *
		 * ethercat alias --master m  --position p  new_alias
		 *
		 * or in short:
		 *
		 * ethercat alias -m m  -p p  na
		 */
		execl(ECC_ETHERCAT_TOOL, ECC_ETHERCAT_TOOL, "alias",
				"-m", m, "-p", p, na, (char *)NULL);

		/* note: _Exit instead of exit not to call atexit() functions and close files */
		_Exit(EXIT_FAILURE);
	}

	LOG("executed: %s alias -m %s -p %s %s", ECC_ETHERCAT_TOOL, m, p, na);

	/* wait for child to terminate */
	if (waitpid(pid, &status, 0) == -1)
	{
		LOG("failed to wait for child");
		return -1;
	}

	if (WIFEXITED(status))
		return WEXITSTATUS(status)?-1:0;
	if (WIFSIGNALED(status))
		return -1;

	LOG("warning: waitpid claimed that the process is terminated, but its status says otherwise");

	return 0;
}

int ecc_realias(struct ecc_database *d, struct ecc_network *network)
{
	unsigned int i_master, i_slave;
	ecc_master *master;
	ecc_slave *slave;

	for (i_master = 0; i_master < network->masters_count; ++i_master)
	{
		master = &network->masters[i_master];

		for (i_slave = 0; i_slave < master->slaves_count; ++i_slave)
		{
			slave = &master->slaves[i_slave];

			if (slave->alias != slave->new_alias)
			{
				if (_realias(master->index, slave->position, slave->new_alias))
					return -1;

				slave->alias = slave->new_alias;
			}
		}
	}

	return 0;
}
