/*
 * Copyright (C) 2012-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ecconfig.h>
#include "network.h"
#include "database.h"
#include "config_input.h"
#include "configure.h"
#include "options.h"
#include "log.h"
#include "last_error.h"

void flush_line(void)
{
	while (!feof(stdin) && fgetc(stdin) != '\n');
}

const char *errors[] =
{
	[0] = "unknown error (please report)",
	[ECC_ERROR_NO_MEM] = "out of memory",
	[ECC_ERROR_NO_MASTER] = "could not open master",
	[ECC_ERROR_NO_FILE] = "could not open file or I/O error",
	[ECC_ERROR_IOCTL] = "ioctl failed",
	[ECC_ERROR_MASTER_VERSION] = "bad master version",
	[ECC_ERROR_BAD_REQUEST] = "invalid request",
	[ECC_ERROR_PARSE_ERROR] = "parse error",
	[ECC_ERROR_CORRUPT] = "corrupted file",
	[ECC_ERROR_NO_BACKUP] = "data safety not ensured",
	[ECC_ERROR_OUT_OF_ALIAS] = "maximum aliases reached",
	[ECC_ERROR_BAD_ARG] = "invalid argument",
	[ECC_ERROR_EXISTS] = "entry already exists",
	[ECC_ERROR_WRITE] = "error writing to file",
	[ECC_ERROR_INTERNAL] = "internal error"
};

static void output_slaves(const ecc_network *network,
		const ecc_database *db)
{
	unsigned int i_master, i_slave;
	ecc_master *master;
	ecc_slave *slave;
	ecc_database_entry *db_entry;

	printf("   master   | position |     name     |  vendor_id |   product  |   serial   | alias |    group     | new alias \n");
	printf("------------+----------+--------------+------------+------------+------------+-------+--------------+-----------\n");
	for (i_master = 0; i_master < network->masters_count; ++i_master)
	{
		master = &network->masters[i_master];
		for (i_slave = 0; i_slave < master->slaves_count; ++i_slave)
		{
			slave = &master->slaves[i_slave];
			db_entry = ecc_lookup_database_alias(db, slave->new_alias);

			printf(" %10u |   %5u  | %12.12s | %0#10x | %0#10x | %0#10x | %5u | %12.12s |   %5u   \n",
				master->index,
				slave->position,
				slave->name?slave->name:"<none>",
				slave->vendor_id,
				slave->product_code,
				slave->serial_number,
				slave->alias,
				db_entry?db->names[db_entry->name]:slave->new_alias != 0?"<unknown>":"<new>",
				slave->new_alias);
		}
	}
}

static void output_options(void)
{
	unsigned int i;

	printf("- masters:");
	for (i = 0; i < ecc_option_masters_count; ++i)
		printf(" %u", ecc_option_masters[i]);
	printf("\n");
	printf("- log %s\n", ecc_option_no_log?"disabled":"enabled");
	printf("- %s\n", ecc_option_silent?"silent":"verbose");
	printf("- config file: %s\n", ecc_option_config_file?ecc_option_config_file:"<null>");
	printf("- database file: %s\n", ecc_option_database_file?ecc_option_database_file:"<null>");
	printf("- log file: %s\n", ecc_option_log_file?ecc_option_log_file:"<null>");
}

int main(int argc, char **argv)
{
	int ret;
	unsigned int choice = 1;
	ecc_network network;
	ecc_database db;
	ecc_config conf;
	char str[101] = "";
	char str2[101] = "";
	char *new_db_name = NULL;
	char *new_config_name = NULL;
	bool matched = false;
	int master_id, slave_pos;
	int domain_id, is_input;
	FILE *commands_out = NULL;

	ecc_init_network(&network);
	ecc_init_database(&db);
	ecc_init_config(&conf);

	ret = ecc_parse_options(argc, argv);
	if (ret != 0)
	{
		if (ret > 0)
			ret = EXIT_SUCCESS;
		else if (ret < 0)
			ret = EXIT_FAILURE;
		goto exit_cleanup;
	}

	ecc_log_open();

	while (choice != 0)
	{
		printf("Select operation:\n");
		printf("0. Quit\n");
		printf("--- Network Operations ---\n");
		printf("1. Scan the network              2. List slaves                   3. Log the network\n");
		printf("--- Database operations ---\n");
		printf("4. Select database               5. Read database                 6. Save database                 7. Saveas database\n");
		printf("8. Add to/Modify database        9. Remove entry from database    10. Unalias slave                11. Log the database\n");
		printf("--- Configuration Operations ---\n");
		printf("12. Select config file           13. Read config file             14. Save config file             15. Saveas config file\n");
		printf("16. Add to configuration         17. Remove from configuration    18. Realias slaves               19. Configure the network\n");
		printf("20. Log the configuration\n");
		printf("--- Miscellaneous\n");
		printf("21. Show parameters\n");
		printf("> ");
		if (scanf("%u", &choice) != 1)
			goto invalid_input;
		if (choice > 21)
			goto invalid_input;
		flush_line();
		switch (choice)
		{
		case 1:
			if (ecc_scan_network(&network) < ecc_option_masters_count)
				printf("Error scanning the network: %s\n", errors[ecc_error]);
			matched = false;
			break;
		case 2:
			if (!matched)
				ecc_match_database(&db, &network);
			matched = true;
			output_slaves(&network, &db);
			break;
		case 3:
			ecc_log_network(&network);
			break;
		case 4:
			printf("Database name (* to cancel): ");
			if (scanf("%100s", str) != 1)
				goto invalid_input;
			if (strcmp(str, "*") != 0)
			{
				free(new_db_name);
				new_db_name = strdup(str);
				if (new_db_name == NULL)
					goto exit_no_mem;
				ecc_option_database_file = new_db_name;
				ecc_free_database(&db);
				matched = false;
			}
			break;
		case 5:
			if (ecc_read_database(&db))
				printf("Error reading the database: %s\n", errors[ecc_error]);
			matched = false;
			break;
		case 6:
			if (ecc_write_database(&db, false))
				printf("Error writing the database: %s\n", errors[ecc_error]);
			break;
		case 7:
			printf("database name (* to cancel): ");
			if (scanf("%100s", str) != 1)
				goto invalid_input;
			if (strcmp(str, "*") == 0)
				break;
			free(new_db_name);
			new_db_name = strdup(str);
			if (new_db_name == NULL)
				goto exit_no_mem;
			if (ecc_saveas_database(&db, new_db_name))
				printf("Error writing the database: %s\n", errors[ecc_error]);
			break;
		case 8:
		case 9:
		case 10:
			if (!matched)
				ecc_match_database(&db, &network);
			matched = true;
			printf("Master id (-1 to cancel): ");
			if (scanf("%d", &master_id) != 1)
				goto invalid_input;
			if (master_id == -1)
				break;
			flush_line();
			printf("Slave position (-1 to cancel): ");
			if (scanf("%d", &slave_pos) != 1)
				goto invalid_input;
			if (slave_pos == -1)
				break;
			if (choice == 8)
			{
				flush_line();
				printf("Group name (* to cancel): ");
				if (scanf("%100s", str) != 1)
					goto invalid_input;
				if (strcmp(str, "*") == 0)
					break;
				if (ecc_add_entry_to_database(&db, &network, master_id, slave_pos, str))
					printf("Error modifying the database: %s\n", errors[ecc_error]);
			}
			else if (choice == 9 || choice == 10)	/* remove from db and unalias are the same function */
				if (ecc_remove_from_database(&db, &network, master_id, slave_pos))
					printf("Error removing from the database: %s\n", errors[ecc_error]);
			break;
		case 11:
			ecc_log_database(&db);
			break;
		case 12:
			printf("Config file name (* to cancel): ");
			if (scanf("%100s", str) != 1)
				goto invalid_input;
			if (strcmp(str, "*") != 0)
			{
				free(new_config_name);
				new_config_name = strdup(str);
				if (new_config_name == NULL)
					goto exit_no_mem;
				ecc_option_config_file = new_config_name;
				ecc_free_config(&conf);
			}
			break;
		case 13:
			if (ecc_read_config(&conf))
				printf("Error reading the config file: %s\n", errors[ecc_error]);
			break;
		case 14:
			if (ecc_write_config(&conf, false))
				printf("Error writing the config file: %s\n", errors[ecc_error]);
			break;
		case 15:
			printf("config file name (* to cancel): ");
			if (scanf("%100s", str) != 1)
				goto invalid_input;
			if (strcmp(str, "*") == 0)
				break;
			free(new_config_name);
			new_config_name = strdup(str);
			if (new_config_name == NULL)
				goto exit_no_mem;
			if (ecc_saveas_config(&conf, new_config_name))
				printf("Error writing the config file: %s\n", errors[ecc_error]);
			break;
		case 16:
		case 17:
			printf("Domain id (-1 to cancel): ");
			if (scanf("%d", &domain_id) != 1)
				goto invalid_input;
			if (domain_id == -1)
				break;
			flush_line();
			printf("Slave name (* to cancel): ");
			if (scanf("%100s", str) != 1)
				goto invalid_input;
			if (strcmp(str, "*") == 0)
				break;
			flush_line();
			printf("Entry name (* to cancel): ");
			if (scanf("%100s", str2) != 1)
				goto invalid_input;
			if (strcmp(str2, "*") == 0)
				break;
			flush_line();
			printf("Input (1) or output (0) (-1 to cancel): ");
			if (scanf("%d", &is_input) != 1)
				goto invalid_input;
			if (is_input == -1)
				break;
			if (choice == 15)
			{
				if (ecc_add_entry_to_config(&conf, domain_id, str, str2, is_input))
					printf("Error modifying the configuration: %s\n", errors[ecc_error]);
			}
			else
				if (ecc_remove_from_config(&conf, domain_id, str, str2, is_input))
					printf("Error removing from the configuration: %s\n", errors[ecc_error]);
			break;
		case 18:
			if (ecc_realias(&db, &network))
				printf("Error realiasing slaves\n");
			break;
		case 19:
			if (!matched)
				ecc_match_database(&db, &network);
			if (ecc_realias(&db, &network))
			{
				printf("Error realiasing slaves\n");
				break;
			}
			matched = true;
			printf("Do you want to write commands to file instead? (Y/n)\n");
			if (scanf(" %c", &str[0]) != 1)
				goto invalid_input;
			if (str[0] == 'Y' || str[0] == 'y')
			{
				flush_line();
				printf("File name (* to cancel): ");
				if (scanf("%100s", str) != 1)
					goto invalid_input;
				if (strcmp(str, "*") == 0)
					break;
				commands_out = fopen(str, "w");
			}
			else
				commands_out = fopen("/proc/"ECC_PROCFS_FILE_NAME, "w");
			if (commands_out == NULL)
			{
				fprintf(stderr, "failed to open file\n");
				break;
			}
			if (ecc_configure(commands_out, &conf, &db, &network))
				printf("Error configuring the network: %s\n", errors[ecc_error]);
			fclose(commands_out);
			break;
		case 20:
			ecc_log_config(&conf);
			break;
		case 21:
			output_options();
			break;
		default:
			break;
		}
		continue;
	invalid_input:
		if (feof(stdin))
			choice = 0;
		else
		{
			printf("Invalid input\n");
			clearerr(stdin);
		}
		flush_line();
		if (feof(stdin))
			choice = 0;
	}

	ret = EXIT_SUCCESS;
	goto exit_cleanup;
exit_no_mem:
	fprintf(stderr, "out of memory\n");
	ret = EXIT_FAILURE;
exit_cleanup:
	ecc_free_network(&network);
	ecc_free_database(&db);
	ecc_free_config(&conf);
	ecc_log_close();
	ecc_free_options();
	free(new_db_name);
	free(new_config_name);

	return 0;
}
