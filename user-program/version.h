/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_VERSION_H
#define ECC_VERSIO_H

#define ECC_STRINGIFY_(x) #x
#define ECC_STRINGIFY(x) ECC_STRINGIFY_(x)

#define ECC_VERSION_MAJOR 0
#define ECC_VERSION_MINOR 2
#define ECC_VERSION_REVISION 0
#define ECC_VERSION_BUILD 97
#define ECC_VERSION ECC_STRINGIFY(ECC_VERSION_MAJOR)"."		\
		ECC_STRINGIFY(ECC_VERSION_MINOR)"."		\
		ECC_STRINGIFY(ECC_VERSION_REVISION)"."		\
		ECC_STRINGIFY(ECC_VERSION_BUILD)

#endif
