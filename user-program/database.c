/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <malloc.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "database.h"
#include "network.h"
#include "log.h"
#include "last_error.h"

static bool _name_is_valid(const char *name, char term)
{
	size_t len;

	for (len = 0; name[len] != term; ++len)
		if (name[len] == '"')
			return false;

	if (len == 0
		|| strncmp(name, "*", len) == 0
		|| strncmp(name, "UNKNOWN", len) == 0)
		return false;

	return true;
}

void ecc_init_database(ecc_database *db)
{
	*db = (ecc_database){0};
	db->aliases_used = malloc((ECC_MAX_ALIAS + 1) * sizeof(*db->aliases_used));
	memset(db->aliases_used, false, (ECC_MAX_ALIAS + 1) * sizeof(*db->aliases_used));
}

void ecc_free_database(ecc_database *db)
{
	uint32_t i;

	if (db->names)
		for (i = 0; i < db->names_count; ++i)
			free(db->names[i]);
	free(db->entries);
	free(db->names);
	free(db->aliases_used);

	*db = (ecc_database){0};
}

/*
 * returns id of name inserted, or a negative number in case of error
 * assumes name is unique, so _find_name must have previously been called
 */
static int _add_name(ecc_database *db, const char *name)
{
	char *name_copy;
	unsigned int i, j;

	if (db->names_count >= db->names_mem_size)
	{
		char **enlarged;
		uint32_t new_size;

		new_size = db->names_mem_size * 2 + 1;
		enlarged = realloc(db->names, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		db->names = enlarged;
		db->names_mem_size = new_size;
	}

	name_copy = strdup(name);
	if (name_copy == NULL)
		return -1;

	/* insert the name in its correct location */
	++db->names_count;
	for (i = db->names_count - 1; i > 0; --i)
		if (strcmp(name, db->names[i - 1]) < 0)
			db->names[i] = db->names[i - 1];
		else
			break;
	db->names[i] = name_copy;

	/* fix name ids of entries entered so far */
	for (j = 0; j < db->entries_count; ++j)
		if (db->entries[j].name >= i)
			++db->entries[j].name;

	return i;
}

static int _namecmp(const void *n1, const void *n2)
{
	return strcmp(*(const char * const *)n1, *(const char * const *)n2);
}

/* returns id of name found, or a negative number if doesn't exist */
static int _find_name(const ecc_database *db, const char *name)
{
	if (db->names)
	{
		char **pos;

		pos = bsearch(&name, db->names, db->names_count, sizeof(*db->names), _namecmp);
		if (pos != NULL)
			return pos - db->names;
	}
	return -1;
}

/* returns id of entry with given alias, or a negative number of doesn't exist */
static int _find_alias(const ecc_database *db, uint16_t alias)
{
	int i;

	if (db->aliases_used && !db->aliases_used[alias])
		return -1;

	for (i = 0; i < db->entries_count; ++i)
		if (db->entries[i].alias == alias)
			return i;

	return -1;
}

static bool _alias_is_in_use(const ecc_database *db, uint16_t alias)
{
	if (db->aliases_used)
		return db->aliases_used[alias];
	return _find_alias(db, alias) != -1;
}

/* returns the id of where the entry was inserted, or -1 if error */
static int _add_entry(ecc_database *db, uint16_t name_id, uint16_t alias)
{
	unsigned int i;

	if (db->entries_count >= db->entries_mem_size)
	{
		ecc_database_entry *enlarged;
		uint32_t new_size;

		new_size = db->entries_mem_size * 2 + 1;
		enlarged = realloc(db->entries, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			goto exit_no_mem;
		db->entries = enlarged;
		db->entries_mem_size = new_size;
	}

	/* put the entry in its sorted location */
	++db->entries_count;
	for (i = db->entries_count - 1; i > 0; --i)
		if (name_id < db->entries[i - 1].name ||
			(name_id == db->entries[i - 1].name
			 	&& alias < db->entries[i - 1].alias))
			db->entries[i] = db->entries[i - 1];
		else
			break;
	db->entries[i] = (ecc_database_entry){
		.alias = alias,
		.name = name_id,
		.match = NULL
	};
	if (db->aliases_used)
		db->aliases_used[alias] = true;

	return i;
exit_no_mem:
	MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	return -1;
}

static int _parse_name(const char *buf, ssize_t read, char **name)
{
	char *enlarged;

	if (read < 3 || !_name_is_valid(buf + 1, ']'))
		goto exit_bad_format;

	/* eliminate terminating ']' */
	--read;
	while (read > 0 && buf[read] != ']')
		--read;
	if (read == 0)
		goto exit_bad_format;

	/* copy the name */
	enlarged = realloc(*name, read * sizeof(*enlarged));
	if (enlarged == NULL)
		goto exit_no_mem;

	*name = enlarged;
	strncpy(*name, buf + 1, read);
	(*name)[read - 1] = '\0';

	return 0;
exit_no_mem:
	MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_bad_format:
	LOG("bad database format");
	ecc_error = ECC_ERROR_PARSE_ERROR;
	goto exit_fail;
exit_fail:
	return -1;
}

static int _parse_and_add_aliases(ecc_database *db, uint16_t name_id, const char *buf)
{
	unsigned int alias;
	int read = 0;

	if (sscanf(buf, "%u%n", &alias, &read) != 1)
		goto exit_bad_format;
	do
	{
		if (alias < 1 || alias > ECC_MAX_ALIAS)
			goto next_bad_alias;

		if (_alias_is_in_use(db, alias))
			goto next_exists;

		if (_add_entry(db, name_id, alias) < 0)
			goto exit_fail;

		goto next;
	next_bad_alias:
		LOG("invalid alias %u for name \"%s\"", alias, db->names[name_id]);
		ecc_error = ECC_ERROR_CORRUPT;
		goto next;
	next_exists:
		LOG("duplicate alias %u for name \"%s\"", alias, db->names[name_id]);
		ecc_error = ECC_ERROR_CORRUPT;
		goto next;
	next:
		buf += read;
	} while (sscanf(buf, "%*[, ]%u%n", &alias, &read) == 1);

	return 0;
exit_bad_format:
	LOG("bad database format");
	ecc_error = ECC_ERROR_PARSE_ERROR;
	goto exit_fail;
exit_fail:
	return -1;
}

int ecc_read_database(ecc_database *db)
{
	FILE *dbin;
	ssize_t read;
	size_t buf_size;
	char *buf = NULL;
	char *cur_name = NULL;
	int cur_name_id = -1;
	int ret = 0;
	int line = 0;

	/* reset db if reread */
	if (db->entries)
	{
		ecc_free_database(db);
		ecc_init_database(db);
	}

	/* open the file */
	dbin = fopen(ecc_option_database_file, "r");
	if (dbin == NULL)
		goto exit_no_file;

	while ((read = getline(&buf, &buf_size, dbin)) != -1)
	{
		++line;
		/* ignore empty lines */
		if (buf[0] == '\n')
			continue;
		/* lines containing [name] */
		if (buf[0] == '[')
		{
			if (_parse_name(buf, read, &cur_name))
				goto next_error_line;
			cur_name_id = _find_name(db, cur_name);
			if (cur_name_id < 0)
				cur_name_id = _add_name(db, cur_name);
			if (cur_name_id < 0)
				goto exit_no_mem;
		}
		/* lines containing fields */
		else if ((buf[0] == ' ' || buf[0] == '\t') && cur_name_id != -1)
		{
			int n;
			char field[20];

			if (sscanf(buf, " %15[^ =] =%n", field, &n) != 1)
				goto next_unexpected_line;

			if (strcmp(field, "alias") == 0 || strcmp(field, "aliases") == 0)
			{
				/* read all aliases and create entries for each */
				if (_parse_and_add_aliases(db, cur_name_id, buf + n))
					goto next_error_line;
			}
			else
				goto next_unexpected_line;
		}
		continue;
	next_unexpected_line:
		LOG("unexpected input (ignored)");
		ecc_error = ECC_ERROR_CORRUPT;
		goto next_error_line;
	next_error_line:
		LOG("  in %s: %d", ecc_option_database_file, line);
	}
	if (!feof(dbin))
		goto exit_no_mem;

#ifndef NDEBUG
	ecc_log_database(db);
#endif

	goto exit_cleanup;
exit_no_file:
	LOG("could not open database file \"%s\"", ecc_option_database_file);
	ecc_error = ECC_ERROR_NO_FILE;
	goto exit_fail;
exit_no_mem:
	MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_fail:
	ecc_free_database(db);
	ret = -1;
exit_cleanup:
	free(cur_name);
	free(buf);
	if (dbin)
		fclose(dbin);

	return ret;
}

static int _write_database(ecc_database *db)
{
	FILE *dbout;
	unsigned int i;
	int ret = 0;

	dbout = fopen(ecc_option_database_file, "w");
	if (dbout == NULL)
		goto exit_fail;

	for (i = 0; i < db->entries_count; ++i)
	{
		ecc_database_entry *e = &db->entries[i];

		fprintf(dbout, "[%s]\n", db->names[e->name]);
		fprintf(dbout, "\talias%s = %u", (i + 1 < db->entries_count
			&& db->entries[i + 1].name == e->name)?"es":"", e->alias);
		while (i + 1 < db->entries_count
			&& db->entries[i + 1].name == e->name)
		{
			++i;
			e = &db->entries[i];

			fprintf(dbout, " %u", e->alias);
		}
		fprintf(dbout, "\n");
	}
	if (ferror(dbout))
		goto exit_fail;
	goto cleanup;
exit_fail:
	ret = -1;
cleanup:
	if (dbout)
		fclose(dbout);
	return 0;
}

int ecc_write_database(ecc_database *db, bool force)
{
	char *backup = NULL;
	int ret = 0;
	struct stat s;

	/* take a backup if database file exists */
	if (stat(ecc_option_database_file, &s) == 0)
	{
		backup = malloc((strlen(ecc_option_database_file) + 10) * sizeof(*backup));
		if (backup == NULL)
			goto exit_no_mem;
		strcpy(backup, ecc_option_database_file);
		strcat(backup, ".backup");

		if (rename(ecc_option_database_file, backup) && !force)
			goto exit_no_backup;
	}

	/* write the database */
	if (_write_database(db))
		goto exit_bad_write;

	/* remove the backup */
	if (backup)
		remove(backup);

	goto cleanup;
exit_no_backup:
	LOG("could not backup database file");
	LOG("  new database was not written");
	ecc_error = ECC_ERROR_NO_BACKUP;
	goto exit_fail;
exit_no_mem:
	LOG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_bad_write:
	LOG("failed to write database");
	LOG("  retrieve old database from .backup file");
	ecc_error = ECC_ERROR_NO_FILE;
	goto exit_fail;
exit_fail:
	ret = -1;
cleanup:
	free(backup);
	return ret;
}

int ecc_saveas_database(ecc_database *db, const char *filename)
{
	ecc_option_database_file = filename;
	return ecc_write_database(db, false);
}

static int _entrycmp(const void *e1, const void *e2)
{
	const ecc_database_entry *first = e1;
	const ecc_database_entry *second = e2;

	if (first->name < second->name)
		return -1;
	if (first->name > second->name)
		return 1;
	return 0;
}

ecc_database_entry *ecc_lookup_database_alias(const ecc_database *db, uint16_t alias)
{
	int pos;

	pos = _find_alias(db, alias);
	if (pos < 0)
		return NULL;

	return &db->entries[pos];
}

ecc_database_entry *ecc_lookup_database_name(const ecc_database *db,
		const char *name)
{
	int name_id;
	int pos;
	ecc_database_entry to_find;
	ecc_database_entry *found;

	/* find name */
	name_id = _find_name(db, name);
	if (name_id < 0)
		return NULL;

	/* find one entry with this name */
	to_find.name = name_id;
	found = bsearch(&to_find, db->entries, db->entries_count, sizeof(*db->entries), _entrycmp);
	if (found == NULL)
		return NULL;
	pos = found - db->entries;

	/* go back until you find the first entry with this name */
	for (; pos > 0; --pos)
		if (db->entries[pos - 1].name != name_id)
			break;

	return &db->entries[pos];
}

ecc_database_entry *ecc_lookup_database_name_next(const ecc_database *db,
		const ecc_database_entry *cur)
{
	unsigned int pos;

	pos = cur - db->entries;
	if (pos + 1 < db->entries_count && db->entries[pos + 1].name == cur->name)
		return &db->entries[pos + 1];

	return NULL;
}

void ecc_match_database(ecc_database *db, const ecc_network *network)
{
	unsigned int i_master, i_slave, i;
	ecc_master *master;
	ecc_slave *slave;
	ecc_database_entry *db_entry;

	for (i = 0; i < db->entries_count; ++i)
		db->entries[i].match = NULL;

	for (i_master = 0; i_master < network->masters_count; ++i_master)
	{
		master = &network->masters[i_master];
		for (i_slave = 0; i_slave < master->slaves_count; ++i_slave)
		{
			slave = &master->slaves[i_slave];
			slave->db_entry = NULL;
			if (slave->new_alias != 0)
			{
				db_entry = ecc_lookup_database_alias(db, slave->new_alias);
				if (db_entry)
				{
					db_entry->match = slave;
					slave->db_entry = db_entry;
				}
			}
		}
	}
}

/* this would be very slow if there is not enough memory for aliases_used map */
static uint16_t _get_free_alias(const ecc_database *db)
{
	unsigned int i = 0;

	for (i = 1; i <= ECC_MAX_ALIAS; ++i)
		if (!_alias_is_in_use(db, i))
			break;

	return i;
}

void _remove_from_database(ecc_database *db, ecc_database_entry *entry)
{
	unsigned int i;

	/* make alias available and unmatch with slave (if matched) */
	if (entry->match != NULL)
	{
		entry->match->new_alias = 0;
		entry->match->db_entry = NULL;
	}
	if (db->aliases_used)
		db->aliases_used[entry->alias] = false;

	/* shift everyone back */
	for (i = entry - db->entries + 1; i < db->entries_count; ++i)
		db->entries[i - 1] = db->entries[i];
	--db->entries_count;
}

int ecc_add_entry_to_database(ecc_database *db, ecc_network *network,
		uint32_t master_index, uint32_t slave_position, const char *entry_name)
{
	int ret = 0;
	int alias;
	int name_id;
	unsigned int i_master;
	ecc_master *master;
	ecc_slave *slave;
	int pos;

	if (!_name_is_valid(entry_name, '\0'))
		goto exit_bad_name;

	/* get master */
	for (i_master = 0; i_master < network->masters_count; ++i_master)
		if (network->masters[i_master].index == master_index)
			break;
	if (i_master == network->masters_count)
		goto exit_bad_master;
	master = &network->masters[i_master];

	/* get slave */
	if (slave_position >= master->slaves_count)
		goto exit_bad_slave;
	slave = &master->slaves[slave_position];

	/* if modifying an already existing entry, first remove it */
	if (slave->db_entry != NULL)
		_remove_from_database(db, slave->db_entry);

	/* get the name id of this name, or a new one if new name */
	name_id = _find_name(db, entry_name);
	if (name_id < 0)
		name_id = _add_name(db, entry_name);
	if (name_id < 0)
		goto exit_no_mem;

	/* get a free alias number */
	alias = _get_free_alias(db);
	if (alias == 0)
		goto exit_no_more_alias;

	/* add entry to the database */
	pos = _add_entry(db, name_id, alias);
	if (pos < 0)
		goto exit_fail;

	/* match with the slave */
	slave->db_entry = &db->entries[pos];
	slave->new_alias = db->entries[pos].alias;
	db->entries[pos].match = slave;

	goto exit_normal;
exit_bad_name:
	LOG("entry name cannot be empty, \"*\" or \"UNKNOWN\", neither can it contain a '\"'");
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_no_more_alias:
	LOG("there are no free aliases available");
	ecc_error = ECC_ERROR_OUT_OF_ALIAS;
	goto exit_fail;
exit_bad_master:
	LOG("invalid master id %u", master_index);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_bad_slave:
	LOG("invalid slave position %u in master %u", slave_position, master_index);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_no_mem:
	MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_fail:
	ret = -1;
exit_normal:
	return ret;
}

int ecc_remove_from_database(ecc_database *db, ecc_network *network,
		uint32_t master_index, uint32_t slave_position)
{
	int ret = 0;
	unsigned int i_master;
	ecc_master *master;
	ecc_slave *slave;

	/* get master */
	for (i_master = 0; i_master < network->masters_count; ++i_master)
		if (network->masters[i_master].index == master_index)
			break;
	if (i_master == network->masters_count)
		goto exit_bad_master;
	master = &network->masters[i_master];

	/* get slave */
	if (slave_position >= master->slaves_count)
		goto exit_bad_slave;
	slave = &master->slaves[slave_position];

	/* mark it as unaliased */
	slave->new_alias = 0;

	/* remove it from database if it exists */
	if (slave->db_entry != NULL)
		_remove_from_database(db, slave->db_entry);

	goto exit_normal;
exit_bad_master:
	LOG("invalid master id %u", master_index);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_bad_slave:
	LOG("invalid slave position %u in master %u", slave_position, master_index);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_fail:
	ret = -1;
exit_normal:
	return ret;
}

void ecc_log_database(ecc_database *db)
{
	unsigned int i_name, i_entry;
	ecc_database_entry *entry;

	LOG("The database:");
	if (db->names_count == 0)
		LOG("Empty");
	else
		LOG("Names:");

	for (i_name = 0; i_name < db->names_count; ++i_name)
		LOG("  %u. %s", i_name, db->names[i_name]);

	for (i_entry = 0; i_entry < db->entries_count; ++i_entry)
	{
		entry = &db->entries[i_entry];

		LOG("Entry: alias: %u", entry->alias);
		LOG("       name: %u(%s)", entry->name, db->names[entry->name]);
		if (entry->match == NULL)
			LOG("       unmatched");
		else
		{
			ecc_slave *slave = entry->match;

			LOG("       matched slave: alias: %u", slave->alias);
			LOG("                      position: %u", slave->position);
			LOG("                      vendor id: %u", slave->vendor_id);
			LOG("                      product code: %u", slave->product_code);
			LOG("                      serial number: %u", slave->serial_number);
			LOG("                      name: %s", slave->name?slave->name:"<none>");
		}
	}
}
