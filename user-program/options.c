/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>
#include "options.h"
#include "version.h"
#include "last_error.h"

uint32_t *ecc_option_masters = NULL;
uint32_t ecc_option_masters_count = 0;

bool ecc_option_no_log = false;
bool ecc_option_silent = false;
const char *ecc_option_log_file = "ecconfig.log";

const char *ecc_option_config_file = "config.xml";
const char *ecc_option_database_file = "network.db";

static const char *_help_string =
"Usage: ecconfig --help\n"
"       ecconfig --version\n"
"       ecconfig [--masters id0 id1 ...]\n"
"                [--no-log]\n"
"                [--log-file[=filename]]\n"
"                [--silent]\n"
"                [--database=filename]\n"
"                [config_filename]\n"
"\n"
"--help or -h:     Print this message\n"
"--version or -v:  Print the version number\n"
"--masters or -m:  Insert master ids of interest.  If option is missing,\n"
"                  master 0 will be taken\n"
"--no-log or -n:   Suppress logging to file\n"
"--log-file or -l: If not --no-log, logging will be done to this file.  If\n"
"                  filename is missing, stderr will be used.  If this option\n"
"                  is missing, ecconfig.log will be used\n"
"--silent or -s:   Suppress stdout messages\n"
"--database or -d: Read the database of previously configured network from\n"
"                  filename.\n"
"--:               Treat the rest of the arguments as non-option\n"
"\n"
"This program scans the network and sends configuration data to the kernel\n"
"module after inspecting the config file given by config_filename.  If\n"
"this argument is missing, config.xml will be used.\n";

static int _mastercmp(const void *id1, const void *id2)
{
	const uint32_t *first = id1;
	const uint32_t *second = id2;

	if (*first < *second)
		return -1;
	if (*first > *second)
		return 1;
	return 0;
}

static int _add_master(uint32_t id)
{
	uint32_t *enlarged = NULL;

	enlarged = realloc(ecc_option_masters, (ecc_option_masters_count + 1) * sizeof(*ecc_option_masters));
	if (enlarged == NULL)
		return -1;
	ecc_option_masters = enlarged;

	ecc_option_masters[ecc_option_masters_count] = id;
	++ecc_option_masters_count;

	return 0;
}

#define IS_OPTION(x, l, s) (strcmp(x, l) == 0 || strcmp(x, s) == 0)
#define IS_OPTION_E(x, l, s) (strncmp(x, l, sizeof(l) - 1) == 0 || strncmp(x, s, sizeof(s) - 1) == 0)

int ecc_parse_options(int argc, char **argv)
{
	int i;
	bool no_more_options = false;

	for (i = 1; i < argc; ++i)
	{
		if (no_more_options)
			ecc_option_config_file = argv[i];
		else if (strcmp(argv[i], "--") == 0)
			no_more_options = true;
		else if (IS_OPTION(argv[i], "--help", "-h"))
		{
			printf("%s", _help_string);
			return 1;
		}
		else if (IS_OPTION(argv[i], "--version", "-v"))
		{
			printf("%s\n", ECC_VERSION);
			return 1;
		}
		else if (IS_OPTION(argv[i], "--masters", "-m"))
		{
			unsigned int id;

			++i;
			while (i < argc)
				if (sscanf(argv[i], "%u", &id) == 1)
				{
					if (_add_master(id))
						goto exit_no_mem;
					++i;
				}
				else
					break;
		}
		else if (IS_OPTION(argv[i], "--no-log", "-n"))
			ecc_option_no_log = true;
		else if (IS_OPTION_E(argv[i], "--log-file=", "-l="))
			ecc_option_log_file = strchr(argv[i], '=') + 1;
		else if (IS_OPTION(argv[i], "--log-file", "-l"))
			ecc_option_log_file = NULL;
		else if (IS_OPTION(argv[i], "--silent", "-s"))
			ecc_option_silent = true;
		else if (IS_OPTION_E(argv[i], "--database=", "-d="))
		{
			const char *filename = strchr(argv[i], '=') + 1;
			if (filename[0] != '\0')
				ecc_option_database_file = filename;
			else
				goto exit_bad_option;
		}
		else
			ecc_option_config_file = argv[i];
	}

	if (ecc_option_masters_count == 0)
		_add_master(0);

	qsort(ecc_option_masters, ecc_option_masters_count,
			sizeof(*ecc_option_masters), _mastercmp);

	return 0;
exit_bad_option:
	printf("bad usage of option \"%s\"\n\n"
		"use --help for a description of options\n", argv[i]);
	ecc_error = ECC_ERROR_PARSE_ERROR;
	goto exit_fail;
exit_no_mem:
	printf("out of memory\n");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_fail:
	return -1;
}

void ecc_free_options(void)
{
	free(ecc_option_masters);
	ecc_option_masters = NULL;
	ecc_option_masters_count = 0;
}
