/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIGURE_H
#define CONFIGURE_H

#include <stdio.h>

struct ecc_config;
struct ecc_database;
struct ecc_network;

/*
 * Realias the slaves based on the database.  This function forks and
 * executes the EtherCAT tool since this functionality is not provided
 * through any API.
 *
 * The network does not need to be rescanned.
 */
int ecc_realias(struct ecc_database *d, struct ecc_network *network);

/* Send configuration commands to the kernel module */
int ecc_configure(FILE *out, struct ecc_config *conf, struct ecc_database *db,
		struct ecc_network *network);

#endif
