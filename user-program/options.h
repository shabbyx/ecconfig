/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_OPTIONS_H
#define ECC_OPTIONS_H

#include <stdbool.h>
#include <stdint.h>

/* network */
extern uint32_t *ecc_option_masters;		/* list of master ids, sorted */
extern uint32_t ecc_option_masters_count;	/* size of ecc_option_masters */

/* logging */
extern bool ecc_option_no_log;			/* if true, no logging will be done */
extern bool ecc_option_silent;			/* if true, no stdout output will be given */
extern const char *ecc_option_log_file;		/* name of log file */

/* input */
extern const char *ecc_option_config_file;	/* where network configuration will be read */
extern const char *ecc_option_database_file;	/* where the database of current network is stored */

/*
 * Parse the options and set the global variables.
 *
 * Returns 0 if everything is ok, or a negative value if there was an error.  It will
 * return a positive value if `--help` or `-h` has been requested, in which case it
 * already has printed the help string and the program needs to terminate
 */
int ecc_parse_options(int argc, char **argv);
void ecc_free_options(void);

/*
 * The following helper functions allow the options to be changed at runtime
 */
static inline void ecc_set_option_no_log(bool v) { ecc_option_no_log = v; }
static inline void ecc_set_option_log_file(const char *f) { ecc_option_log_file = f; };
static inline void ecc_set_option_silent(bool v) { ecc_option_silent = v; }
static inline void ecc_set_option_config_file(const char *f) { if (f) ecc_option_config_file = f; }
static inline void ecc_set_option_database_file(const char *f) { if (f) ecc_option_database_file = f; }

#endif
