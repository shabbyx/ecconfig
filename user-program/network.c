/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fcntl.h>
#include <sys/ioctl.h>
#include <master/ioctl.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include "network.h"
#include "log.h"
#include "last_error.h"

void ecc_init_network(ecc_network *network)
{
	*network = (ecc_network){0};
}

void ecc_free_network(ecc_network *network)
{
	uint32_t i;

	if (!network || !network->masters)
		return;

	for (i = 0; i < network->masters_count; ++i)
		ecc_free_master(&network->masters[i]);
	free(network->masters);

	*network = (ecc_network){0};
}

int copy_str(char **to, const char *from)
{
	char *enlarged;

	if (*to && strcmp(*to, from) == 0)
		return 0;

	enlarged = realloc(*to, (strlen(from) + 1) * sizeof(*enlarged));
	if (enlarged == NULL)
		return -1;

	*to = enlarged;
	strcpy(*to, from);

	return 0;
}

int ecc_scan_network(ecc_network *network)
{
	char devname[100];
	int fd;
	int ret = 0;
	uint32_t i_master;
	bool first_time = network->masters == NULL;
	ec_ioctl_module_t ec_module = {0};
	ec_ioctl_master_t ec_master = {0};
	ec_ioctl_slave_t ec_slave = {0};
	ec_ioctl_slave_sync_t ec_sync = {0};
	ec_ioctl_slave_sync_pdo_t ec_pdo = {0};
	ec_ioctl_slave_sync_pdo_entry_t ec_pdo_entry = {0};
	ec_ioctl_slave_sdo_t ec_sdo = {0};
	ec_ioctl_slave_sdo_entry_t ec_sdo_entry = {0};

	for (i_master = 0; i_master < ecc_option_masters_count; ++i_master)
	{
		ecc_master *master;
		unsigned int i_slave;

		sprintf(devname, "/dev/EtherCAT%u", ecc_option_masters[i_master]);
		fd = open(devname, O_RDONLY | O_NONBLOCK);	/* see NOTES in manpage of open(2) for O_NONBLOCK */
		if (fd == -1)
			goto next_bad_open;

		/* make sure we are working under the correct version */
		if (ioctl(fd, EC_IOCTL_MODULE, &ec_module) < 0)
			goto exit_bad_ioctl;
		if (ec_module.ioctl_version_magic != EC_IOCTL_VERSION_MAGIC)
			goto exit_bad_version;

		/* take the master */
		if (ioctl(fd, EC_IOCTL_MASTER, &ec_master) < 0)
			goto exit_bad_ioctl;

		/* add to/get in the structures */
		master = ecc_get_master(network, ecc_option_masters[i_master]);
		if (master == NULL)
			goto exit_no_mem;

		for (i_slave = 0; i_slave < ec_master.slave_count; ++i_slave)
		{
			ecc_slave *slave;
			unsigned int i_sync;
			unsigned int i_sdo;

			/* take slave */
			ec_slave.position = i_slave;
			if (ioctl(fd, EC_IOCTL_SLAVE, &ec_slave))
				goto exit_bad_ioctl;

			/* TODO: the following was in the original feeder: */
			/*if (ec_slave.sync_count == 0)	// if no sync, ignore slave
				continue;*/

			/* add to/get in the structures */
			slave = ecc_get_slave(master, ec_slave.position);
			if (slave == NULL)
				goto exit_no_mem;
			slave->alias = ec_slave.alias;
			slave->new_alias = ec_slave.alias;
			slave->vendor_id = ec_slave.vendor_id;
			slave->product_code = ec_slave.product_code;
			slave->serial_number = ec_slave.serial_number;
			if (copy_str(&slave->name, (const char *)ec_slave.name))
				goto exit_no_mem;

			for (i_sdo = 0; i_sdo < ec_slave.sdo_count; ++i_sdo)
			{
				ecc_sdo *sdo;
				unsigned int i_sdo_entry;

				/* take sdo */
				ec_sdo.slave_position = i_slave;
				ec_sdo.sdo_position = i_sdo;
				if (ioctl(fd, EC_IOCTL_SLAVE_SDO, &ec_sdo))
					goto exit_bad_ioctl;

				/* add to/get in the structures */
				sdo = ecc_get_sdo(slave, ec_sdo.sdo_index);
				if (sdo == NULL)
					goto exit_no_mem;
				if (copy_str(&sdo->name, (const char *)ec_sdo.name))
					goto exit_no_mem;

				for (i_sdo_entry = 0; i_sdo_entry < ec_sdo.max_subindex; ++i_sdo_entry)
				{
					ecc_sdo_entry *sdo_entry;

					/* take sdo entry */
					ec_sdo_entry.slave_position = i_slave;
					ec_sdo_entry.sdo_spec = -(int)i_sdo;	/* alternatively: sdo->index */
					ec_sdo_entry.sdo_entry_subindex = i_sdo_entry;
					if (ioctl(fd, EC_IOCTL_SLAVE_SDO_ENTRY, &ec_sdo_entry))
						goto exit_bad_ioctl;

					/* add to/get in the structures */
					sdo_entry = ecc_get_sdo_entry(sdo, sdo->index, ec_sdo_entry.sdo_entry_subindex);
					if (sdo_entry == NULL)
						goto exit_no_mem;
					sdo_entry->read_access[0] = ec_sdo_entry.read_access[EC_SDO_ENTRY_ACCESS_PREOP];
					sdo_entry->read_access[1] = ec_sdo_entry.read_access[EC_SDO_ENTRY_ACCESS_SAFEOP];
					sdo_entry->read_access[2] = ec_sdo_entry.read_access[EC_SDO_ENTRY_ACCESS_OP];
					sdo_entry->write_access[0] = ec_sdo_entry.write_access[EC_SDO_ENTRY_ACCESS_PREOP];
					sdo_entry->write_access[1] = ec_sdo_entry.write_access[EC_SDO_ENTRY_ACCESS_SAFEOP];
					sdo_entry->write_access[2] = ec_sdo_entry.write_access[EC_SDO_ENTRY_ACCESS_OP];
					if (copy_str(&sdo_entry->description, (const char *)ec_sdo_entry.description))
						goto exit_no_mem;
				}
			}

			for (i_sync = 0; i_sync < ec_slave.sync_count; ++i_sync)
			{
				ecc_sync *sync;
				unsigned int i_pdo;

				/* take sync */
				ec_sync.slave_position = i_slave;
				ec_sync.sync_index = i_sync;
				if (ioctl(fd, EC_IOCTL_SLAVE_SYNC, &ec_sync))
					goto exit_bad_ioctl;

				/* add to/get in the structures */
				sync = ecc_get_sync(slave, ec_sync.sync_index);
				if (sync == NULL)
					goto exit_no_mem;
				sync->is_output = (ec_sync.control_register >> 2) & 0x01;
				sync->has_watchdog = (ec_sync.control_register >> 6) & 0x01;

				for (i_pdo = 0; i_pdo < ec_sync.pdo_count; ++i_pdo)
				{
					ecc_pdo *pdo;
					unsigned int i_pdo_entry;

					/* take pdo */
					ec_pdo.slave_position = i_slave;
					ec_pdo.sync_index = i_sync;
					ec_pdo.pdo_pos = i_pdo;
					if (ioctl(fd, EC_IOCTL_SLAVE_SYNC_PDO, &ec_pdo))
						goto exit_bad_ioctl;

					/* add to/get in the structures */
					pdo = ecc_get_pdo(sync, ec_pdo.index);
					if (pdo == NULL)
						goto exit_no_mem;
					if (copy_str(&pdo->name, (const char *)ec_pdo.name))
						goto exit_no_mem;

					for (i_pdo_entry = 0; i_pdo_entry < ec_pdo.entry_count; ++i_pdo_entry)
					{
						ecc_pdo_entry *pdo_entry;

						/* take pdo entry */
						ec_pdo_entry.slave_position = i_slave;
						ec_pdo_entry.sync_index = i_sync;
						ec_pdo_entry.pdo_pos = i_pdo;
						ec_pdo_entry.entry_pos = i_pdo_entry;
						if (ioctl(fd, EC_IOCTL_SLAVE_SYNC_PDO_ENTRY, &ec_pdo_entry))
							goto exit_bad_ioctl;

						/* add to/get in the structures */
						pdo_entry = ecc_get_pdo_entry(pdo, ec_pdo_entry.index, ec_pdo_entry.subindex);
						if (pdo_entry == NULL)
							goto exit_no_mem;
						pdo_entry->bit_length = ec_pdo_entry.bit_length;
						if (copy_str(&pdo_entry->name, (const char *)ec_pdo_entry.name))
							goto exit_no_mem;
					}
				}
			}
		}

		++ret;
		goto cleanup;
	next_bad_open:
		if (first_time)
			LOG("could not open master %u", ecc_option_masters[i_master]);
		ecc_error = ECC_ERROR_NO_MASTER;
		continue;
	cleanup:
		close(fd);
	}

#ifndef NDEBUG
	ecc_log_network(network);
#endif

	return ret;
exit_bad_ioctl:
	if (first_time)
		LOG("error in ioctl (master: %u)", ecc_option_masters[i_master]);
	ecc_error = ECC_ERROR_IOCTL;
	goto exit_fail;
exit_bad_version:
	if (first_time)
		LOG("mismatch with EtherCAT version (master: %u)", ecc_option_masters[i_master]);
	ecc_error = ECC_ERROR_MASTER_VERSION;
	goto exit_fail;
exit_no_mem:
	if (first_time)
		MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_fail:
	return ret;
}

ecc_master *ecc_find_master(ecc_network *network, uint32_t index)
{
	unsigned int i;

	for (i = 0; i < network->masters_count; ++i)
		if (network->masters[i].index == index)
			return &network->masters[i];

	return NULL;
}

ecc_master *ecc_add_master(ecc_network *network)
{
	if (network->masters_count >= network->masters_mem_size)
	{
		ecc_master *enlarged;
		uint32_t new_size;

		new_size = network->masters_mem_size * 2 + 1;
		enlarged = realloc(network->masters, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		network->masters = enlarged;
		network->masters_mem_size = new_size;
	}
	network->masters[network->masters_count] = (ecc_master){0};
	return &network->masters[network->masters_count++];
}

ecc_slave *ecc_find_slave(ecc_master *master, uint16_t position)
{
	unsigned int i;

	for (i = 0; i < master->slaves_count; ++i)
		if (master->slaves[i].position == position)
			return &master->slaves[i];

	return NULL;
}

ecc_slave *ecc_add_slave(ecc_master *master)
{
	if (master->slaves_count >= master->slaves_mem_size)
	{
		ecc_slave *enlarged;
		uint32_t new_size;

		new_size = master->slaves_mem_size * 2 + 1;
		enlarged = realloc(master->slaves, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		master->slaves = enlarged;
		master->slaves_mem_size = new_size;
	}
	master->slaves[master->slaves_count] = (ecc_slave){0};
	return &master->slaves[master->slaves_count++];
}

ecc_sdo *ecc_find_sdo(ecc_slave *slave, uint32_t index)
{
	unsigned int i;

	for (i = 0; i < slave->sdos_count; ++i)
		if (slave->sdos[i].index == index)
			return &slave->sdos[i];

	return NULL;
}

ecc_sdo *ecc_add_sdo(ecc_slave *slave)
{
	if (slave->sdos_count >= slave->sdos_mem_size)
	{
		ecc_sdo *enlarged;
		uint32_t new_size;

		new_size = slave->sdos_mem_size * 2 + 1;
		enlarged = realloc(slave->sdos, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		slave->sdos = enlarged;
		slave->sdos_mem_size = new_size;
	}
	slave->sdos[slave->sdos_count] = (ecc_sdo){0};
	return &slave->sdos[slave->sdos_count++];
}

ecc_sdo_entry *ecc_find_sdo_entry(ecc_sdo *sdo, uint16_t index, uint8_t sub_index)
{
	unsigned int i;

	for (i = 0; i < sdo->entries_count; ++i)
		if (sdo->entries[i].index == index && sdo->entries[i].sub_index == sub_index)
			return &sdo->entries[i];

	return NULL;
}

ecc_sdo_entry *ecc_add_sdo_entry(ecc_sdo *sdo)
{
	if (sdo->entries_count >= sdo->entries_mem_size)
	{
		ecc_sdo_entry *enlarged;
		uint32_t new_size;

		new_size = sdo->entries_mem_size * 2 + 1;
		enlarged = realloc(sdo->entries, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		sdo->entries = enlarged;
		sdo->entries_mem_size = new_size;
	}
	sdo->entries[sdo->entries_count] = (ecc_sdo_entry){0};
	return &sdo->entries[sdo->entries_count++];
}

ecc_sync *ecc_find_sync(ecc_slave *slave, uint32_t index)
{
	unsigned int i;

	for (i = 0; i < slave->syncs_count; ++i)
		if (slave->syncs[i].index == index)
			return &slave->syncs[i];

	return NULL;
}

ecc_sync *ecc_add_sync(ecc_slave *slave)
{
	if (slave->syncs_count >= slave->syncs_mem_size)
	{
		ecc_sync *enlarged;
		uint32_t new_size;

		new_size = slave->syncs_mem_size * 2 + 1;
		enlarged = realloc(slave->syncs, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		slave->syncs = enlarged;
		slave->syncs_mem_size = new_size;
	}
	slave->syncs[slave->syncs_count] = (ecc_sync){0};
	return &slave->syncs[slave->syncs_count++];
}

ecc_pdo *ecc_find_pdo(ecc_sync *sync, uint32_t index)
{
	unsigned int i;

	for (i = 0; i < sync->pdos_count; ++i)
		if (sync->pdos[i].index == index)
			return &sync->pdos[i];

	return NULL;
}

ecc_pdo *ecc_add_pdo(ecc_sync *sync)
{
	if (sync->pdos_count >= sync->pdos_mem_size)
	{
		ecc_pdo *enlarged;
		uint32_t new_size;

		new_size = sync->pdos_mem_size * 2 + 1;
		enlarged = realloc(sync->pdos, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		sync->pdos = enlarged;
		sync->pdos_mem_size = new_size;
	}
	sync->pdos[sync->pdos_count] = (ecc_pdo){0};
	return &sync->pdos[sync->pdos_count++];
}

ecc_pdo_entry *ecc_find_pdo_entry(ecc_pdo *pdo, uint16_t index, uint8_t sub_index)
{
	unsigned int i;

	for (i = 0; i < pdo->entries_count; ++i)
		if (pdo->entries[i].index == index && pdo->entries[i].sub_index == sub_index)
			return &pdo->entries[i];

	return NULL;
}

ecc_pdo_entry *ecc_add_pdo_entry(ecc_pdo *pdo)
{
	if (pdo->entries_count >= pdo->entries_mem_size)
	{
		ecc_pdo_entry *enlarged;
		uint32_t new_size;

		new_size = pdo->entries_mem_size * 2 + 1;
		enlarged = realloc(pdo->entries, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return NULL;
		pdo->entries = enlarged;
		pdo->entries_mem_size = new_size;
	}
	pdo->entries[pdo->entries_count] = (ecc_pdo_entry){0};
	return &pdo->entries[pdo->entries_count++];
}

void ecc_free_master(ecc_master *master)
{
	uint32_t i;

	if (!master || !master->slaves)
		return;

	for (i = 0; i < master->slaves_count; ++i)
		ecc_free_slave(&master->slaves[i]);
	free(master->slaves);

	*master = (ecc_master){0};
}

void ecc_free_slave(ecc_slave *slave)
{
	uint32_t i;

	if (!slave)
		return;

	if (slave->syncs)
		for (i = 0; i < slave->syncs_count; ++i)
			ecc_free_sync(&slave->syncs[i]);
	if (slave->sdos)
		for (i = 0; i < slave->sdos_count; ++i)
			ecc_free_sdo(&slave->sdos[i]);
	free(slave->name);
	free(slave->syncs);
	free(slave->sdos);

	*slave = (ecc_slave){0};
}

void ecc_free_sdo(ecc_sdo *sdo)
{
	uint32_t i;

	if (!sdo)
		return;

	if (sdo->entries)
		for (i = 0; i < sdo->entries_count; ++i)
			ecc_free_sdo_entry(&sdo->entries[i]);
	free(sdo->name);
	free(sdo->entries);

	*sdo = (ecc_sdo){0};
}

void ecc_free_sdo_entry(ecc_sdo_entry *sdo_entry)
{
	if (!sdo_entry)
		return;

	free(sdo_entry->description);

	*sdo_entry = (ecc_sdo_entry){0};
}

void ecc_free_sync(ecc_sync *sync)
{
	uint32_t i;

	if (!sync || !sync->pdos)
		return;

	for (i = 0; i < sync->pdos_count; ++i)
		ecc_free_pdo(&sync->pdos[i]);
	free(sync->pdos);

	*sync = (ecc_sync){0};
}

void ecc_free_pdo(ecc_pdo *pdo)
{
	uint32_t i;

	if (!pdo)
		return;

	if (pdo->entries)
		for (i = 0; i < pdo->entries_count; ++i)
			ecc_free_pdo_entry(&pdo->entries[i]);
	free(pdo->name);
	free(pdo->entries);

	*pdo = (ecc_pdo){0};
}

void ecc_free_pdo_entry(ecc_pdo_entry *pdo_entry)
{
	if (!pdo_entry)
		return;

	free(pdo_entry->name);

	*pdo_entry = (ecc_pdo_entry){0};
}


void ecc_log_network(ecc_network *network)
{
	unsigned int i_master, i_slave;
	unsigned int i_sdo, i_sdo_entry;
	unsigned int i_sync, i_pdo, i_pdo_entry;
	ecc_master *master;
	ecc_slave *slave;
	ecc_sdo *sdo;
	ecc_sdo_entry *sdo_entry;
	ecc_sync *sync;
	ecc_pdo *pdo;
	ecc_pdo_entry *pdo_entry;

	LOG("The network:");
	if (network->masters_count == 0)
		LOG("Empty");

	for (i_master = 0; i_master < network->masters_count; ++i_master)
	{
		master = &network->masters[i_master];

		LOG("Master: index: %0#10x", master->index);
		LOG("        slaves: %u", master->slaves_count);

		for (i_slave = 0; i_slave < master->slaves_count; ++i_slave)
		{
			slave = &master->slaves[i_slave];

			LOG("  Slave: alias: %u", slave->alias);
			LOG("         position: %u", slave->position);
			LOG("         vendor id: %0#10x", slave->vendor_id);
			LOG("         product code: %0#10x", slave->product_code);
			LOG("         serial number: %0#10x", slave->serial_number);
			LOG("         name: %s", slave->name?slave->name:"<none>");
			LOG("         sdos: %u", slave->sdos_count);
			LOG("         syncs: %u", slave->syncs_count);

			for (i_sdo = 0; i_sdo < slave->sdos_count; ++i_sdo)
			{
				sdo = &slave->sdos[i_sdo];

				LOG("    SDO: index: %0#6x", sdo->index);
				LOG("         name: %s", sdo->name?sdo->name:"<none>");
				LOG("         entries: %u", sdo->entries_count);

				for (i_sdo_entry = 0; i_sdo_entry < sdo->entries_count; ++i_sdo_entry)
				{
					sdo_entry = &sdo->entries[i_sdo_entry];

					LOG("      SDO Entry: index: %0#6x", sdo_entry->index);
					LOG("                 sub index: %0#6x", sdo_entry->sub_index);
					LOG("                 description: %s", sdo_entry->description?sdo_entry->description:"<none>");
					LOG("                 access (preop, safeop and op): %c%c%c%c%c%c",
							sdo_entry->read_access[0]?'r':'-', sdo_entry->write_access[0]?'w':'-',
							sdo_entry->read_access[1]?'r':'-', sdo_entry->write_access[1]?'w':'-',
							sdo_entry->read_access[2]?'r':'-', sdo_entry->write_access[2]?'w':'-');
				}
			}

			for (i_sync = 0; i_sync < slave->syncs_count; ++i_sync)
			{
				sync = &slave->syncs[i_sync];

				LOG("    Sync: index: %0#10x", sync->index);
				LOG("          direction: %s", sync->is_output?"output":"input");
				LOG("          has watchdog: %s", sync->has_watchdog?"yes":"no");
				LOG("          pdos: %u", sync->pdos_count);

				for (i_pdo = 0; i_pdo < sync->pdos_count; ++i_pdo)
				{
					pdo = &sync->pdos[i_pdo];

					LOG("      PDO: index: %0#6x", pdo->index);
					LOG("           name: %s", pdo->name?pdo->name:"<none>");
					LOG("           entries: %u", pdo->entries_count);

					for (i_pdo_entry = 0; i_pdo_entry < pdo->entries_count; ++i_pdo_entry)
					{
						pdo_entry = &pdo->entries[i_pdo_entry];

						LOG("        PDO Entry: index: %0#6x", pdo_entry->index);
						LOG("                   sub index: %0#6x", pdo_entry->sub_index);
						LOG("                   bit length: %u", pdo_entry->bit_length);
						LOG("                   name: %s", pdo_entry->name?pdo_entry->name:"<none>");
					}
				}
			}
		}
	}
}
