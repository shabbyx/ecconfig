/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <mxml.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "config_input.h"
#include "log.h"
#include "last_error.h"

static bool _name_is_valid(const char *name)
{
	if (name[0] == '\0'
		|| strcmp(name, "*") == 0
		|| strcmp(name, "UNKNOWN") == 0)
		return false;

	for (; *name; ++name)
		if (*name == '"')
			return false;

	return true;
}

void ecc_init_config(ecc_config *config)
{
	*config = (ecc_config){0};
}

static void _free_config_slave(ecc_config_slave *slave)
{
	uint32_t i;

	if (slave->inputs)
		for (i = 0; i < slave->inputs_count; ++i)
			free(slave->inputs[i]);
	if (slave->outputs)
		for (i = 0; i < slave->outputs_count; ++i)
			free(slave->outputs[i]);
	free(slave->inputs);
	free(slave->outputs);
	free(slave->name);

	*slave = (ecc_config_slave){0};
}

static void _free_config_domain(ecc_config_domain *domain)
{
	uint32_t i;

	if (domain->slaves)
		for (i = 0; i < domain->slaves_count; ++i)
			_free_config_slave(&domain->slaves[i]);
	free(domain->slaves);

	*domain = (ecc_config_domain){0};
}

void ecc_free_config(ecc_config *config)
{
	uint32_t i;

	if (config->domains)
		for (i = 0; i < config->domains_count; ++i)
			_free_config_domain(&config->domains[i]);
	free(config->domains);

	*config = (ecc_config){0};
}

int ecc_read_config(ecc_config *conf)
{
	FILE *xmlin;
	mxml_node_t *tree = NULL;
	mxml_node_t *ecconfig_node;
	mxml_node_t *domain_node;
	mxml_node_t *slave_node;
	mxml_node_t *io_node;
	uint32_t domain_id = 0;
	int ret = 0;

	/* open the file */
	xmlin = fopen(ecc_option_config_file, "r");
	if (xmlin == NULL)
		goto exit_no_file;

	/* read the config file */
	tree = mxmlLoadFile(NULL, xmlin, MXML_OPAQUE_CALLBACK);
	fclose(xmlin);
	if (tree == NULL)
		goto exit_not_xml;

	/* get the <ecconfig> tag */
	ecconfig_node = mxmlFindElement(tree, tree, "ecconfig", NULL, NULL, MXML_DESCEND);
	if (ecconfig_node == NULL)
		goto exit_bad_format;

	for (domain_node = ecconfig_node->child; domain_node; domain_node = domain_node->next)
	{
		uint32_t slaves_seen = 0;

		/* ignore other than <domain> */
		if (domain_node->type != MXML_ELEMENT
			|| strcmp(domain_node->value.element.name, "domain") != 0)
			continue;

		for (slave_node = domain_node->child; slave_node; slave_node = slave_node->next)
		{
			const char *slave_name = NULL;
			int i;
			uint32_t ios_seen = 0;

			/* ignore other than <slave> */
			if (slave_node->type != MXML_ELEMENT
				|| strcmp(slave_node->value.element.name, "slave") != 0)
				continue;

			/* get the name attribute */
			for (i = 0; i < slave_node->value.element.num_attrs; ++i)
				if (strcmp(slave_node->value.element.attrs[i].name, "name") == 0)
					slave_name = slave_node->value.element.attrs[i].value;
			if (slave_name == NULL)
			{
				LOG("warning: missing name attribute in slave %u in domain %u",
						slaves_seen++, domain_id);
				continue;
			}
			if (!_name_is_valid(slave_name))
			{
				LOG("warning: name attribute in slave %u in domain %u is invalid",
						slaves_seen++, domain_id);
				continue;
			}

			for (io_node = slave_node->child; io_node; io_node = io_node->next)
			{
				const char *entry_name = NULL;
				bool is_input;

				if (io_node->type != MXML_ELEMENT)
					continue;

				/* ignore other than <input> and <output> */
				if (strcmp(io_node->value.element.name, "input") == 0)
					is_input = true;
				else if (strcmp(io_node->value.element.name, "output") == 0)
					is_input = false;
				else
					continue;

				/* make sure the entry has a name */
				if (io_node->child == NULL || io_node->child->type != MXML_OPAQUE)
				{
					LOG("warning: missing name for io node %u in slave %u in domain %u",
							ios_seen++, slaves_seen, domain_id);
					continue;
				}
				entry_name = io_node->child->value.opaque;
				if (!_name_is_valid(entry_name))
				{
					LOG("warning: name for io node %u in slave %u in domain %u is invalid",
							ios_seen++, slaves_seen, domain_id);
					continue;
				}

				/* add to configuration */
				if (ecc_add_entry_to_config(conf, domain_id, slave_name,
						entry_name, is_input))
					goto exit_fail;

				++ios_seen;
			}
			++slaves_seen;
		}
		++domain_id;
	}

	goto exit_cleanup;
exit_no_file:
	LOG("could not open config file \"%s\"", ecc_option_config_file);
	ecc_error = ECC_ERROR_NO_FILE;
	goto exit_fail;
exit_not_xml:
	LOG("file \"%s\" is not XML", ecc_option_config_file);
	ecc_error = ECC_ERROR_CORRUPT;
	goto exit_fail;
exit_bad_format:
	LOG("bad config file format");
	ecc_error = ECC_ERROR_PARSE_ERROR;
	goto exit_fail;
exit_fail:
	ret = -1;
exit_cleanup:
	if (tree)
		mxmlDelete(tree);
	return ret;
}

static const char *_xml_whitespace_callback(mxml_node_t *node, int where)
{
	const char *name = node->value.element.name;

	if (node->type != MXML_ELEMENT)
		return NULL;
	if (name == NULL)
		return NULL;

	switch (where)
	{
	case MXML_WS_BEFORE_OPEN:
		if (strcmp(name, "ecconfig") == 0)
			return NULL;
		if (strcmp(name, "domain") == 0)
			return "\t\t";
		if (strcmp(name, "slave") == 0)
			return "\t\t\t";
		if (strcmp(name, "input") == 0
			|| strcmp(name, "output") == 0)
			return "\t\t\t\t";
		return NULL;
	case MXML_WS_AFTER_OPEN:
		if (strcmp(name, "ecconfig") == 0)
			return "\n";
		if (strcmp(name, "domain") == 0)
			return "\n";
		if (strcmp(name, "slave") == 0)
			return "\n";
		if (strcmp(name, "input") == 0
			|| strcmp(name, "output") == 0)
			return NULL;
		return NULL;
	case MXML_WS_BEFORE_CLOSE:
		if (strcmp(name, "ecconfig") == 0)
			return "\t";
		if (strcmp(name, "domain") == 0)
			return "\t\t";
		if (strcmp(name, "slave") == 0)
			return "\t\t\t";
		if (strcmp(name, "input") == 0
			|| strcmp(name, "output") == 0)
			return NULL;
		return NULL;
	case MXML_WS_AFTER_CLOSE:
		if (strcmp(name, "ecconfig") == 0)
			return NULL;
		if (strcmp(name, "domain") == 0)
			return "\n";
		if (strcmp(name, "slave") == 0)
			return "\n";
		if (strcmp(name, "input") == 0
			|| strcmp(name, "output") == 0)
			return "\n";
		return NULL;
	default:
		return NULL;
	}
}

static int _write_config(const ecc_config *conf, const char *from, const char *to)
{
	FILE *xml = NULL;
	mxml_node_t *tree = NULL;
	mxml_node_t *ecconfig_node;
	mxml_node_t *domain_node;
	mxml_node_t *slave_node;
	mxml_node_t *io_node;
	unsigned int i, j, k;
	int ret = 0;

	/* open the file */
	if (from)
		xml = fopen(from, "r");
	if (xml != NULL)
	{
		/* read the config file */
		tree = mxmlLoadFile(NULL, xml, MXML_OPAQUE_CALLBACK);
		fclose(xml);
	}

	/* if file doesn't exist, or couldn't read file, create a new file */
	if (tree == NULL)
		tree = mxmlNewXML("1.0");
	if (tree == NULL)
		goto exit_no_mem;

	/* clear the <ecconfig> tag if the tree is loaded */
	ecconfig_node = mxmlFindElement(tree, tree, "ecconfig", NULL, NULL, MXML_DESCEND);
	if (ecconfig_node != NULL)
	{
		domain_node = ecconfig_node->child;
		while (domain_node)
		{
			mxml_node_t *next = domain_node->next;
			mxmlDelete(domain_node);
			domain_node = next;
		}
	}
	else
	{
		/* otherwise, create a new <ecconfig> tag */
		if (mxmlNewOpaque(tree, "\n\t") == NULL)
			goto exit_no_mem;
		ecconfig_node = mxmlNewElement(tree, "ecconfig");
		if (ecconfig_node == NULL)
			goto exit_no_mem;
		if (mxmlNewOpaque(tree, "\n") == NULL)
			goto exit_no_mem;
	}

	for (i = 0; i < conf->domains_count; ++i)
	{
		const ecc_config_domain *domain = &conf->domains[i];

		if (domain->slaves_count == 0)
			continue;

		/* create a <domain> tag */
		domain_node = mxmlNewElement(ecconfig_node, "domain");
		if (domain_node == NULL)
			goto exit_no_mem;

		for (j = 0; j < domain->slaves_count; ++j)
		{
			const ecc_config_slave *slave = &domain->slaves[j];

			if (slave->inputs_count == 0 && slave->outputs_count == 0)
				continue;

			/* create a <slave name="name"> tag */
			slave_node = mxmlNewElement(domain_node, "slave");
			if (slave_node == NULL)
				goto exit_no_mem;
			mxmlElementSetAttr(slave_node, "name", slave->name);

			for (k = 0; k < slave->inputs_count; ++k)
			{
				/* create an <input> tag */
				io_node = mxmlNewElement(slave_node, "input");
				if (io_node == NULL)
					goto exit_no_mem;
				if (mxmlNewOpaque(io_node, slave->inputs[k]) == NULL)
					goto exit_no_mem;
			}

			for (k = 0; k < slave->outputs_count; ++k)
			{
				/* create an <output> tag */
				io_node = mxmlNewElement(slave_node, "output");
				if (io_node == NULL)
					goto exit_no_mem;
				if (mxmlNewOpaque(io_node, slave->outputs[k]) == NULL)
					goto exit_no_mem;
			}
		}
	}

	xml = fopen(to, "w");
	if (xml == NULL)
		goto exit_no_file;

	/* write the config file */
	ret = mxmlSaveFile(tree, xml, _xml_whitespace_callback);
	fclose(xml);
	if (ret)
		goto exit_no_file;

	goto exit_cleanup;
exit_no_file:
	LOG("could not open config file \"%s\"", ecc_option_config_file);
	ecc_error = ECC_ERROR_NO_FILE;
	goto exit_fail;
exit_no_mem:
	MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_fail:
	ret = -1;
exit_cleanup:
	if (tree)
		mxmlDelete(tree);
	return ret;
}

int ecc_write_config(const ecc_config *conf, bool force)
{
	char *backup = NULL;
	int ret = 0;
	struct stat s;

	/* take a backup if config file exists */
	if (stat(ecc_option_config_file, &s) == 0)
	{
		backup = malloc((strlen(ecc_option_config_file) + 10) * sizeof(*backup));
		if (backup == NULL)
			goto exit_no_mem;
		strcpy(backup, ecc_option_config_file);
		strcat(backup, ".backup");

		if (rename(ecc_option_config_file, backup) && !force)
			goto exit_no_backup;
	}

	/* write the config file */
	if (_write_config(conf, backup, ecc_option_config_file))
		goto exit_bad_write;

	/* remove the backup */
	if (backup)
		remove(backup);

	goto cleanup;
exit_no_backup:
	LOG("could not backup config file");
	LOG("  new config file was not written");
	ecc_error = ECC_ERROR_NO_BACKUP;
	goto exit_fail;
exit_no_mem:
	LOG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	goto exit_fail;
exit_bad_write:
	LOG("failed to write config");
	LOG("  retrieve old database from .backup file");
	/* ecc_error already set by _write_config */
	goto exit_fail;
exit_fail:
	ret = -1;
cleanup:
	free(backup);
	return ret;
}

int ecc_saveas_config(const ecc_config *conf, const char *filename)
{
	ecc_option_config_file = filename;
	return ecc_write_config(conf, false);
}

static int _slavecmp(const void *e1, const void *e2)
{
	return strcmp(((const ecc_config_slave *)e1)->name,
			((const ecc_config_slave *)e2)->name);
}

ecc_config_slave *ecc_lookup_config_slave(const ecc_config *conf, uint32_t domain,
		const char *name)
{
	const ecc_config_domain *d;
	const ecc_config_slave to_find = {
		.name = (char *)name
	};

	if (domain >= conf->domains_count)
		return NULL;

	d = &conf->domains[domain];

	return bsearch(&to_find, d->slaves, d->slaves_count, sizeof(*d->slaves), _slavecmp);
}

ecc_config_domain *ecc_lookup_config_domain(const ecc_config *conf, const char *name)
{
	unsigned int i;

	for (i = 0; i < conf->domains_count; ++i)
		if (ecc_lookup_config_slave(conf, i, name) != NULL)
			return &conf->domains[i];

	return NULL;
}

ecc_config_domain *ecc_lookup_config_domain_next(const ecc_config *conf,
		const ecc_config_domain *cur, const char *name)
{
	unsigned int pos;

	pos = cur - conf->domains;
	for (++pos; pos < conf->domains_count; ++pos)
		if (ecc_lookup_config_slave(conf, pos, name) != NULL)
			return &conf->domains[pos];

	return NULL;
}

static int _append_domain(ecc_config *conf)
{
	if (conf->domains_count >= conf->domains_mem_size)
	{
		ecc_config_domain *enlarged;
		uint32_t new_size;

		new_size = conf->domains_mem_size * 2 + 1;
		enlarged = realloc(conf->domains, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			return -1;
		conf->domains = enlarged;
		conf->domains_mem_size = new_size;
	}

	conf->domains[conf->domains_count] = (ecc_config_domain){
		.id = conf->domains_count,
	};
	++conf->domains_count;

	return 0;
}

static ecc_config_slave *_add_slave(ecc_config *conf, uint32_t domain, const char *slave)
{
	unsigned int i;
	ecc_config_domain *d;
	char *name_copy = NULL;

	d = &conf->domains[domain];

	name_copy = strdup(slave);
	if (name_copy == NULL)
		goto exit_no_mem;

	if (d->slaves_count >= d->slaves_mem_size)
	{
		ecc_config_slave *enlarged;
		uint32_t new_size;

		new_size = d->slaves_mem_size * 2 + 1;
		enlarged = realloc(d->slaves, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			goto exit_no_mem;
		d->slaves = enlarged;
		d->slaves_mem_size = new_size;
	}

	/* place slave in its sorted position */
	for (i = d->slaves_count; i > 0; --i)
		if (strcmp(slave, d->slaves[i - 1].name) < 0)
			d->slaves[i] = d->slaves[i - 1];
		else
			break;
	d->slaves[i] = (ecc_config_slave){
		.name = name_copy
	};
	++d->slaves_count;

	return &d->slaves[i];
exit_no_mem:
	free(name_copy);
	return NULL;
}

static int _add_entry(char ***entries, uint32_t *entries_count, uint32_t *entries_mem_size,
		const char *name)
{
	unsigned int i;
	char *name_copy = NULL;

	name_copy = strdup(name);
	if (name_copy == NULL)
		goto exit_no_mem;

	if (*entries_count >= *entries_mem_size)
	{
		char **enlarged;
		uint32_t new_size;

		new_size = *entries_mem_size * 2 + 1;
		enlarged = realloc(*entries, new_size * sizeof(*enlarged));
		if (enlarged == NULL)
			goto exit_no_mem;
		*entries = enlarged;
		*entries_mem_size = new_size;
	}

	/* place entry in its sorted position */
	for (i = *entries_count; i > 0; --i)
		if (strcmp(name, (*entries)[i - 1]) < 0)
			(*entries)[i] = (*entries)[i - 1];
		else
			break;
	(*entries)[i] = name_copy;
	++*entries_count;

	return 0;
exit_no_mem:
	free(name_copy);
	return -1;
}

static int _iocmp(const void *e1, const void *e2)
{
	return strcmp(*(const char * const *)e1, *(const char * const *)e2);
}

static int _remove_entry(char ***entries, uint32_t *entries_count, const char *name)
{
	unsigned int i;
	char **pos;

	pos = bsearch(&name, *entries, *entries_count, sizeof(**entries), _iocmp);
	if (pos == NULL)
		return -1;

	i = pos - *entries;
	free((*entries)[i]);

	for (++i; i < *entries_count; ++i)
		(*entries)[i - 1] = (*entries)[i];

	--*entries_count;

	return 0;
}

int ecc_add_entry_to_config(ecc_config *conf, uint32_t domain, const char *slave,
		const char *entry_name, bool is_input)
{
	ecc_config_slave *s;

	if (!_name_is_valid(entry_name))
		goto exit_bad_name;

	/* create a domain if domain id is new */
	while (domain >= conf->domains_count)
		if (_append_domain(conf))
			goto exit_no_mem;

	/* create a slave if slave->name is new */
	s = ecc_lookup_config_slave(conf, domain, slave);
	if (s == NULL)
		s = _add_slave(conf, domain, slave);
	if (s == NULL)
		goto exit_no_mem;

	/* make sure entry doesn't already exist */
	if (bsearch(&entry_name, s->outputs, s->outputs_count, sizeof(*s->outputs), _iocmp) != NULL
		|| bsearch(&entry_name, s->inputs, s->inputs_count, sizeof(*s->inputs), _iocmp) != NULL)
		goto exit_exists;

	/* create entry in the slave */
	if (is_input)
	{
		if (_add_entry(&s->inputs, &s->inputs_count, &s->inputs_mem_size, entry_name))
			goto exit_no_mem;
	}
	else
		if (_add_entry(&s->outputs, &s->outputs_count, &s->outputs_mem_size, entry_name))
			goto exit_no_mem;

	return 0;
exit_bad_name:
	LOG("entry name cannot be empty, \"*\" or \"UNKNOWN\", neither can it contain a '\"'");
	ecc_error = ECC_ERROR_BAD_ARG;
	return -1;
exit_exists:
	LOG("warning: entry \"%s\" of slave \"%s\" is already configured to be in domain %u",
			entry_name, slave, domain);
	ecc_error = ECC_ERROR_EXISTS;
	return 0;
exit_no_mem:
	MSG("out of memory");
	ecc_error = ECC_ERROR_NO_MEM;
	return -1;
}

static void _remove_slave(ecc_config *conf, uint32_t domain, uint32_t slave)
{
	unsigned int i;
	ecc_config_domain *d;

	d = &conf->domains[domain];

	_free_config_slave(&d->slaves[slave]);

	for (i = slave + 1; i < d->slaves_count; ++i)
		d->slaves[i - 1] = d->slaves[i];

	--d->slaves_count;
}

static void _remove_domain(ecc_config *conf, uint32_t domain)
{
	unsigned int i;

	_free_config_domain(&conf->domains[domain]);

	for (i = domain + 1; i < conf->domains_count; ++i)
	{
		conf->domains[i - 1] = conf->domains[i];
		conf->domains[i - 1].id = i - 1;
	}

	--conf->domains_count;
}

int ecc_remove_from_config(ecc_config *conf, uint32_t domain, const char *slave,
		const char *entry_name, bool is_input)
{
	ecc_config_slave *s;

	if (domain > conf->domains_count)
		goto exit_bad_domain;

	s = ecc_lookup_config_slave(conf, domain, slave);
	if (s == NULL)
		goto exit_bad_slave;

	/* remove the entry from slave */
	if (is_input)
	{
		if (_remove_entry(&s->inputs, &s->inputs_count, entry_name))
			goto exit_bad_entry;
	}
	else
		if (_remove_entry(&s->outputs, &s->outputs_count, entry_name))
			goto exit_bad_entry;

	/* if slave is empty, remove it from domain */
	if (s->inputs_count == 0 && s->outputs_count == 0)
		_remove_slave(conf, domain, s - conf->domains[domain].slaves);

	/* if domain is empty, remove it from configuration */
	if (conf->domains[domain].slaves_count == 0)
		_remove_domain(conf, domain);

	return 0;
exit_bad_domain:
	LOG("invalid domain id %u", domain);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_bad_slave:
	LOG("no such slave \"%s\" in domain %u", slave, domain);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_bad_entry:
	LOG("no such entry \"%s\" in slave \"%s\" in domain %u", entry_name, slave, domain);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_fail:
	return -1;
}

int ecc_remove_slave_from_config(ecc_config *conf, uint32_t domain, const char *slave)
{
	ecc_config_slave *s;

	if (domain > conf->domains_count)
		goto exit_bad_domain;

	s = ecc_lookup_config_slave(conf, domain, slave);
	if (s == NULL)
		goto exit_bad_slave;

	_remove_slave(conf, domain, s - conf->domains[domain].slaves);

	/* if domain is empty, remove it from configuration */
	if (conf->domains[domain].slaves_count == 0)
		_remove_domain(conf, domain);

	return 0;
exit_bad_domain:
	LOG("invalid domain id %u", domain);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_bad_slave:
	LOG("no such slave \"%s\" in domain %u", slave, domain);
	ecc_error = ECC_ERROR_BAD_ARG;
	goto exit_fail;
exit_fail:
	return -1;
}

void ecc_log_config(ecc_config *conf)
{
	uint32_t i_domain, i_slave;
	uint32_t i_input, i_output;
	ecc_config_domain *domain;
	ecc_config_slave *slave;

	LOG("The configuration:");
	if (conf->domains_count == 0)
		LOG("Empty");

	for (i_domain = 0; i_domain < conf->domains_count; ++i_domain)
	{
		domain = &conf->domains[i_domain];

		LOG("Domain: id: %u", domain->id);
		LOG("        slaves: %u", domain->slaves_count);

		for (i_slave = 0; i_slave < domain->slaves_count; ++i_slave)
		{
			slave = &domain->slaves[i_slave];

			LOG("  Slave: name: %s", slave->name);
			LOG("         inputs: %u", slave->inputs_count);
			LOG("         outputs: %u", slave->outputs_count);

			for (i_input = 0; i_input < slave->inputs_count; ++i_input)
				LOG("    Input: name: %s", slave->inputs[i_input]);

			for (i_output = 0; i_output < slave->outputs_count; ++i_output)
				LOG("    Output: name: %s", slave->outputs[i_output]);
		}
	}
}
