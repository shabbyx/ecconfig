/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_LOG_H
#define ECC_LOG_H

#include <stdio.h>
#include "options.h"

extern FILE *ecc_log_file;

#ifndef NDEBUG
# define LOG(fmt, ...)								\
	do {									\
		if (!ecc_option_no_log)						\
		{								\
			fprintf(ecc_log_file == NULL?stderr:ecc_log_file,	\
				"%s(%u): "fmt"\n",				\
				__FILE__ + (sizeof(__FILE__) < 15?0:		\
					(sizeof(__FILE__) - 15)),		\
				__LINE__, ##__VA_ARGS__);			\
			fflush(ecc_log_file == NULL?stderr:ecc_log_file);	\
		}								\
	} while (0)
# define DBG(fmt, ...) LOG(fmt, ##__VA_ARGS__)
#else
# define LOG(fmt, ...)								\
	do {									\
		if (!ecc_option_no_log)						\
			fprintf(ecc_log_file == NULL?stderr:ecc_log_file,		\
				fmt"\n", ##__VA_ARGS__);			\
	} while (0)
# define DBG(fmt, ...) ((void )0)
#endif

#define MSG(fmt, ...)								\
	do {									\
		if (!ecc_option_silent)						\
			fprintf(stdout, fmt"\n", ##__VA_ARGS__);		\
		LOG(fmt, ##__VA_ARGS__);					\
	} while (0)

int ecc_log_open(void);
void ecc_log_close(void);

#endif
