/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "log.h"
#include "last_error.h"

FILE *ecc_log_file = NULL;

int ecc_log_open(void)
{
	if (ecc_log_file)
		ecc_log_close();
	ecc_log_file = NULL;

	if (ecc_option_no_log)
	{
		ecc_error = ECC_ERROR_BAD_REQUEST;
		return -1;
	}

	if (ecc_option_log_file)
		ecc_log_file = fopen(ecc_option_log_file, "w");
	if (ecc_log_file == NULL)
	{
		ecc_error = ECC_ERROR_NO_FILE;
		return -1;
	}

	return 0;
}

void ecc_log_close(void)
{
	if (ecc_log_file)
		fclose(ecc_log_file);

	ecc_log_file = NULL;
}
