/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ECC_NETWORK_H
#define ECC_NETWORK_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

struct ecc_database_entry;

typedef struct ecc_pdo_entry
{
	uint16_t index;
	uint8_t sub_index;
	uint8_t bit_length;
	char *name;
} ecc_pdo_entry;

typedef struct ecc_pdo
{
	uint16_t index;
	char *name;

	ecc_pdo_entry *entries;
	uint32_t entries_count;

	uint32_t entries_mem_size;
} ecc_pdo;

typedef struct ecc_sync
{
	uint32_t index;
	bool is_output;		/* if not output, then it's input */
	bool has_watchdog;

	struct ecc_pdo *pdos;
	uint32_t pdos_count;

	uint32_t pdos_mem_size;
} ecc_sync;

typedef struct ecc_sdo_entry
{
	uint16_t index;		/* TODO: is this the same as index in ecc_sdo? */
	uint8_t sub_index;
	char *description;
	bool read_access[3];	/* preop, safeop and op */
	bool write_access[3];	/* preop, safeop and op */
} ecc_sdo_entry;

typedef struct ecc_sdo
{
	uint16_t index;
	char *name;

	ecc_sdo_entry *entries;
	uint32_t entries_count;

	uint32_t entries_mem_size;
} ecc_sdo;

typedef struct ecc_slave
{
	uint16_t alias;
	uint16_t position;
	uint32_t vendor_id;
	uint32_t product_code;
	uint32_t serial_number;
	char *name;
	uint16_t new_alias;	/* managed by database */

	ecc_sync *syncs;
	uint32_t syncs_count;
	ecc_sdo *sdos;
	uint32_t sdos_count;

	uint32_t syncs_mem_size;
	uint32_t sdos_mem_size;

	struct ecc_database_entry *db_entry;
} ecc_slave;

typedef struct ecc_master
{
	uint32_t index;

	ecc_slave *slaves;
	uint32_t slaves_count;

	uint32_t slaves_mem_size;
} ecc_master;

typedef struct ecc_network
{
	ecc_master *masters;		/* sorted on index */
	uint32_t masters_count;

	uint32_t masters_mem_size;
} ecc_network;

/*
 * The following functions are for creation and destruction of the structures.
 *
 * The add functions return NULL in case of error or a pointer to the newly
 * added structure.
 *
 * The find functions search and return the pointer to the found structure,
 * or NULL if non existant.
 *
 * The get functions try to search for the structure.  If not found, they will
 * add the structure and set the provided fields.  If an error occurred, they
 * will return NULL, otherwise they will return the pointer to the found/added
 * structure.
 */
ecc_master *ecc_find_master(ecc_network *network, uint32_t index);
ecc_master *ecc_add_master(ecc_network *network);
ecc_slave *ecc_find_slave(ecc_master *master, uint16_t position);
ecc_slave *ecc_add_slave(ecc_master *master);
ecc_sdo *ecc_find_sdo(ecc_slave *slave, uint32_t index);
ecc_sdo *ecc_add_sdo(ecc_slave *slave);
ecc_sdo_entry *ecc_find_sdo_entry(ecc_sdo *sdo, uint16_t index, uint8_t sub_index);
ecc_sdo_entry *ecc_add_sdo_entry(ecc_sdo *sdo);
ecc_sync *ecc_find_sync(ecc_slave *slave, uint32_t index);
ecc_sync *ecc_add_sync(ecc_slave *slave);
ecc_pdo *ecc_find_pdo(ecc_sync *sync, uint32_t index);
ecc_pdo *ecc_add_pdo(ecc_sync *sync);
ecc_pdo_entry *ecc_find_pdo_entry(ecc_pdo *pdo, uint16_t index, uint8_t sub_index);
ecc_pdo_entry *ecc_add_pdo_entry(ecc_pdo *pdo);
void ecc_free_master(ecc_master *master);
void ecc_free_slave(ecc_slave *slave);
void ecc_free_sdo(ecc_sdo *sdo);
void ecc_free_sdo_entry(ecc_sdo_entry *sdo_entry);
void ecc_free_sync(ecc_sync *sync);
void ecc_free_pdo(ecc_pdo *pdo);
void ecc_free_pdo_entry(ecc_pdo_entry *pdo_entry);

static inline ecc_master *ecc_get_master(ecc_network *network, uint32_t index)
{
	ecc_master *master = ecc_find_master(network, index);
	if (master == NULL)
	{
		master = ecc_add_master(network);
		if (master != NULL)
			master->index = index;
	}
	return master;
}

static inline ecc_slave *ecc_get_slave(ecc_master *master, uint16_t position)
{
	ecc_slave *slave = ecc_find_slave(master, position);
	if (slave == NULL)
	{
		slave = ecc_add_slave(master);
		if (slave != NULL)
			slave->position = position;
	}
	return slave;
}

static inline ecc_sdo *ecc_get_sdo(ecc_slave *slave, uint32_t index)
{
	ecc_sdo *sdo = ecc_find_sdo(slave, index);
	if (sdo == NULL)
	{
		sdo = ecc_add_sdo(slave);
		if (sdo != NULL)
			sdo->index = index;
	}
	return sdo;
}

static inline ecc_sdo_entry *ecc_get_sdo_entry(ecc_sdo *sdo, uint16_t index, uint8_t sub_index)
{
	ecc_sdo_entry *sdo_entry = ecc_find_sdo_entry(sdo, index, sub_index);
	if (sdo_entry == NULL)
	{
		sdo_entry = ecc_add_sdo_entry(sdo);
		if (sdo_entry != NULL)
		{
			sdo_entry->index = index;
			sdo_entry->sub_index = sub_index;
		}
	}
	return sdo_entry;
}

static inline ecc_sync *ecc_get_sync(ecc_slave *slave, uint32_t index)
{
	ecc_sync *sync = ecc_find_sync(slave, index);
	if (sync == NULL)
	{
		sync = ecc_add_sync(slave);
		if (sync != NULL)
			sync->index = index;
	}
	return sync;
}

static inline ecc_pdo *ecc_get_pdo(ecc_sync *sync, uint32_t index)
{
	ecc_pdo *pdo = ecc_find_pdo(sync, index);
	if (pdo == NULL)
	{
		pdo = ecc_add_pdo(sync);
		if (pdo != NULL)
			pdo->index = index;
	}
	return pdo;
}

static inline ecc_pdo_entry *ecc_get_pdo_entry(ecc_pdo *pdo, uint16_t index, uint8_t sub_index)
{
	ecc_pdo_entry *pdo_entry = ecc_find_pdo_entry(pdo, index, sub_index);
	if (pdo_entry == NULL)
	{
		pdo_entry = ecc_add_pdo_entry(pdo);
		if (pdo_entry != NULL)
		{
			pdo_entry->index = index;
			pdo_entry->sub_index = sub_index;
		}
	}
	return pdo_entry;
}

void ecc_init_network(ecc_network *network);
void ecc_free_network(ecc_network *network);

/*
 * Takes ids of masters of interest from options.  If `ecc_option_masters` is NULL,
 * master 0 will be assumed.  It fills `network` with the results of scanning the network.
 *
 * If `network` is already populated, it will scan the network and looks for possible
 * changes, updating `network` on the way.
 *
 * Returns number of masters that were successfully inspected.
 */
int ecc_scan_network(ecc_network *network);

/*
 * Debug function: Logs the network using LOG macro
 */
void ecc_log_network(ecc_network *network);

#endif
