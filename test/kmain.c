/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

// Linux
#include <linux/module.h>
#include <linux/err.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

// RTAI
#include <rtai_sched.h>
#include <rtai_sem.h>

#include <ecconfig.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

#define PFX "Ethercat Test: "

#define FREQUENCY 25 // task frequency in Hz
#define FREQUENCY2 1000 // task frequency in Hz
#define PROCFS_NAME "ecc_test_output"

static RT_TASK task;
static RT_TASK task2;

#define MARGIN 100
unsigned int module0_domain0[12];
unsigned int module0_domain1[12];

#define SLEEP_INTERVAL 1000
#define TIMEOUT (2000000/SLEEP_INTERVAL)

/* The fast method:
 * while (1)
 *     queue
 *     send
 *
 *     receive
 *     while !domain_received
 *         sleep
 *         receive
 *
 *     read values
 */
void run(long data)
{
	unsigned int i;
	unsigned int lastNumber = 0;
	RTIME timeMax = 0;
	RTIME timeMin = 1000000000;
	RTIME t1, t2;
	ecc_pdo_entry *entries[12];
	unsigned int domains[12];
	unsigned int entries_count = 0;

	for (i = 0; i < ecc_masters[0].domains_count; ++i)
	{
		unsigned int j;
		ecc_slave *s = &ecc_masters[0].slaves[i];

		for (j = 0; j < s->syncs_count; ++j)
		{
			unsigned int k;
			ecc_sync *y = &s->syncs[j];

			for (k = 0; k < y->pdos_count; ++k)
			{
				unsigned int l;
				ecc_pdo *p = &y->pdos[k];

				for (l = 0; l < p->entries_count; ++l)
				{
					unsigned int m;
					ecc_pdo_entry *e = &p->entries[l];

					for (m = 0; m < e->domains_count; ++m)
					{
						unsigned int n;
						ecc_domain *d = &ecc_masters[0].domains[e->domains[m].domain];

						for (n = 0; n < d->entries_count; ++n)
							if (strcmp(ecc_masters[0].entry_names[d->entries[n].entry], "Taxel") == 0)
							{
								entries[entries_count] = e;
								domains[entries_count] = m;
								++entries_count;
								break;
							}
						if (entries_count >= 12)
							goto found_all;
					}
				}
			}
		}
	}

found_all:
	while (1)
	{
		ecc_lock_master(&ecc_masters[0]);
		ecrt_domain_queue(ecc_masters[0].domains[0].domain);
		ecc_unlock_master(&ecc_masters[0]);
		ecrt_master_send(ecc_masters[0].master);

		t1 = rt_get_time_ns();

		rt_sleep(nano2count(SLEEP_INTERVAL));
		ecc_lock_master(&ecc_masters[0]);
		ecrt_master_receive(ecc_masters[0].master);
		ecc_unlock_master(&ecc_masters[0]);

		i = 0;
		while (!ecrt_domain_received(ecc_masters[0].domains[0].domain) && i++ < TIMEOUT)
		{
			rt_sleep(nano2count(SLEEP_INTERVAL));
			ecc_lock_master(&ecc_masters[0]);
			ecrt_master_receive(ecc_masters[0].master);
			ecc_unlock_master(&ecc_masters[0]);
		}
		if (i >= TIMEOUT)
			rt_printk("Domain timed out!\n");
		else
		{
			t2 = rt_get_time_ns();

			rt_printk(KERN_INFO"Read took %lldns\n", t2-t1);

			ecrt_domain_process(ecc_masters[0].domains[0].domain);
			for (i = 0; i < entries_count; ++i)
				module0_domain0[i] = EC_READ_U16(ecc_masters[0].domains[0].data + entries[i]->domains[domains[i]].offset);
			if (lastNumber-module0_domain0[0] > MARGIN && module0_domain0[0] - lastNumber > MARGIN)
			{
				rt_printk("It took %llu ns for the domains to exchange data (min: %llu, max: %llu)\n", t2-t1, timeMin, timeMax);
				rt_printk("Read value changed to %u for entry 0\n", module0_domain0[0]);
				lastNumber = module0_domain0[0];
			}
		}

		rt_task_wait_period();
	}
}

/* The standard method:
 * while (1)
 *     receive
 *     process
 *
 *     read values
 *
 *     queue
 *     send
 */
void run2(long data)
{
	unsigned int i;
	ecc_pdo_entry *entries[8];
	unsigned int domains[8];
	unsigned int entries_count = 0;

	for (i = 0; i < ecc_masters[0].domains_count; ++i)
	{
		unsigned int j;
		ecc_slave *s = &ecc_masters[0].slaves[i];

		for (j = 0; j < s->syncs_count; ++j)
		{
			unsigned int k;
			ecc_sync *y = &s->syncs[j];

			for (k = 0; k < y->pdos_count; ++k)
			{
				unsigned int l;
				ecc_pdo *p = &y->pdos[k];

				for (l = 0; l < p->entries_count; ++l)
				{
					unsigned int m;
					ecc_pdo_entry *e = &p->entries[l];

					for (m = 0; m < e->domains_count; ++m)
					{
						unsigned int n;
						ecc_domain *d = &ecc_masters[0].domains[e->domains[m].domain];

						for (n = 0; n < d->entries_count; ++n)
							if (strcmp(ecc_masters[0].entry_names[d->entries[n].entry], "Taxel") == 0)
							{
								entries[entries_count] = e;
								domains[entries_count] = m;
								++entries_count;
								break;
							}
						if (entries_count >= 8)
							goto found_all;
					}
				}
			}
		}
	}

found_all:
	while (1)
	{
		// receive process data
		ecc_lock_master(&ecc_masters[0]);
		ecrt_master_receive(ecc_masters[0].master);
		ecrt_domain_process(ecc_masters[0].domains[1].domain);
		ecc_unlock_master(&ecc_masters[0]);

		for (i = 0; i < entries_count; ++i)
			module0_domain1[i] = EC_READ_U16(ecc_masters[0].domains[1].data + entries[i]->domains[domains[i]].offset);

		ecc_lock_master(&ecc_masters[0]);
		ecrt_domain_queue(ecc_masters[0].domains[1].domain);
		ecc_unlock_master(&ecc_masters[0]);
		ecrt_master_send(ecc_masters[0].master);

		rt_task_wait_period();
	}
}

static int _provide_data(char *page, char **start, off_t offset, int count, int *eof, void *data)
{
	int writtenBytes;
	sprintf(page+offset, "%u %u %u %u %u %u %u %u %u %u %u %u\n"
			"%u %u %u %u %u %u %u %u%n",
			module0_domain0[0], module0_domain0[1], module0_domain0[2], module0_domain0[3],
			module0_domain0[4], module0_domain0[5], module0_domain0[6], module0_domain0[7],
			module0_domain0[8], module0_domain0[9], module0_domain0[10], module0_domain0[11],
			module0_domain1[0], module0_domain1[1], module0_domain1[2], module0_domain1[3],
			module0_domain1[4], module0_domain1[5], module0_domain1[6], module0_domain1[7],
			&writtenBytes);
	return writtenBytes;
}

int init_module(void)
{
	struct proc_dir_entry *procEntry;
	printk(KERN_INFO PFX "Starting...\n");
	procEntry = create_proc_entry(PROCFS_NAME, 0200, '\0');
	if (procEntry == '\0')
	{
		printk(KERN_INFO PFX "Could not create /proc entry!");
		remove_proc_entry(PROCFS_NAME, '\0');
		return -500;
	}
	procEntry->read_proc = _provide_data;
	procEntry->mode = S_IFREG | S_IRUGO;
	procEntry->uid = 0;
	procEntry->gid = 0;
	procEntry->size = 0;
	printk(KERN_INFO PFX "Starting cyclic sample thread...\n");
	rt_set_oneshot_mode();
	start_rt_timer(nano2count(100000));
	if (rt_task_init(&task, run, 0, 2000, 0, 1, NULL))
	{
		printk(KERN_ERR PFX "Failed to init RTAI task!\n");
		remove_proc_entry(PROCFS_NAME, '\0');
		return -1000;
	}
	if (rt_task_make_periodic(&task, rt_get_time() + 1000000, nano2count(1000000000/FREQUENCY)))
	{
		printk(KERN_ERR PFX "Failed to run RTAI task!\n");
		rt_task_delete(&task);
		remove_proc_entry(PROCFS_NAME, '\0');
		return -1500;
	}
/*	if (rt_task_init(&task2, run2, 0, 2000, 0, 1, NULL))
	{
		printk(KERN_ERR PFX "Failed to init RTAI task2!\n");
		remove_proc_entry(PROCFS_NAME, '\0');
		rt_task_delete(&task);
		return -2000;
	}
	if (rt_task_make_periodic(&task2, rt_get_time() + 1000000, nano2count(1000000000/FREQUENCY2)))
	{
		printk(KERN_ERR PFX "Failed to run RTAI task!\n");
		rt_task_delete(&task2);
		rt_task_delete(&task);
		remove_proc_entry(PROCFS_NAME, '\0');
		return -2500;
	}*/
	printk(KERN_INFO PFX "Starting...done.\n");
	return 0;
}

void cleanup_module(void)
{
	printk(KERN_INFO PFX "Stopping...\n");
	stop_rt_timer();
	rt_task_delete(&task);
//	rt_task_delete(&task2);
	remove_proc_entry(PROCFS_NAME, '\0');
	printk(KERN_INFO PFX "Stopping...done.\n");
}
