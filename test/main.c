/*
 * Copyright (C) 2011-2013  Shahbaz Youssefi <ShabbyX@gmail.com>
 *
 * The research leading to these results has received funding from
 * the European Commission's Seventh Framework Programme (FP7) under
 * Grant Agreement n. 231500 (ROBOSKIN).
 *
 * This file is part of the EtherCAT Configuration Module.
 *
 * The EtherCAT Configuration Module is free software: you can redistribute it
 * and/or modify it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * The EtherCAT Configuration Module is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * the EtherCAT Configuration Module.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unistd.h>
#include <stdio.h>

int last_value = 0;
int last_value2 = 0;

int main()
{
	unsigned int seq = 0;
	int i;

	while (1)
	{
		FILE *fin;
		unsigned int taxel_value;
		unsigned int taxel_value2;

/*		printf("%u: ", seq);*/

		fin = fopen("/proc/ecc_test_output", "r");
		if (fin == NULL)
		{
			printf("The test module must first be loaded\n");
			return 0;
		}

/*		for (i = 0; i < 12; ++i)
		{
			fscanf(fin, "%u", &taxel_value);
			printf("%u", taxel_value);
		}
		printf("\n");*/

		fscanf(fin, "%u", &taxel_value);
		printf("%u", taxel_value);

		for (i = 0; i < 11/*12+2*/; ++i)
		{
			fscanf(fin, "%u", &taxel_value2);
			printf(" %u", taxel_value2);
		}
		printf("\n");

		if (last_value > taxel_value + 500 || last_value < taxel_value - 500
			|| last_value2 > taxel_value2 + 10 || last_value2 < taxel_value2 - 10)
			printf("%u: %u %u\n", seq++, taxel_value, taxel_value2);
		last_value = taxel_value;
		last_value2 = taxel_value2;

		fclose(fin);
		usleep(5000);
	}
	return 0;
}
