\section{Developing User Modules}\label{DevelopingUserModules}\label{DevelopingUserModules Overview}
In this chapter, the process of developing Linux kernel modules using the EtherCAT configuration module is
explained. Beforehand, the terminology of EtherCAT needs to be defined.

\begin{itemize}
\item \textbf{Entry} - An entry is a single element in the network which provides a value (an \textbf{input} entry) or captures a
 value (an output entry).


\item \textbf{PDO} - (Process Data Object) - A physically grouped set of entries managed by a microcontroller. \% TODO: Check with Emma


\item \textbf{Slave} - A slave is a node in the EtherCAT network with certain PDOs connected to it. These PDOs could manage any
 different types of entries.


\item \textbf{Domain} - A domain is a set of entries logically grouped together. Data from each domain is gathered
 individually, possible at different rates.
\end{itemize}

To develop a kernel module working with EtherCAT, you first need to connect to the configuration module. Then through the provided
variables, you can have access to EtherCAT. Section \ulhyperref{DevelopingUserModules ConnectionwithConfigurationModule}{Connection with Configuration Module} deals with connection with the configuration module
while section \ulhyperref{DevelopingUserModules UsingProvidedData}{Using Provided Data} explains how it is used.

\subsection{Connection with Configuration Module}\label{DevelopingUserModules ConnectionwithConfigurationModule}
The connection to the configuration module is simple. There is a header file named \code{ecconfig.h} in the configuration
kernel module's folder that you need to include in your module. This file includes both \code{ecrt.h} and \code{rtai\_sem.h} and
thus, as explained in the \ulhyperref{index BuildandInstallation}{previous sections}, need to be present and accessible in your module. This file
introduces the following structures.

\begin{itemize}
\item \code{ecc\_domain}


\item \code{ecc\_slave}


\item \code{ecc\_entry\_name\_to\_type}
\end{itemize}

Additionally, it introduces the following variables and functions \code{export}ed by the configuration module.

\begin{itemize}
\item \code{ecc\_ready: unsigned char}


\item \code{ecc\_master: ec\_master\_t *}


\item \code{ecc\_domains: ecc\_domain *}


\item \code{ecc\_domains\_count: unsigned int}


\item \code{ecc\_slaves: ecc\_slave *}


\item \code{ecc\_slaves\_count: unsigned int}


\item \code{ecc\_entry\_name\_map: ecc\_entry\_name\_to\_type *}


\item \code{ecc\_entry\_name\_map\_count: unsigned int}


\item \code{ecc\_lock\_master(): void}


\item \code{ecc\_unlock\_master(): void}
\end{itemize}

For detailed explanation of each structure or variable, please refer to the developer documentation.

\subsection{Using Provided Data}\label{DevelopingUserModules UsingProvidedData}
As already mentioned, you need to include the header file \code{ecconfig.h} in yout kernel module source code and the
variables and functions will then be available to you. Short description of the variables/functions are as follows:

\begin{itemize}
\item \code{ecc\_ready}: When 0, the other variables must not be accessed. When non-zero, it is guaranteed that other the variables hold correct data.


\item \code{ecc\_master}: This variable is used to communicate with the master. See the part on \ulhyperref{DevelopingUserModules Sending/ReceivingData}{Sending/Receiving Data} for usage.


\item \code{ecc\_domains}: This array contains information on the domains of the network, including entries in each domain, their
 type-codes, and the domain process data. See the part on \ulhyperref{DevelopingUserModules Sending/ReceivingData}{Sending/Receiving Data} and \ulhyperref{DevelopingUserModules AccessingEntriesenMasse}{Accessing Entries en Masse} for usage.


\item \code{ecc\_domains\_count}: The size of \code{ecc\_domains} array.


\item \code{ecc\_slaves}: This array contains hierarchical information on the slaves in the network. See the part on \ulhyperref{DevelopingUserModules Slave/PDO/EntryStructure}{Slave/PDO/Entry Structure} for usage.


\item \code{ecc\_slaves\_count}: The size of \code{ecc\_slaves} array.


\item \code{ecc\_entry\_name\_map}: A map that converts type names into codes. See the part on \ulhyperref{DevelopingUserModules IdentifyingEntriesofInterest}{Identifying Entries of Interest} for usage.


\item \code{ecc\_entry\_name\_map\_count}: The size of \code{ecc\_entry\_name\_map} array.


\item \code{ecc\_lock\_master}: An internal mutex is locked for making access to the master atomic. See the part on \ulhyperref{DevelopingUserModules Sending/ReceivingData}{Sending/Receiving Data} for usage.


\item \code{ecc\_unlock\_master}: Unlocks the mutex locked by \code{ecc\_lock\_master}
\end{itemize}

For detailed explanation of these variables, please refer to the developer documentation, on the section about exported variables.

\subsubsection{Sending/Receiving Data}\label{DevelopingUserModules Sending/ReceivingData}
Sending and receiving from the network is simple. Apart from accessing the domain process data discussed in the next section, the
sending and receiving is done as follows.

\paragraph{Sending}\label{DevelopingUserModules Sending}
\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{ecc\_lock\_master();}&
&\unimportantCode{for\ d\ in\ [0\ ecc\_domains\_count)}&
&\unimportantCode{\ \ \ \ ecrt\_domain\_queue(ecc\_domains[d].domain);}&
&\unimportantCode{ecc\_unlock\_master();}&
&\unimportantCode{ecrt\_master\_send(ecc\_master);}&
\end{lstlisting}
\end{codeblockframe}

\paragraph{Receiving}\label{DevelopingUserModules Receiving}
\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{ecc\_lock\_master();}&
&\unimportantCode{ecrt\_master\_receive(ecc\_master);}&
&\unimportantCode{for\ d\ in\ [0\ ecc\_domains\_count)}&
&\unimportantCode{\ \ \ \ ecrt\_domain\_process(ecc\_domains[d].domain);}&
&\unimportantCode{ecc\_unlock\_master();}&
\end{lstlisting}
\end{codeblockframe}

\subsubsection{Accessing Entries en Masse}\label{DevelopingUserModules AccessingEntriesenMasse}
In this section, accessing the entries are discussed. In the simplest case, all entries of the network may need to be inspected.
Assume in domain \code{d}, entry \code{e} needs to be read or written. To do the following, you first need to find out the offset
of the process data related to that entry in that domain. This is computable by

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ *ecc\_domains[d].entries[e].offset}&
\end{lstlisting}
\end{codeblockframe}

This is an index to

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_domains[d].process\_data}&
\end{lstlisting}
\end{codeblockframe}

Reading from and writing to the domain process data in IgH EtherCAT master is done through the defined \code{EC\_READ\_*} and \code{EC\_WRITE\_*} macros.
For detailed explanation of those macros, please refer to the IgH EtherCAT master documentation. As an example, if a 16-bit unsigned integer
needs to be read from the domain process data, the following code could be used.

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ entry\_data\ =\ EC\_READ\_U16(\&ecc\_domains[d].process\_data[}&
&\unimportantCode{\ \ \ \ \ \ \ \ \ \ \ \ \ *ecc\_domains[d].entries[e].offset]);}&
\end{lstlisting}
\end{codeblockframe}

\subsubsection{Slave/PDO/Entry Structure}\label{DevelopingUserModules Slave/PDO/EntryStructure}
In case the entries of the network need to be inspected hierarchically, it can be done through \code{ecc\_slaves}. The syncs, currently
four, group PDOs as input/output in synchronous/asynchronous (TODO: Check with Emma) modes. In slave \code{s}, the PDOs of sync
\code{y} are

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_slaves[s].syncs[y].pdos[p]:}&
&\unimportantCode{\ \ \ \ \ \ p\ in\ [0\ ecc\_slaves[s].syncs[y].n\_pdos)}&
\end{lstlisting}
\end{codeblockframe}

\code{ecc\_slaves[s].syncs[y].pdos} is a part of \code{ecc\_slaves[s].pdos} array. There may be need for the user to know the indices of
these pdos of a sync in \code{ecc\_slaves[s].pdos} directly, rather than local access through \code{ecc\_slaves[s].syncs[y].pdos}. This can
be done with the \code{pdo\_indices\_of\_syncs} variable in \code{ecc\_slaves[s]}. That is, the PDOs of syncs \code{y} could be accessed
also with

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_slaves[s].pdos[p]:}&
&\unimportantCode{\ \ \ \ \ \ p\ in\ [start\_index\ start\_index+ecc\_slaves[s].syncs[y].n\_pdos)}&
&\unimportantCode{\ \ \ \ \ \ and\ start\_index\ =\ ecc\_slaves[s].pdo\_indices\_of\_syncs[y]}&
\end{lstlisting}
\end{codeblockframe}

Similarly, the entries of PDOs can be inspected either from the PDO itself (through the sync or not), or from the entries array with
the help of \code{entry\_indices\_of\_pdos}. In slave \code{s}, entries of PDO \code{p} local to sync \code{y} are

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_slaves[s].syncs[y].pdos[p].entries[e]:}&
&\unimportantCode{\ \ \ \ \ \ e\ in\ [0\ ecc\_slaves[s].syncs[y].pdos[p].n\_entries)}&
\end{lstlisting}
\end{codeblockframe}

If \code{p} is global to the \code{ecc\_slaves[s].pdos}, then this can be written as

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_slaves[s].pdos[p].entries[e]:}&
&\unimportantCode{\ \ \ \ \ \ e\ in\ [0\ ecc\_slaves[s].pdos[p].n\_entries)}&
\end{lstlisting}
\end{codeblockframe}

And if the \code{ecc\_slaves[s].entries} needs to be directly accessed, these entries would be

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_slaves[s].entries[e]:}&
&\unimportantCode{\ \ \ \ \ \ e\ in\ [start\_index\ start\_index+ecc\_slaves[s].pdos[p].n\_entries)}&
&\unimportantCode{\ \ \ \ \ \ and\ start\_index\ =\ ecc\_slaves[s].entry\_indices\_of\_pdos[p]}&
\end{lstlisting}
\end{codeblockframe}

The later is specifically important to identify the type of entry. See the part on \ulhyperref{DevelopingUserModules IdentifyingEntriesofInterest}{Identifying Entries of Interest} for more details.

\subsubsection{Identifying Entries of Interest}\label{DevelopingUserModules IdentifyingEntriesofInterest}
Let's say in slave \code{s}, sync \code{y}, PDO \code{p} in the sync, entry \code{e} in that PDO is being inspected. In that
case, as already shown, the global index of the entry in \code{ecc\_slaves[s].entries} can be computed as follows

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ p\_global\ =\ ecc\_slaves[s].pdo\_indices\_of\_syncs[y];}&
&\unimportantCode{\ \ e\_global\ =\ ecc\_slaves[s].entry\_indices\_of\_pdos[p\_global];}&
\end{lstlisting}
\end{codeblockframe}

Using the \code{position\_in\_domains} array in \code{ecc\_slaves[s]}, this entry can be identified in \code{ecc\_domains}. This entry is

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_domains[pos.domain].entries[pos.entry\_index]}&
&\unimportantCode{\ \ with\ pos\ =\ ecc\_slaves[s].position\_in\_domains[e\_global]}&
\end{lstlisting}
\end{codeblockframe}

and similarly, the type of the entry can be found with

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ecc\_domains[pos.domain].entry\_types[pos.entry\_index]}&
&\unimportantCode{\ \ with\ pos\ =\ ecc\_slaves[s].position\_in\_domains[e\_global]}&
\end{lstlisting}
\end{codeblockframe}

To identify entries of interest, this code must be checked against the type-code of intereted types. To identify this type, prior
knowledge of the network is needed. To simplify this matter and reduce the complexity of this prior knowledge, the entry names are
used as a base. See \ulhyperref{index Execution}{Execution} section, where the input XML file of the feeder is discussed for an explanation of how
entry names are used to group them as a type. In short, a group name groups all taxels that have the group name as a substring of
their name.

Exploiting this idea, the \code{ecc\_entry\_name\_map} contains group names and assigned codes that in your kernel module initialization,
it is possible to search this array for the group name of interest and extract its assigned code.

See the examples in the developer documentation for more details.

\textcolor[rgb]{0.5, 0.5, 0.5}{\footnotesize{\textit{Generated by} \textbf{DocThis! Documentation Generator} \textit{program.}}}
