\phantomsection
\label{index}\label{index Overview}
\section{Introduction}\label{index Introduction}
This article contains documentation on how the EtherCAT configuration program works, both from a user's perspective and developer's.
The configuration program is divided in two parts: a Linux kernel module and a user-space program. This article first provides users
with explanation on how to compile the programs, what prerequisites exist, the input files and finally how to configure the EtherCAT
network. To see how a developer writing a kernel module which interacts with the EtherCAT network can use the configuration
module for her purpose, or getting into details of the process of configuration and the roles of the kernel module and the
user-space program, i.e. details of implementation of these two programs, please refer to the developer documentation of the
configuration module.

The research leading to these results has received funding from the European Commission's Seventh Framework Programme (FP7) under
Grant Agreement n. 231500 (ROBOSKIN). This program has been primarily written by Shahbaz Youssefi based on an initial work by
Nguyen Quoc Nam with the help of members of MacLab, DIST, University of Genova, especially Emanuele Baglini.

The following two chapters, \ulhyperref{index BuildandInstallation}{Build and Installation} and \ulhyperref{index Execution}{Execution} will explain the details of the build, install and execution
of the EtherCAT configuration module.

\section{Build and Installation}\label{index BuildandInstallation}
We will first go through the process of building the configuration software and running it. In \ulhyperref{index Prerequisites}{Prerequisites} section,
the prerequisites are introduced. These are extra libraries that need to be present for the EtherCAT configuration to build.
In \ulhyperref{index Building}{Building} section, the process of building both the kernel module and the user-space program are explained. In \ulhyperref{index Installation}{Installation} section,
the process of installing the software is explained.

\subsection{Prerequisites}\label{index Prerequisites}
First and foremost, the EtherCAT configuration module is based on two libraries crucial for its execution; \ulhref{http://www.rtai.org}{RTAI}
and the IgH EtherCAT master present in \ulhref{http://www.etherlab.org/}{EtherLab}. RTAI patches the Linux kernel and together with provided
modules allows implementation of applications that have real-time constraints. EtherCAT, as a network designed for real-time applications
is best used with real-time applications. Installing RTAI however is not a trivial task and is very system dependent. Please refer to the
RTAI website for more details. Etherlab plays an equally important role as it provides driver for ethernet devices, allowing them to act as
EtherCAT masters, along with API both in kernel and user spaces to use the device. For installation of the IgH EtherCAT master, please refer
to its website.

Additionally, the user-space program uses the \ulhref{http://www.minixml.org/}{Mini-XML} library to read from its input XML file. To
install Mini-XML, either follow the instructions on its website, or simply execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ apt-get\ install\ libmxml-dev}&
\end{lstlisting}
\end{codeblockframe}

to automatically download and install the library.

Finally, if you have received the source for these documentation files, you can build them to HTML and LaTeX using
\ulhref{https://gitlab.com/shabbyX/DocThis}{DocThis!} documentation generator. You would also need \code{pdflatex} and \code{inkscape} installed to
convert the LaTeX output to PDF.

\subsection{Building}\label{index Building}
To build each part, the Makefiles are already written and are existent in their respective folders. The following subsection
describes how to configure the software and the two subsections that follow are devoted to the two parts of the configuration module.

To build all parts, including a simple test program, simply execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make}&
\end{lstlisting}
\end{codeblockframe}

in the top directory of the program.

\subsubsection{Configuration}\label{index Configuration}
The configuration of the program is simple. In the root directory of the EtherCAT configuration module, a file named \code{Makefile.config}
can be found. In this file, three variables are defined, namely \code{RTAI\_CONFIG\_TOOL}, \code{ETHERCAT\_HOME} and \code{ETHERCAT\_SOURCE}. The first one
defines the RTAI configuration tool. If the path to RTAI's binary files are in \code{PATH}, then \code{rtai-config} should suffice. Otherwise,
this variable needs to be properly set. The second variable defines the path to where EtherCAT has been installed and the third variable
defines the path to where the EtherCAT source is. Note that the EtherCAT source must contain the already-built EtherCAT master and not a
clean source. Most specifically, the file \code{Module.symvers} of the EtherCAT master must exist in the specified directory.

\code{ETHERCAT\_HOME} is defined as:

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ETHERCAT\_HOME\ :=\ /opt/etherlab}&
\end{lstlisting}
\end{codeblockframe}

which is the default installation path of EtherLab. \code{ETHERCAT\_SOURCE} however depends on where you have placed the source and therefore,
most probably needs to be updated. Simply change the line defining \code{ETHERCAT\_SOURCE} to:

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ ETHERCAT\_SOURCE\ :=\ /path/to/ethercat/source}&
\end{lstlisting}
\end{codeblockframe}

Note that you must use absolute paths and not relative paths!

\subsubsection{Kernel Module}\label{index KernelModule}
The kernel module of the configuration software is compiled with Linux kernel's already provided Makefile for modules (in newer Linux
kernel versions where KBuild exists). Note that there should not be any whitespaces in the path to where the kernel module is located.

To build the module only the module, execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make\ module}&
\end{lstlisting}
\end{codeblockframe}

in the top directory of the program or

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make}&
\end{lstlisting}
\end{codeblockframe}

in the kernel module's respective directory. This creates a module with name \code{ethercat-config.ko}.

\subsubsection{User-space Program}\label{index User-spaceProgram}
The user-space program, also called the \textit{feeder program}, or simply \textit{feeder}, is used to build the EtherCAT configuration data and feed
it to the configuration kernel module. The communication protocol between the feeder program and the kernel module are explained in details
in the developer documentation so that the feeder program could be rewritten by administrators that need a specific configuration
program rather than the provided generic one.

If you have built but not installed Mini-XML header and library files in the default paths of the compiler, you need to add
\code{-I/path/to/mxml/headers} to the \code{OPTIONS} variable and \code{-L/path/to/mxml/libfile} to the beginning of \code{LINK} variable in the \code{Makefile}.

To build only the feeder, execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make\ feeder}&
\end{lstlisting}
\end{codeblockframe}

in the top directory of the program or

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make}&
\end{lstlisting}
\end{codeblockframe}

in the feeder's respective directory. This creates an executable named \code{configure\_ethercat}.

\subsubsection{Documentation}\label{index Documentation}
To build the documentation of the EtherCAT configuration module, you need the DocThis! documentation generator. This program is not yet officially
released (although included in the Skin Middle-ware), and therefore you need to either install it from the Skin Middle-ware or obtain the latest
version from \ulhref{https://gitlab.com/ShabbyX/DocThis}{gitlab}. Either way, the source files of the documentation are in a syntax very similar to
\ulhref{http://daringfireball.net/projects/markdown/}{markdown} and can be easily read.

If you do have DocThis!, simply set \code{BUILD\_DOCUMENTATION} to \code{y} in \code{Makefile.config}. Then execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make\ doc}&
\end{lstlisting}
\end{codeblockframe}

in the top directory of the program or

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make}&
\end{lstlisting}
\end{codeblockframe}

in the documentation folder. In the top directory, when \code{make} is called, the documentation will also be built if \code{BUILD\_DOCUMENTATION} is set.

The documentation will be generated in HTML and LaTeX, the later of which would in turn be compiled to PDF using \code{pdflatex}.

\subsection{Installation}\label{index Installation}
To install the EtherCAT configuration module, you need to specify an installation directory. The default directory is \code{/opt/ethercat-config}
which can be changed in \code{Makefile.config} file where \code{INSTALL\_DIRECTORY} variable is defined.

After the installation directory is set, simply execute the following command (as root, if installation directory needs root access)

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make\ install}&
\end{lstlisting}
\end{codeblockframe}

To uninstall, similarly execute (again as root, if installation directory needs root access)

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ make\ uninstall}&
\end{lstlisting}
\end{codeblockframe}

After installation, the following more interesting files are available among some others:

\begin{itemize}
\item \code{<install-path>/modules/ethercat\_config.ko}: The EtherCAT configuration kernel module.


\item \code{<install-path>/bin/configure\_ethercat}: The EtherCAT configuration feeder program (see documentation on \textit{api} for more information on the feeder).
There is also a link created to the feeder in \code{/usr/bin} which is hopefully in your \code{PATH} and therefore the program can be accessed anywhere.


\item \code{<install-path>/include/ecconfig.h}: The header file to be included in kernel modules using the EtherCAT configuration module.


\item \code{<install-path>/doc}: Both API documentation and user-manual will be installed in this path. If the documentation is not generated, only the
markdown files will be available.
\end{itemize}

\section{Execution}\label{index Execution}
The execution of the configuration module consists of two parts. First, you need to insert the configuration module. To do this, execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ insmod\ <install-path>/modules/ethercat-config.ko\ [ecc\_master\_id=id]}&
\end{lstlisting}
\end{codeblockframe}

in the kernel-module directory. \code{ecc\_master\_id} is the id of the master to be configured, which defaults to 0 if not provided. Note that you need \code{root}
previlages to execute this command. Second you need to run the feeder program, which also needs \code{root} previlages. To do this, execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ configure\_ethercat}&
\end{lstlisting}
\end{codeblockframe}

in the user-program directory. To see the complete usage of the feeder program use the \code{--help} or \code{-h} option, that is execute

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ \$\ configure\_ethercat\ --help}&
\end{lstlisting}
\end{codeblockframe}

The usage of this program is in short

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ configure\_ethercat\ [--master\ master\_id]\ [--codes\ entry\_codes\_file]\ [config\_file]}&
\end{lstlisting}
\end{codeblockframe}

where \code{config\_file} is the input XML file. \code{master\_id} identifies the id of the master being configured. If \code{--master}
is not specified, master with id 0 will be configured, i.e. device \code{/dev/EtherCAT0}. If the \code{config\_file} is not
specified, file \code{config.xml} will be used. The program can output the entry codes assigned to entry names and write them to
\code{entry\_codes\_file} if the \code{--codes} option is specified.

\subsection{Feeder Input}\label{index FeederInput}
Before inspecting the format of the input XML file, parts of the terminology used with EtherCAT will be reviewed as follows.

\begin{itemize}
\item \textbf{Entry} - An entry is a single element in the network which provides a value (an \textbf{input} entry) or captures a value
  (an output entry).


\item \textbf{Slave} - A slave is a node in the EtherCAT network with certain entries connected to it. These entries could be of
 different types are not necessarily uniform.


\item \textbf{Domain} - A domain is a set of entries logically grouped together. Data from each domain is gathered
 individually, possible at different rates.
\end{itemize}

The input XML file must contain a tag named \code{ethercat}. In that block, the blocks with tags \code{domain} are inspected.
In each domain there can be many \code{slave} tags, within each may exist many entries identified by \code{input} and
\code{output} tags. When scanning the network, it is already known which entry is input and which are output and therefore the
\code{input} and \code{output} tags are used solely for error-checking. The slaves could be present in many domains, but each
entry in each slave can be present in only one domain. The entries are identified by names (the values inside the \code{input}
and \code{output} blocks). However, each name represents not just one entry, but a group of entries that have the specified name
as a substring of their name. For example, if there are sensors in the network with names \code{Sensor 01}, \code{Sensor 02},
..., they can be identified by the block \code{<input>Sensor</input>}.

Here is an example of an input XML file to configure a network with two slaves. Note that at each block level, other tags may exist
and would be ignored by the feeder program. In the HTML documentation, this input file is explained more in details.

\begin{codeblockframe}
\begin{lstlisting}
&\unimportantCode{\ \ <?xml\_version="1.0"\ encondig="UTF-8"?>}&
&\unimportantCode{\ \ <network>}&
&\unimportantCode{\ \ \ \ <ethercat>}&
&\unimportantCode{\ \ \ \ \ \ <domain>}&
&\unimportantCode{\ \ \ \ \ \ \ \ <slave\ vendor\_id="0x0000061d"\ product\_code="0x00000001">}&
&\unimportantCode{\ \ \ \ <input>Position\ Feedback</input>}&
&\unimportantCode{\ \ \ \ <input>Current\ Feedback</input>}&
&\unimportantCode{\ \ \ \ <output>ABS\ Position</output>}&
&\unimportantCode{\ \ \ \ <output>REL\ Position</output>}&
&\unimportantCode{\ \ \ \ <output>Velocity</output>}&
&\unimportantCode{\ \ \ \ \ \ \ \ </slave>}&
&\unimportantCode{\ \ \ \ \ \ \ \ <slave\ vendor\_id="0x12345678"\ product\_code="0x01010101">}&
&\unimportantCode{\ \ \ \ <input>Switch</input>}&
&\unimportantCode{\ \ \ \ <output>LED</output>}&
&\unimportantCode{\ \ \ \ \ \ \ \ </slave>}&
&\unimportantCode{\ \ \ \ \ \ </domain>}&
&\unimportantCode{\ \ \ \ \ \ <domain>}&
&\unimportantCode{\ \ \ \ \ \ \ \ <slave\ vendor\_id="0x0000061d"\ product\_code="0x00000001">}&
&\unimportantCode{\ \ \ \ <input>Taxel</input>}&
&\unimportantCode{\ \ \ \ \ \ \ \ </slave>}&
&\unimportantCode{\ \ \ \ \ \ </domain>}&
&\unimportantCode{\ \ \ \ </ethercat>}&
&\unimportantCode{\ \ </network>}&
\end{lstlisting}
\end{codeblockframe}

\textcolor[rgb]{0.5, 0.5, 0.5}{\footnotesize{\textit{Generated by} \textbf{DocThis! Documentation Generator} \textit{program.}}}
