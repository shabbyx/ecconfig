\phantomsection
\label{index}\label{index Overview}
\section{Introduction}\label{index Introduction}
The EtherCAT configuration module consists of two parts; a kernel space module and user space program. There are two
libraries that need to be present for the module to work, namely \ulhref{http://www.etherlab.org}{IgH EtherCAT master} and
\ulhref{https://www.rtai.org}{RTAI}. It is assumed that the reader of this documentation has basic knowledge of these libraries,
especially IgH EtherCAT master, and is able to look up variable types and constants used from those libraries in their respective
documentations.

\begin{itemize}
\item In kernel space, a module is responsible for establishing connection with EtherCAT and performing the configuration.
 The configuration data however are more trivialy sorted and customized in user-space and thus, this kernel module awaits
 configuration data on a \code{/proc} file. After receiving the configuration data, the kernel module configures and
 activates the EtherCAT master. During this process, certain data structures are built and exported so that other kernel
 modules that need to work with EtherCAT could use them.

\item In user space, a program is implemented that reads from an \ulhyperref{DomainsInputFile}{XML input} file defining domains
 on network \textit{entries}. Afterwards, the program scans the EtherCAT network, identifying \textit{slaves}, \textit{PDOs} and
 entries and generates configuration data. Finally, it sends the configuration by a certain \ulhyperref{Feeder}{protocol}
 to the kernel module via the \code{/proc} file. This program henceworth will be called the \textit{feeder program}.
\end{itemize}

First the kernel module neededs to be inserted in the kernel. Then the user space program runs and feeds it. If everything
was ok, the exported variables should be usable as soon as the kernel module \ulhyperref{Exported eccUNDERSCOREready}{finishes} configuration.

\textbf{Note:} The structures and variables introduced in this documentation all refer to what is defined in
\code{ethercat\_configure.h} located in the folder of the configuration kernel module and which needs to be included in
your kernel module. To see the format of the domains XML input file, see \ulhyperref{DomainsInputFile}{here} and to see the protocol of
communication between the feeder program and the kernel module, please see \ulhyperref{Feeder}{this} page.

For documentation on installation and execution please refer to the PDF documentation.

\subsection{List of Structures}\label{index ListofStructures}
\subsubsection{\code{\texorpdfstring{\ulhyperref{eccUNDERSCOREdomain}{ecc\_domain}}{ecc\_domain}}}\label{index eccUNDERSCOREdomain}
\leftskip 20pt This structure contains information on one domain. These information include EtherCAT master's variable identifying the domain, as
 well as list of entries in the domain, their types and their I/O memory. Through a list of this structure, named
 \code{\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}}, data of the domain could be sent/received and entries of each domain accessed
 as a whole.

\leftskip 0pt
\subsubsection{\code{\texorpdfstring{\ulhyperref{eccUNDERSCOREslave}{ecc\_slave}}{ecc\_slave}}}\label{index eccUNDERSCOREslave}
\leftskip 20pt This structure contains information on one slave. These information include a list of entries, PDOs and syncs as well as a link
 to \code{\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}} for every entry of the slave. Through a list of this structure, named
 \code{\ulhyperref{Exported eccUNDERSCOREslaves}{ecc\_slaves}}, the EtherCAT network could be examined hierarchically.

\leftskip 0pt
\subsubsection{\code{\texorpdfstring{\ulhyperref{eccUNDERSCOREentryUNDERSCOREnameUNDERSCOREtoUNDERSCOREtype}{ecc\_entry\_name\_to\_type}}{ecc\_entry\_name\_to\_type}}}\label{index eccUNDERSCOREentryUNDERSCOREnameUNDERSCOREtoUNDERSCOREtype}
\leftskip 20pt This structure is a map from entry names, as defined in the \ulhyperref{DomainsInputFile}{XML input}, to type ids assigned by the
 feeder program. Through a list of this structure, each kernel module can search for, and identify in
 \code{\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}} the entries of interest for its purpose.

\leftskip 0pt
\subsection{Globals}\label{index Globals}
\subsubsection{ \texorpdfstring{\ulhyperref{Exported}{Exported Variables}}{Exported Variables}}\label{index ExportedVariables}
\leftskip 20pt The kernel module exports variables for use of other kernel modules using the EtherCAT network. These variables include a reference
 to the master, master access mutex and lists of \ulhyperref{index eccUNDERSCOREdomain}{domain data}, \ulhyperref{index eccUNDERSCOREslave}{slave data} and \ulhyperref{index eccUNDERSCOREentryUNDERSCOREnameUNDERSCOREtoUNDERSCOREtype}{entry name mapping}.

\leftskip 0pt
\subsection{Input}\label{index Input}
\subsubsection{ \texorpdfstring{\ulhyperref{DomainsInputFile}{Domains Input File}}{Domains Input File}}\label{index DomainsInputFile}
\leftskip 20pt This file is an input to the feeder program which contains an XML definition of domains and how the PDO entries of the network are
 assigned to them.

\leftskip 0pt
\subsection{List of Examples}\label{index ListofExamples}
\subsubsection{ \texorpdfstring{\ulhyperref{SimpleRead}{Simple Read}}{Simple Read}}\label{index SimpleRead}
\leftskip 20pt This example shows a simple thread that reads from the network with one domain.

\leftskip 0pt
\subsubsection{ \texorpdfstring{\ulhyperref{MultipleDomains}{Multiple Domains}}{Multiple Domains}}\label{index MultipleDomains}
\leftskip 20pt This example is similar to the previous one, except there are more than 1 domains in this example.

\leftskip 0pt
\subsubsection{ \texorpdfstring{\ulhyperref{FindingEntriesofInterest}{Finding Entries of Interest}}{Finding Entries of Interest}}\label{index FindingEntriesofInterest}
\leftskip 20pt In this example, a list of entries are inspected and entries of interest are identified. Additionally, a sample thread that reads
 from these values is presented.

\leftskip 0pt
\subsubsection{ \texorpdfstring{\ulhyperref{FastRead}{Fast Read}}{Fast Read}}\label{index FastRead}
\leftskip 20pt In this example, a new method of reading from the network is presented. It requires a feature that is now under test and would possibly
 be added to the master in the future. With this method, the data are read from the network exactly when they arrive which has smaller
 delay in comparison with the the standard methods were the data being processed are those requested by in the previous cycle.

\leftskip 0pt
\textcolor[rgb]{0.5, 0.5, 0.5}{\footnotesize{\textit{Generated by} \textbf{DocThis! Documentation Generator} \textit{program.}}}
