\section{Feeder}\label{Feeder}\label{Feeder Overview}
The feeder program gets an \ulhyperref{DomainsInputFile}{XML file} from input that is intended to group PDO entries of the
EtherCAT network in one or more domains. Then it scans the network and identifies the slaves, matching them against the
provided input. In case there are no fatal errors, the feeder prepares the data to be fed to the configuration kernel module.
This data is transfered through a \code{/proc} file named \code{configure\_ethercat\_?} where \code{?} is
replaced by the kernel module's input parameter \code{ecc\_master\_id}. The state machine for the configuration module is
presented in Figure~\ref{Figure Feeder 0 Configuration module's state machine}.
Next the \ulhyperref{Feeder TableofStates}{Table of States} and more importantly, the \ulhyperref{Feeder TableofCommands}{Table of Commands} are presented. For the maintainer of
the kernel module, an additional \ulhyperref{Feeder TableofActions}{Table of Actions} is provided outlining the important actions taken on receipt of
each command.

\begin{figure}
\includegraphics[width=\textwidth,type=pdf,ext=.pdf,read=.pdf]{files/config_fsm.svg}
\caption{Configuration module's state machine}
\label{Figure Feeder 0 Configuration module's state machine}
\end{figure}

It is assumed that the reader of this page is already familiar with IgH EtherCAT master, or is able to look up necessary
information, such as fields of \code{ec\_sync\_info\_t}, \code{ec\_pdo\_info\_t} and \code{ec\_pdo\_entry\_info\_t}
structures.

\subsection{Table of States}\label{Feeder TableofStates}
The descriptions of states are as follows.


 
 State
 Description
 
 
 Init
 Initial state of configuration
 
 
 Wait nSlaves
 At this state, the domain information such as number of entries in each domain has arrived
 and the configuration module is waiting for the command indicating number of slaves (\ulhyperref{Feeder N}{N})
 
 
 Wait domains
 At this state, the number of slaves has arrived and the configuration module is waiting for the command indicating
 domain information such as number of entries in each domain (\ulhyperref{Feeder D}{D})
 
 
 Wait slave
 At this state, the configuration module is waiting for the command indicating beginning of configuration of one slave (\ulhyperref{Feeder S}{S})
 
 
 Wait data
 At this state, the configuration module is configuring a slave. The possible commands are the commands for specifying syncs (\ulhyperref{Feeder Y}{Y}),
 PDOs (\ulhyperref{Feeder P}{P}) and entries (\ulhyperref{Feeder E}{E}) of the slave. These commands can arrive in arbitrary sequence
 
 
 Wait register
 At this state, the slaves have all been configured, the type-name-to-code map has been built and the configuration module waits
 for the command to register the domains in the EtherCAT master (\ulhyperref{Feeder R}{R})
 
 
 Wait map
 At this state, the slaves have all been configured, the domains have been registered in the EtherCAT master and the configuration module
 waits for the command to create type-name-to-code map (\ulhyperref{Feeder M}{M})
 
 
 Wait activate
 At this state, everything is ready and the activate command is needed for the configuration module to activate the master (\ulhyperref{Feeder A}{A})
 
 
 End
 The state where \code{\ulhyperref{Exported eccUNDERSCOREready}{ecc\_ready}} is changed away from 0 and the EtherCAT network is usable
 


\subsection{Table of Commands}\label{Feeder TableofCommands}
Next, the table of commands, their formats and their descriptions are presented. In the same table, for each command a few examples are
included.


 
 Command \& Format
 Description
 Examples
 
 
 D
 \code{d-nd-}(\code{nd}$\times$)\{\code{nei-}\}
 This command begins with a \code{d-} followed by the number of domains to be configured, \code{nd}. After that,
 there are \code{nd} numbers, \code{nei}, that show the number of entries in domain \code{i}, \code{i} from 0 to \code{nd-1}
 \code{d-5-10-13-9-45-43-}\\
 \code{d-1-120-}
 
 
 N
 \code{n-ns-}
 This command begins with an \code{n-} followed by the number of slaves to be configured, \code{ds}
 \code{n-6-}
 
 
 S
 \code{s-a-p-vid-pc-ne-np-ny-}
 This command begins with an \code{s-} followed by the slave alias \code{a}, position \code{p}, vendor id \code{vid}, product code \code{pc},
 number of entries \code{ne}, number of PDOs \code{np} and number of syncs \code{ny} that indicates the beginning of a slave configuration.
 See the EtherCAT master's documentation on structure \code{ec\_slave\_info\_t}
 \code{s-0-0-0x0000061d-0x01-72-6-4-}\\
 \code{s-0-1-0x12345678-0x01-23-2-4-}
 
 
 E
 \code{e-i-si-b-t-d-}
 This command begins with an \code{e-} followed by the entry index \code{i}, subindex \code{s}, bit length \code{b}, its type code \code{t} and assigned
 domain \code{d} that specifies an entry of the current slave being configured as well as the domain it is assigned to. Note that entries
 are added to a list of \code{\ulhyperref{eccUNDERSCOREslave entries}{entries}} of the slave and therefore the entries of a PDO must arrive continuously,
 even though they may be interleaved with \ulhyperref{Feeder P}{P} and \ulhyperref{Feeder Y}{Y} commands. See the EtherCAT master's documentation on structures
 \code{ec\_pdo\_entry\_info\_t} and \code{ec\_pdo\_entry\_reg\_t}
 \code{e-0x30a3-0x6013-16-0-0-}\\
 \code{e-52365-46224-0x13-5-1-}
 
 
 P
 \code{p-i-ne-}
 This command begins with a \code{p-} followed by the pdo index \code{i} and number of entries \code{ne} that specifies a PDO of the current
 slave being configured. Note that PDOs are added to a list of \ulhyperref{eccUNDERSCOREslave pdos}{PDOs} of the slave and therefore the PDOs of a sync must
 arrive continuously, even though they may be interleaved with \ulhyperref{Feeder E}{E} and \ulhyperref{Feeder Y}{Y} commands. The indices from \code{\ulhyperref{eccUNDERSCOREslave pdos}{pdos}} to \code{\ulhyperref{eccUNDERSCOREslave entries}{entries}}
 will be automatically created based on this assumption. See the EtherCAT master's documentation on structure \code{ec\_pdo\_info\_t}
 \code{p-0x164a-12-}\\
 \code{p-23814-35-}
 
 
 Y
 \code{y-i-io-wd-np-}
 This command begins with a \code{y-} followed by the sync index \code{i}, whether it is input \code{io} (1 if input and 0 if output), whether watchdog
 is enabled (1 if yes and 0 if not) and number of PDOs \code{np} that specifies a sync of the current slave being configured. See the EtherCAT
 master's documentation on structure \code{ec\_sync\_info\_t}
 \code{y-0x00-0-0-0-}\\
 \code{y-0x01-1-0-0-}\\
 \code{y-0x02-0-1-0-}\\
 \code{y-0x03-1-0-6-}
 
 
 C
 \code{c-}
 This command consists of only a \code{c-} which tells the configuration module that its entries, PDOs and syncs have been fully defined
 \code{c-}
 
 
 M
 \code{m-nt-}(\code{nt}$\times$)\{\code{name--id-}\}
 This command begins with an \code{m-} followed by the number of entry types \code{nt}. After that, there are \code{nt} pairs of names and ids, \code{name} and
 \code{id}, that shows the type code of entries with name \code{name} as \code{id}. Please note the double dash after \code{name} that shows the end of the string
 \code{m-1-Sensor--0-}\\
 \code{m-2-Taxel--5-LED--2-}\\
 \code{m-3-Taxel--0-Temp--2-LED--1-}
 
 
 R
 \code{r-}
 This command consists of only an \code{r-} which tells the configuration module that all the slaves have been configured and all entries
 assigned to domains
 \code{r-}
 
 
 A
 \code{a-}
 This command consists of only an \code{a-} which tells the configuration module that all the configuration has been done successfully and
 that the kernel module should now activate the master
 \code{a-}
 


In the above commands, \code{-} is used as a delimiter and can be replaced by whitespace. You should note however that after the \code{name} string in
the \ulhyperref{Feeder M}{M} command there are two dashes that indicate the end of the string and cannot be replaced with whitespace. Otherwise, whitespace and dashes can be
mixed and they both work as delimiter. In truth, no delimiter is needed after a character and no delimeter is needed between an integer and a following
character. This is however most discouraged and at times could be problematic, for example when a \ulhyperref{Feeder C}{C} command follows a hex number which will be interpreted
as part of that number if not delimeted. An important note is that the command list \textbf{must} have at least one character of trailing whitespace or dash.

\subsection{Table of Actions}\label{Feeder TableofActions}
\textbf{For the kernel module maintainer}: The following table sketches the actions taken when each command is received.


 
 Command \& Format
 Actions taken
 
 
 Initialization
 
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREready}{ecc\_ready}}
 \code{$\leftarrow$ 0}
 
 
 \code{rt\_typed\_sem\_init(\&\_master\_mutex, 1, BIN\_SEM)}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREmaster}{ecc\_master}}
 \code{$\leftarrow$ ecrt\_request\_master(ecc\_master\_id}\ulhyperref{Feeder footnote1}{1}\code{)}
 
 \code{ecrt\_master\_callbacks(\ulhyperref{Exported eccUNDERSCOREmaster}{ecc\_master}, ecc\_request\_lock\_callback, ecc\_release\_lock\_callback, ecc\_master)}
 
 
 
 
 D
 \code{d-nd-}(\code{nd}$\times$)\{\code{nei-}\}
 
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREdomainsUNDERSCOREcount}{ecc\_domains\_count}}
 \code{$\leftarrow$ nd}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}}
 \code{$\leftarrow$ allocate nd $\times$ \ulhyperref{eccUNDERSCOREdomain}{ecc\_domain}}
 
 
 \code{domain\_entry\_offsets }
 \code{$\leftarrow$ allocate nd $\times$ unsigned int *}
 
 
 \code{for i from 0 to nd-1}
 
 
 \code{~~~~~~ecc\_domains[i].entries\_count }
 \code{$\leftarrow$ nei}
 
 
 \code{~~~~~~ecc\_domains[i].entries }
 \code{$\leftarrow$ allocate nei $\times$ unsigned int}
 
 
 \code{~~~~~~ecc\_domains[i].entry\_types }
 \code{$\leftarrow$ allocate nei $\times$ unsigned int}
 
 
 \code{~~~~~~ecc\_domains[i].domain }
 \code{$\leftarrow$ ecrt\_master\_create\_domain(\ulhyperref{Exported eccUNDERSCOREmaster}{ecc\_master})}
 
 
 \code{~~~~~~domain\_entry\_offsets[i] }
 \code{$\leftarrow$ allocate nei $\times$ unsigned int}
 
 
 
 
 N
 \code{n-ns-}
 
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREslavesUNDERSCOREcount}{ecc\_slaves\_count}}
 \code{$\leftarrow$ ns}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREslaves}{ecc\_slaves}}
 \code{$\leftarrow$ allocate ns $\times$ \ulhyperref{eccUNDERSCOREslave}{ecc\_slave}}
 
 
 
 
 S
 \code{s-a-p-vid-pc-ne-np-ny-}
 
 
 
 \code{newSlave.info.alias}
 \code{$\leftarrow$ a}
 
 
 \code{newSlave.info.position}
 \code{$\leftarrow$ p}
 
 
 \code{newSlave.info.vendor\_id}
 \code{$\leftarrow$ vid}
 
 
 \code{newSlave.info.product\_code}
 \code{$\leftarrow$ pc}
 
 
 \code{newSlave.entries\_count}
 \code{$\leftarrow$ ne}
 
 
 \code{newSlave.pdos\_count}
 \code{$\leftarrow$ np}
 
 
 \code{newSlave.syncs\_count}
 \code{$\leftarrow$ ns}
 
 
 \code{newSlave.entries}
 \code{$\leftarrow$ allocate ne $\times$ ec\_pdo\_entry\_info\_t}
 
 
 \code{newSlave.pdos}
 \code{$\leftarrow$ allocate np $\times$ ec\_pdo\_info\_t}
 
 
 \code{newSlave.syncs}
 \code{$\leftarrow$ allocate ny $\times$ ec\_sync\_info\_t}
 
 
 \code{newSlave.position\_in\_domains}
 \code{$\leftarrow$ allocate ne $\times$ ecc\_position\_in\_domain}
 
 
 \code{newSlave.pdo\_indices\_of\_syncs}
 \code{$\leftarrow$ allocate np $\times$ unsigned int}
 
 
 \code{newSlave.entry\_indices\_of\_pdos}
 \code{$\leftarrow$ allocate ne $\times$ unsigned int}
 
 
 \code{ecc\_slaves[cur\_slave]}
 \code{$\leftarrow$ newSlave}
 
 
 
 
 E
 \code{e-i-si-b-t-d-}
 
 
 
 \code{newEntry.index}
 \code{$\leftarrow$ i}
 
 
 \code{newEntry.subindex}
 \code{$\leftarrow$ si}
 
 
 \code{newEntry.bit\_length}
 \code{$\leftarrow$ b}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREslaves}{ecc\_slaves}[cur\_slave].entries[cur\_entry]}
 \code{$\leftarrow$ newEntry}
 
 
 \code{newEntryInDomain.alias}
 \code{$\leftarrow$ ecc\_slaves[cur\_slave].info.alias}
 
 
 \code{newEntryInDomain.position}
 \code{$\leftarrow$ ecc\_slaves[cur\_slave].info.position}
 
 
 \code{newEntryInDomain.vendor\_id}
 \code{$\leftarrow$ ecc\_slaves[cur\_slave].info.vendor\_id}
 
 
 \code{newEntryInDomain.product\_code}
 \code{$\leftarrow$ ecc\_slaves[cur\_slave].info.product\_code}
 
 
 \code{newEntryInDomain.index}
 \code{$\leftarrow$ i}
 
 
 \code{newEntryInDomain.subindex}
 \code{$\leftarrow$ si}
 
 
 \code{newEntryInDomain.offset}
 \code{$\leftarrow$ current free offset position}
 
 
 \code{newEntryInDomain.bit\_position}
 \code{$\leftarrow$ 0}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}[d].entries[cur\_entry\_in\_domain]}
 \code{$\leftarrow$ newEntryInDomain}
 
 
 \code{ecc\_domains[d].entry\_types[cur\_entry\_in\_domain]}
 \code{$\leftarrow$ t}
 
 
 \code{newPositionInDomains.domain}
 \code{$\leftarrow$ d}
 
 
 \code{newPositionInDomains.entry\_index}
 \code{$\leftarrow$ index of cur entry}
 
 
 \code{ecc\_slaves[cur\_slave].position\_in\_domains[cur\_entry]}
 \code{$\leftarrow$ newPositionInDomains}
 
 
 
 
 P
 \code{p-i-ne-}
 
 
 
 \code{newPdo.index}
 \code{$\leftarrow$ i}
 
 
 \code{newPdo.n\_entries}
 \code{$\leftarrow$ ne}
 
 
 \code{newPdo.entries}
 \code{$\leftarrow$ location in ecc\_slaves[cur\_slave].entries}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREslaves}{ecc\_slaves}[cur\_slave].pdos[cur\_pdo]}
 \code{$\leftarrow$ newPdo}
 
 
 \code{ecc\_slaves[cur\_slave].pdo\_indices\_of\_syncs[cur\_pdo]}
 \code{$\leftarrow$ offset of newPdo.entries}
 
 
 
 
 Y
 \code{y-i-io-wd-np-}
 
 
 
 \code{newSync.index}
 \code{$\leftarrow$ i}
 
 
 \code{if io == 0}
 
 
 \code{~~~~~~newSync.dir}
 \code{$\leftarrow$ EC\_DIR\_OUTPUT}
 
 
 \code{else}
 
 
 \code{~~~~~~newSync.dir}
 \code{$\leftarrow$ EC\_DIR\_INPUT}
 
 
 \code{if wd == 0}
 
 
 \code{~~~~~~newSync.watchdog\_mode}
 \code{$\leftarrow$ EC\_WD\_ENABLE}
 
 
 \code{else if wd == 1}
 
 
 \code{~~~~~~newSync.watchdog\_mode}
 \code{$\leftarrow$ EC\_WD\_DISABLE}
 
 
 \code{else}
 
 
 \code{~~~~~~newSync.watchdog\_mode}
 \code{$\leftarrow$ EC\_WD\_DEFAULT}
 
 
 \code{newSync.n\_pdos}
 \code{$\leftarrow$ np}
 
 
 \code{newSync.pdos}
 \code{$\leftarrow$ location in ecc\_slaves[cur\_slave].pdos}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREslaves}{ecc\_slaves}[cur\_slave].syncs[cur\_sync]}
 \code{$\leftarrow$ newSync}
 
 
 \code{ecc\_slaves[cur\_slave].pdo\_indices\_of\_syncs[cur\_sync]}
 \code{$\leftarrow$ offset of newSync.pdos}
 
 
 
 
 C
 \code{c-}
 
 
 
 \code{slave\_config}
 \code{$\leftarrow$ ecrt\_master\_slave\_config(\ulhyperref{Exported eccUNDERSCOREmaster}{ecc\_master},\\
 ~~~~~~~~\ulhyperref{Exported eccUNDERSCOREslaves}{ecc\_slaves}[cur\_slave].info.alias,\\
 ~~~~~~~~ecc\_slaves[cur\_slave].info.position,\\
 ~~~~~~~~ecc\_slaves[cur\_slave].info.vendor\_id,\\
 ~~~~~~~~ecc\_slaves[cur\_slave].info.product\_code)
 }
 
 
 \code{ecrt\_slave\_config\_pdos(slave\_config, syncCount, ecc\_slaves[cur\_slave].syncs)}
 
 
 
 
 M
 \code{m-nt-}(\code{nt}$\times$)\{\code{name--id-}\}
 
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREentryUNDERSCOREnameUNDERSCOREmapUNDERSCOREcount}{ecc\_entry\_name\_map\_count}}
 \code{$\leftarrow$ nt}
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREentryUNDERSCOREnameUNDERSCOREmap}{ecc\_entry\_name\_map}}
 \code{$\leftarrow$ allocate nt $\times$ \ulhyperref{eccUNDERSCOREentryUNDERSCOREnameUNDERSCOREtoUNDERSCOREtype}{ecc\_entry\_name\_to\_type}}
 
 
 \code{for i from 0 to nt-1}
 
 
 \code{~~~~~~ecc\_entry\_name\_map[i].name}
 \code{$\leftarrow$ name}
 
 
 \code{~~~~~~ecc\_entry\_name\_map[i].type}
 \code{$\leftarrow$ id}
 
 
 
 
 R
 \code{r-}
 
 
 
 \code{for i from 0 to \ulhyperref{Exported eccUNDERSCOREdomainsUNDERSCOREcount}{ecc\_domains\_count}-1}
 
 
 \code{~~~~~~\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}[i].entries[one\_after\_last]}
 \code{$\leftarrow$ emptyEntry}
 
 
 \code{~~~~~~ecrt\_domain\_reg\_pdo\_entry\_list(ecc\_domains[i].domain, ecc\_domains[i].entries)}
 
 
 
 
 A
 \code{a-}
 
 
 
 \code{ecrt\_master\_activate(\ulhyperref{Exported eccUNDERSCOREmaster}{ecc\_master})}
 
 
 \code{for i from 0 to \ulhyperref{Exported eccUNDERSCOREdomainsUNDERSCOREcount}{ecc\_domains\_count}-1}
 
 
 \code{~~~~~~\ulhyperref{Exported eccUNDERSCOREdomains}{ecc\_domains}[i].process\_data}
 \code{$\leftarrow$ ecrt\_domain\_data(ecc\_domains[i].domain)}
 
 
 
 
 Finilization
 
 
 
 \code{\ulhyperref{Exported eccUNDERSCOREready}{ecc\_ready}}
 \code{$\leftarrow$ 1}
 
 
 


1 \code{ecc\_master\_id} is an input parameter to the kernel module. That is, if there are mutiple masters, multiple instance
of the kernel module must be loaded. The \code{/proc} file name will appended by this code. For more information, please refer to the PDF documentation.

\textcolor[rgb]{0.5, 0.5, 0.5}{\footnotesize{\textit{Generated by} \textbf{DocThis! Documentation Generator} \textit{program.}}}
