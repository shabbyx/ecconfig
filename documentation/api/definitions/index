index
# EtherCAT Configuration Module #
version v0.1.2.107 (kernel module) and v0.1.2.95 (feeder program)
author Shahbaz Youssefi
keyword EtherCAT
keyword automatic configuration
keyword configuration
shortcut constants
shortcut globals
shortcut Feeder
seealso [Feeder program](Feeder)

# Introduction

The EtherCAT configuration module consists of two parts; a kernel space module and user space program. There are two
libraries that need to be present for the module to work, namely [IgH EtherCAT master](custom http://www.etherlab.org) and
[RTAI](custom https://www.rtai.org). It is assumed that the reader of this documentation has basic knowledge of these libraries,
especially IgH EtherCAT master, and is able to look up variable types and constants used from those libraries in their respective
documentations.

	- In kernel space, a module is responsible for establishing connection with EtherCAT and performing the configuration.
	The configuration data however are more trivialy sorted and customized in user-space and thus, this kernel module awaits
	configuration data on a `/proc` file. After receiving the configuration data, the kernel module configures and
	activates the EtherCAT master. During this process, certain data structures are built and exported so that other kernel
	modules that need to work with EtherCAT could use them.

	- In user space, a program is implemented that reads from an [XML input](Domains Input File) file defining domains
	on network _entries_. Afterwards, the program scans the EtherCAT network, identifying _slaves_, _PDOs_ and
	entries and generates configuration data. Finally, it sends the configuration by a certain [protocol](Feeder)
	to the kernel module via the `/proc` file. This program henceworth will be called the _feeder program_.

First the kernel module neededs to be inserted in the kernel. Then the user space program runs and feeds it. If everything
was ok, the exported variables should be usable as soon as the kernel module [finishes](Exported#ecc_ready) configuration.

**Note:** The structures and variables introduced in this documentation all refer to what is defined in
`ethercat_configure.h` located in the folder of the configuration kernel module and which needs to be included in
your kernel module. To see the format of the domains XML input file, see [here](Domains Input File) and to see the protocol of
communication between the feeder program and the kernel module, please see [this](Feeder) page.

For documentation on installation and execution please refer to the PDF documentation.

## List of Structures

### `[ecc_domain]`
>	This structure contains information on one domain. These information include EtherCAT master's variable identifying the domain, as
	well as list of entries in the domain, their types and their I/O memory. Through a list of this structure, named
	`[ecc_domains](Exported#ecc_domains)`, data of the domain could be sent/received and entries of each domain accessed
	as a whole.
### `[ecc_slave]`
>	This structure contains information on one slave. These information include a list of entries, PDOs and syncs as well as a link
	to `[ecc_domains](Exported#ecc_domains)` for every entry of the slave. Through a list of this structure, named
	`[ecc_slaves](Exported#ecc_slaves)`, the EtherCAT network could be examined hierarchically.
### `[ecc_entry_name_to_type]`
>	This structure is a map from entry names, as defined in the [XML input](Domains Input File), to type ids assigned by the
	feeder program. Through a list of this structure, each kernel module can search for, and identify in
	`[ecc_domains](Exported#ecc_domains)` the entries of interest for its purpose.

## Globals

### [Exported Variables](Exported)
>	The kernel module exports variables for use of other kernel modules using the EtherCAT network. These variables include a reference
	to the master, master access mutex and lists of [domain data](#ecc_domain), [slave data](#ecc_slave) and [entry name mapping](#ecc_entry_name_to_type).

## Input

### [Domains Input File]
>	This file is an input to the feeder program which contains an XML definition of domains and how the PDO entries of the network are
	assigned to them.

## List of Examples

### [Simple Read]
>	This example shows a simple thread that reads from the network with one domain.
### [Multiple Domains]
>	This example is similar to the previous one, except there are more than 1 domains in this example.
### [Finding Entries of Interest]
>	In this example, a list of entries are inspected and entries of interest are identified. Additionally, a sample thread that reads
	from these values is presented.
### [Fast Read]
>	In this example, a new method of reading from the network is presented. It requires a feature that is now under test and would possibly
	be added to the master in the future. With this method, the data are read from the network exactly when they arrive which has smaller
	delay in comparison with the the standard methods were the data being processed are those requested by in the previous cycle.
