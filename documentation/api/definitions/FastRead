example Fast Read
# EtherCAT Configuration Module #
version v0.1.2.107 (kernel module) and v0.1.2.95 (feeder program)
author Shahbaz Youssefi
keyword EtherCAT
keyword automatic configuration
keyword configuration
shortcut index
shortcut constants
shortcut globals
shortcut Feeder
previous example Finding Entries of Interest
next variables Exported

In this example, a differet method of reading from the network is presented. Before reading this example, please see [the standard method](Simple Read).
This method of acquisition is possible through a new feature of the IgH EtherCAT master which is as of this date not yet included in the released source code
of the master. This new function is called `ecrt_domain_received`.

```
// Linux
#include \<linux/module.h\>
#include \<linux/err.h\>
#include \<linux/vmalloc.h\>

// RTAI
#include \<rtai_sched.h\>

`#include "ethercat_configure.h"`

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Shahbaz Youssefi");

#define                 PFX                     "Ethercat Test: "

#define                 FREQUENCY               25 // task frequency in Hz

// If not read in 1ms, then the packet is lost
``#define                 TIMEOUT                 1000000``
// Check for domain every 50us
``#define                 SLEEP_TIME              50000``

`static RT_TASK          task;`

`unsigned short int      *domain0_data           = '\\0';`

`static void run(long data)`
{
	int i;
	`RTIME predicted_wait_time = 0;`
	`RTIME start_time, end_time;`
	`while (1)`
	`{`
		// send packet
		``[#ecc_lock_master](Exported)();``
		``ecrt_domain_queue([#ecc_domains](Exported)\[0].[#domain](ecc_domain));``
		``[#ecc_unlock_master](Exported)();``
		``ecrt_master_send([#ecc_master](Exported));``

		// wait an expected amount of time (predicted network delay)
		`start_time = rt_get_time_ns();`
		`rt_sleep(nano2count(predicted_wait_time));`

		// try receiving everything from the network
		``ecc_lock_master();``
		``ecrt_master_receive(ecc_master);``
		``ecc_unlock_master();``

		// check to see of domain has arrived.  If not, sleep and try again
		``while (!ecrt_domain_received(ecc_domains\[0].domain)`` `\&\& rt_get_time_ns()-start_time \< TIMEOUT`<>``)``
		``{``
			``rt_sleep(nano2count(SLEEP_TIME));``
			``ecc_lock_master();``
			``ecrt_master_receive(ecc_master);``
			``ecc_unlock_master();``
		``}``

		`end_time = rt_get_time_ns();`
		`if (end_time-start_time \> TIMEOUT)`
			`rt_printk(KERN_INFO"domain timed out");`
		`else`
		`{`
			// receive process data:
			``ecc_lock_master();``
			``ecrt_domain_process(ecc_domains\[0].domain);``
			``ecc_unlock_master();``

			// predict wait time: (old_value*7+new_value)/8
			// This formula is written merely so that 64 bit division could be avoided
			`if (predicted_wait_time < end_time-start_time)`
				`predicted_wait_time = (predicted_wait_time*7+(end_time-start_time))>>3;`
		`}`

		// read process data
		`for (i = 0; i \< ecc_domains\[0].[#entries_count](ecc_domain); ++i)`
			`domain0_data\[i] = EC_READ_U16(ecc_domains\[0].[#process_data](ecc_domain)+*ecc_domains\[0].[#entries](ecc_domain)\[i].offset);`

		`rt_task_wait_period();`
	`}`
}

`int init_module(void)`
{
	printk(KERN_INFO PFX "Starting...\\n");
	printk(KERN_INFO PFX "Starting cyclic sample thread...\\n");
	`domain0_data = vmalloc(ecc_domains\[0].entries_count*sizeof(*domain0_data));`
	if (!domain0_data)
	{
		printk(KERN_ERR PFX "Failed to aquire process memory\\n");
		return -500;
	}
	`rt_set_oneshot_mode();`
	`start_rt_timer(nano2count(100000));`
	if (`rt_task_init(&amp;task, run, 0, 2000, 0, 1, NULL)`)
	{
		printk(KERN_ERR PFX "Failed to init RTAI task!\\n");
		return -1000;
	}
	if (`rt_task_make_periodic(&amp;task, rt_get_time() + 1000000, nano2count(1000000000/FREQUENCY))`)
	{
		printk(KERN_ERR PFX "Failed to run RTAI task!\\n");
		rt_task_delete(&amp;task);
		return -1500;
	}
	printk(KERN_INFO PFX "Starting...done.\\n");
	return 0;
}

`void cleanup_module(void)`
{
	printk(KERN_INFO PFX "Stopping...\\n");
	`stop_rt_timer();`
	`rt_task_delete(\&task);`
	`if (domain0_data)`
		`vfree(domain0_data);`
	printk(KERN_INFO PFX "Stopping...done.\\n");
}
```
